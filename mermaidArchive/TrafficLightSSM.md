```mermaid
        graph TD
    Start --> NSEW_Red_1

    NSEW_Red_1 -->
    NRSensor{NRSensor}

    NRSensor -->|true|SRSensor{SRSensor}

    NRSensor -->|false|SRSensor2{SRSensorFalse}
    SRSensor2 --> |true|S_SR_yellow
    S_SR_yellow --> S_SR_green 

    SRSensor2 --> |false|NS_green

    SRSensor -->|true|NR_SR_yellow
    NR_SR_yellow --> NR_SR_green

    SRSensor -->|false|N_NR_yellow
    N_NR_yellow --> N_NR_green

    S_SR_green --> NS_green
    N_NR_green --> NS_green
    NR_SR_green --> NS_green

    NS_green --> NS_yellow
    NS_yellow --> NSEW_Red_2

    NSEW_Red_2 --> ERSensor{ERSensor}

    ERSensor -->|true| WRSensor{WRSensor}

    WRSensor -->|true| ER_WR_yellow
    ER_WR_yellow --> ER_WR_green

    WRSensor -->|false| E_ER_yellow
    E_ER_yellow --> E_ER_green

    ERSensor -->|false|WRSensor2{WRSensorFalse}
    WRSensor2 --> |true|W_WR_yellow
    W_WR_yellow --> W_WR_green 
    WRSensor2 --> |false|EW_green

    E_ER_green --> EW_green
    W_WR_green --> EW_green
    ER_WR_green --> EW_green

    EW_green --> EW_yellow
    EW_yellow --> NSEW_Red_1
```