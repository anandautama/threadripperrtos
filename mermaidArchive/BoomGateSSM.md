```mermaid
    graph TD
    Start --> NoTrain
    NoTrain --> test1{counter IN > 0 <br> OR  <br> counter OUT > 0}
    test1 --> |false|NoTrain
    test1 --> |true|Train
    Train --> test2{counter IN == 0 <br> AND <br> counter OUT > 0}
    test2 --> |false|Train
    test2 --> |true|NoTrain
```