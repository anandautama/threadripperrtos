```mermaid
    graph TD
    Start --> NSEW_Red_1
    NSEW_Red_1 --> NS_green
    NS_green --> NS_yellow
    NS_yellow --> NSEW_Red_2

    NSEW_Red_2 --> EW_green
    EW_green --> EW_yellow
    EW_yellow --> NSEW_Red_1
```