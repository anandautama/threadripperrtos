/*
 * Traffic_Light_System_Def.h
 *
 *  Created on: 12Oct.,2018
 *      Author: Michael Christoe
 */

#ifndef SRC_TRAFFIC_LIGHT_SYSTEM_DEF_H_
#define SRC_TRAFFIC_LIGHT_SYSTEM_DEF_H_

// hostname RMIT_BBB_v5_07 // 192.168.201.138 (next to chris)
// hostname RMIT_BBB_v5_06 // 192.168.201.71
// hostname RMIT_BBB_v5_03 // 192.168.201.24
// hostname RMIT_BBB_v5_09 // 192.168.201.149

#define RW_ATTACH_POINT "/net/RMIT_BBB_v5_10/dev/name/local/RailwayCrossing"
#define CC_ATTACH_POINT  "/net/RMIT_BBB_v5_01/dev/name/local/CentralControl"  // hostname using full path, change myname to the name used for server
#define TL1_ATTACH_POINT  "/net/RMIT_BBB_v5_04/dev/name/local/TrafficNode1"  // hostname using full path, change myname to the name used for server
#define TL2_ATTACH_POINT  "/net/RMIT_BBB_v5_08/dev/name/local/TrafficNode2"  // hostname using full path, change myname to the name used for server
#define BUF_SIZE 100

#define STATUS_UPDATE_ID			0xFF
#define COMMAND_FORCE_STATE			0x01
#define COMMAND_FORCE_STATE_CANCEL	0x02
#define COMMAND_FORCE_MODE			0x03
#define COMMAND_FORCE_MODE_CANCEL	0x04
#define COMMAND_GET_MODE_SCHED		0x05
#define COMMAND_SET_MODE_SCHED		0x06
#define ACK_FORCE_STATE				0x81
#define ACK_FORCE_STATE_CANCEL		0x82
#define ACK_FORCE_MODE				0x83
#define ACK_FORCE_MODE_CANCEL		0x84
#define ACK_GET_MODE_SCHED			0x85
#define ACK_SET_MODE_SCHED			0x86

#define MODE_ID_DEFAULT 0x01
#define MODE_ID_PEAK_TIME 0x02
#define MODE_ID_NIGHT_TIME 0x03


#define ENTITY_CC	0x01
#define ENTITY_TL1	0x02
#define ENTITY_TL2	0x03
#define ENTITY_RW	0x04

enum pedStates{
	NSEWPRed,
	NPGreen,
	NPFlash,
	SPGreen,
	SPFlash,
	EPGreen,
	EPFLash,
	WPGreen,
	WPFlash,
	NP_SPGreen,
	NP_SPFlash,
	EP_WPGreen,
	EP_WPFlash
};

enum trafficLightStates {
	NSEWRed_NorthSouth,
	NSEWRed_NorthSouth_Special_NoTrain,
	NSEWRed_NorthSouth_Special_TrainCond,
	NSEWRed_EastWest,
	NSEWRed_EastWest_Special_NoTrain,
	//NSEWRed_EastWest_Special_TrainCond,
	NSGreen,
	NSYellow,
	EWGreen,
	EWYellow,
	NR_SRGreen,
	NR_SRYellow,
	ER_WRGreen,
	ER_WRYellow,
	N_NRGreen,
	NGreen_NRYellow,
	NGreen,
	S_SRGreen,
	SGreen_SRYellow,
	SYellow,
	SGreenNoTrain,
	SGreenTrain,
	E_ERGreen,
	EGreen_ERYellow,
	EGreenNoTrain,
	EGreenTrain,
	W_WRGreen,
	WGreen_WRYellow,
	WGreen
};

enum Rail_Crossing_States {
	Train_Present,
	Train_NotPresent
};

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
    uint8_t data[BUF_SIZE];     // our data
} packet_data; //


typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
    uint8_t data[BUF_SIZE];// Message we send back to clients to tell them the messages was processed correctly.
} packet_reply_data;

//Keypad hardware defines
#define AM335X_CONTROL_MODULE_BASE   (uint64_t) 0x44E10000
#define AM335X_CONTROL_MODULE_SIZE   (size_t)   0x00001448
#define AM335X_GPIO_SIZE             (uint64_t) 0x00001000
#define AM335X_GPIO1_BASE            (size_t)   0x4804C000

#define LED0          (1<<21)   // GPIO1_21
#define LED1          (1<<22)   // GPIO1_22
#define LED2          (1<<23)   // GPIO1_23
#define LED3          (1<<24)   // GPIO1_24

#define SD0 (1<<28)  // SD0 is connected to GPIO1_28
#define SCL (1<<16)  // SCL is connected to GPIO1_16


#define GPIO_OE        0x134
#define GPIO_DATAIN    0x138
#define GPIO_DATAOUT   0x13C

#define GPIO_IRQSTATUS_SET_1 0x38   // enable interrupt generation
#define GPIO_IRQWAKEN_1      0x48   // Wakeup Enable for Interrupt Line
#define GPIO_FALLINGDETECT   0x14C  // set falling edge trigger
#define GPIO_CLEARDATAOUT    0x190  // clear data out Register
#define GPIO_IRQSTATUS_1     0x30   // clear any prior IRQs

#define GPIO1_IRQ 99  // TRG page 465 list the IRQs for the am335x


#define P9_12_pinConfig 0x878 //  conf_gpmc_ben1 (TRM pp 1364) for GPIO1_28,  P9_12

// GPMC_A1_Configuration
#define PIN_MODE_0   0x00
#define PIN_MODE_1   0x01
#define PIN_MODE_2   0x02
#define PIN_MODE_3   0x03
#define PIN_MODE_4   0x04
#define PIN_MODE_5   0x05
#define PIN_MODE_6   0x06
#define PIN_MODE_7   0x07

// PIN MUX Configuration strut values  (page 1420 from TRM)
#define PU_ENABLE    0x00
#define PU_DISABLE   0x01
#define PU_PULL_UP   0x01
#define PU_PULL_DOWN 0x00
#define RECV_ENABLE  0x01
#define RECV_DISABLE 0x00
#define SLEW_FAST    0x00
#define SLEW_SLOW    0x01

typedef union _CONF_MODULE_PIN_STRUCT   // See TRM Page 1420
{
  unsigned int d32;
  struct {    // name: field size
           unsigned int conf_mmode : 3;       // LSB
           unsigned int conf_puden : 1;
           unsigned int conf_putypesel : 1;
           unsigned int conf_rxactive : 1;
           unsigned int conf_slewctrl : 1;
           unsigned int conf_res_1 : 13;      // reserved
           unsigned int conf_res_2 : 12;      // reserved MSB
         } b;
} _CONF_MODULE_PIN;

#endif /* SRC_TRAFFIC_LIGHT_SYSTEM_DEF_H_ */


