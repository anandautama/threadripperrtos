#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <share.h>
#include <time.h>
#include <errno.h>

#define LOCAL_ATTACH_POINT "myname"			  			     // change myname to the same name used for the server code.
#define QNET_ATTACH_POINT  "/net/VM_x86_Target02/dev/name/local/myname"  // hostname using full path, change myname to the name used for server

#define MY_PULSE_CODE  _PULSE_CODE_MINAVAIL //Pulse from button press?

#define BUF_SIZE 100

enum controllerFSM {
	menu_Init,
	main_Menu,
	status_Display,
	control_Status,
	functional_Status,
	control_Menu,
	control_Menu2,
	get_Sched_or_Mode,
	set_Sched_or_Mode,
	Force_or_Canc_State,
	Force_or_Canc_Mode};

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
    int data;     // our data
} packet_data; //

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
    char buf[BUF_SIZE];// Message we send back to clients to tell them the messages was processed correctly.
} packet_reply_data;

typedef union
{
	struct _pulse   pulse;
} pulse_data;

 typedef struct {
  pthread_mutex_t mutex; // add a mutex object into the structure we want to protect
  int channel_ID;
} thread_data;

int packetListen(int chid, packet_data * msg_recv, packet_reply_data * msg_reply_send)
{
	//msg_recv is used to hold value of packet data
	//msg_reply_send is used to send packet reply when button press received

	int rcvid=0; //return value when message received to determine what to do.
	int buttonDetect = 10; //hold value of button press

	rcvid = MsgReceive(chid, msg_recv, sizeof(*msg_recv), NULL); //blocks and waits for message from server

	//Following code is standard lab 5 code

	if (rcvid == -1)  // Error condition, exit
	{
		   printf("\nFailed to MsgReceive\n");
		   return 10;
	}
	else
	{
			printf("---------------From Server - Message is: %d\n\n",msg_recv->data);
			buttonDetect = msg_recv->data;
	}


	if (rcvid == 0)  //  Pulse received, work out what type
		   {
			   switch (msg_recv->hdr.code)
			   {
				   case _PULSE_CODE_DISCONNECT:
						// A client disconnected all its connections by running
						// name_close() for each name_open()  or terminated
					    ConnectDetach(msg_recv->hdr.scoid);
						printf("\nServer was told to Detach from ClientID:%d ...\n", msg_recv->ClientID);
						return 10;

				   case _PULSE_CODE_UNBLOCK:
						// REPLY blocked client wants to unblock (was hit by a signal
						// or timed out).  It's up to you if you reply now or later.
					   printf("\nServer got _PULSE_CODE_UNBLOCK\n");
					   break;

				   case _PULSE_CODE_COIDDEATH:  // from the kernel
					   printf("\nServer got _PULSE_CODE_COIDDEATH\n");
					   break;

				   case _PULSE_CODE_THREADDEATH: // from the kernel
					   printf("\nServer got _PULSE_CODE_THREADDEATH\n");
					   break;

				   default:
					   // Some other pulse sent by one of your processes or the kernel
					   printf("\nServer got some other pulse\n");
					   break;

			   }
		   }


	   if(rcvid > 0) // if true then A message was received
	   {
		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (msg_recv->hdr.type == _IO_CONNECT )
		   {
			   MsgReply( rcvid, EOK, NULL, 0 );
			   printf("\n gns service is running....");
		   }

		   // Some other I/O message was received; reject it
		   if (msg_recv->hdr.type > _IO_BASE && msg_recv->hdr.type <= _IO_MAX )
		   {
			   MsgError( rcvid, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   return 10;
		   }

		   // put your message handling code here and assemble a reply message
		   sprintf(msg_reply_send->buf, "Value: %d Received", msg_recv->data);


		   fflush(stdout);
		   sleep(1); // Delay the reply by a second (just for demonstration purposes)
	//	   printf("\n    -----> replying with: '%s'\n",replymsg.buf);
		   MsgReply(rcvid, EOK, &msg_reply_send, sizeof(msg_reply_send));
	   }
	   else
	   {
		   printf("\nERROR: Server received something, but could not handle it correctly\n");
	   }

	   return buttonDetect; //return the value of the button input
}

void *button_thread(void *data)
{
	printf("-----------Button Thread Started\n");
	thread_data button_Resources = *((thread_data*)data);

	packet_data msg_send;
	packet_reply_data msg_reply_recv;

	int serverPID  = 100000;	// CHANGE THIS Value to PID of the server process
	int	serverCHID = 1;			// CHANGE THIS Value to Channel ID of the server process (typically 1)
	int server_coid = 0;	//Value of channel

	//Following code reads file from relative location
	FILE *fp;
	fp = fopen( "CentralController.info", "r" );
	if( fp != NULL )
	{
		int readValue[1];
    	fread( &readValue, sizeof(int), sizeof(readValue), fp );
	    printf("From Client: CHID is: %d\n", readValue[0]);
	    printf("From Client: PID Value is: %d\n", readValue[1]);
	    fclose( fp );

	    serverPID = readValue[0]; //wrote pid from file
	    serverCHID = readValue[1]; //write chid from file
	}
	else
	{
		printf("From Client: Could not open file\n");
	}

	server_coid = ConnectAttach(ND_LOCAL_NODE, serverPID, serverCHID, _NTO_SIDE_CHANNEL, 0); //conenct to channel, returns value of connection ID

	if (server_coid == -1)
	{
	   printf("ERROR, could not connect to server!\n\n");
     //  pthread_exit(EXIT_FAILURE);
	}


	  msg_send.ClientID = 200; //can be changed to whatever
	  msg_send.hdr.type = 0x00; //set information as data send
	  msg_send.hdr.subtype = 0x00; //set information as data send



	while(1)
	{
		scanf("%d",&msg_send.data); //wait for user input
		printf("-------------Message Sent: %d\n",msg_send.data);

        if (MsgSend(server_coid, &msg_send, sizeof(msg_send), &msg_reply_recv, sizeof(msg_reply_recv)) == -1)
        {
            printf(" Error data '%d' NOT sent to server\n", msg_send.data);
            break;
        }
        else
        { // now process the reply
           // printf("   -->Reply is: '%s'\n", msg_reply_recv.buf);
        }

	}
}

void *menuFSM_thread(void*data)
{
	printf("---------Menu Thread Started\n");

	thread_data FSM_Resources = *((thread_data*)data); //contains mutexs and etc to for resource sharing


	enum controllerFSM FSM = main_Menu; //State machine to be used
	int buttonInput; //hold buttoninput


	packet_reply_data msg_reply_send;
	int rcvid=0;
	int serverPID=0, chid=0; 	// Server PID and channel ID
	serverPID = getpid(); 		// get server process ID
	FILE *fp;

	packet_data msg_recv;
	msg_recv.hdr.type = 0x01;
	msg_recv.hdr.subtype = 0x00;

	// Create Channel
	chid = ChannelCreate(_NTO_CHF_DISCONNECT);
	/*
	 * Create channel that can be used to receive messages and pulses
	 * Once created, channel owned by process and not creating thread
	 * _NTO_CHF_DISCONNECT delivers pulse when all connections from pulse detach
	 * 		if process dies without detachign connection, kernal does it
	 */

	if (chid == -1)  // _NTO_CHF_DISCONNECT flag used to allow detach
	{
	    printf("\nFailed to create communication channel on server\n");
		pthread_exit(EXIT_FAILURE);
	}

	fp = fopen( "CentralController.info", "w" );
	if( fp != NULL )
	{
		int writeError;
		writeError = fwrite( &serverPID, sizeof(int), sizeof(serverPID), fp );
		writeError = fwrite( &chid, sizeof(int), sizeof(chid), fp );
		printf( "From Server: Successfully wrote ServerID and ChannelID: %d and %d \n", serverPID, chid);
		fclose( fp );
	}
	else
	{
		printf("Failed to write\n");
		pthread_exit(EXIT_FAILURE);
	}

	printf("From Server: Process ID   : %d \n", serverPID);
	printf("From Server: Channel ID   : %d \n\n", chid);


	while(1)
	{
		printf("--------------Server now listening\n");

		switch(FSM)
		{
			case main_Menu:
				printf("Main Menu State:\n-Status Display:1\n-Control Status:2\n\n");
				buttonInput = packetListen(chid, &msg_recv, &msg_reply_send);
				//pass connection ID, message packet, and message reply information. Reading the packet from this thread may be useful later
				if(buttonInput == 1)
				{
					FSM = status_Display;
				}
				else if(buttonInput == 2)
				{
					FSM = control_Menu;
				}
				break;

			case status_Display:
				printf("Status Display State:\n-Connection Status:1\n-Functional Status:2\n-Back:9\n\n");
				buttonInput = packetListen(chid, &msg_recv, &msg_reply_send);
				if(buttonInput == 9)
				{
					FSM = main_Menu;
				}

				break;

			case control_Menu:
				printf("Control Menu State:\n-Get Schedule/Mode:1\n-Set Schedule/Mode:2\n-Back:9\n-Scroll Down:0\n\n");
				buttonInput = packetListen(chid, &msg_recv, &msg_reply_send);
				if(buttonInput == 9)
				{
					FSM = main_Menu;
				}
				else if(buttonInput == 0)
				{
					FSM = control_Menu2;
				}
				break;

			case control_Menu2:
				printf("Control Menu State:\n-Force/Cancel State:1\n-Force/Cancel Mode:2\n-Back:9\n-Scroll Up:0\n\n");
				buttonInput = packetListen(chid, &msg_recv, &msg_reply_send);
				if(buttonInput == 9)
				{
					FSM = main_Menu;
				}
				else if(buttonInput == 0)
				{
					FSM = control_Menu;
				}

				break;
			default:

				printf("------------Error in state\n, buttonInput is: %d\n", buttonInput);
				break;
		}
	}
}

void *client_thread(void *data)
{
	/*
	 packet_send_data packetData;
	 packet_reply_data replyData;

	 packetData.ClientID = 601; // unique number for this client (optional)

	 int serverConn;

	 if ((serverConn = name_open(QNET_ATTACH_POINT, 0)) == -1)
	 {
		 printf("\n    ERROR, could not connect to server!\n\n");
	     return EXIT_FAILURE;
	 }

	 packetData.hdr.type = 0x00;
	 packetData.hdr.subtype = 0x00;

	fflush(stdout);
	if (MsgSend(serverConn, &packetData, sizeof(packetData), &replyData, sizeof(replyData)) == -1)
	{
    }
    else
    {
       printf("   -->Reply is: '%s'\n", replyData.buf);
    }

	 // Close the connection
	 printf("\n Sending message to server to tell it to close the connection\n");
	 name_close(serverConn);
	 return EXIT_SUCCESS;
	 */
}


int main(int argc, char *argv[]) {
	pthread_t client_pthread, stateMachine_pthread, button_pthread;
	thread_data resource_data;
	void *pthread_end;

	resource_data.channel_ID = ChannelCreate(0);

	pthread_create(&stateMachine_pthread,NULL,menuFSM_thread,&resource_data);
//	pthread_create(&client_pthread,NULL,client_thread,&resource_data);
	pthread_create(&button_pthread,NULL,button_thread,&resource_data);

//	pthread_join(client_pthread,&pthread_end);
	pthread_join(stateMachine_pthread,&pthread_end);
	pthread_join(button_pthread,&pthread_end);

	printf("\nMain Terminating....");
	return EXIT_SUCCESS;
}
