#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <sys/procmgr.h>
#include <hw/inout.h>      // for in32() and out32();
#include <sys/mman.h>      // for mmap_device_io();
#include <stdint.h>        // for unit32 types
#include <share.h>
#include <time.h>
#include <errno.h>

#include "Traffic_Light_System_Def.h"

#define MY_PULSE_CODE  _PULSE_CODE_MINAVAIL //Pulse for timer
#define BUF_SIZE 100

enum controllerFSM { //Reference report for code flow
	menu_Init,
	main_Menu,
	status_Display,
	conn_Status,
	func_Status,
	control_Menu,
	control_Menu2,
	get_Sched_or_Mode,
	set_Sched_or_Mode,
	Force_or_Canc_State,
	Force_or_Canc_Mode
};

typedef union {
	struct _pulse pulse;
} timer_data; //used for getting pulse for timer

typedef struct {
	pthread_mutex_t button_Mutex; // add a mutex object into the structure we want to protect
	pthread_mutex_t command_Mutex; // add a mutex object into the structure we want to protect
	pthread_mutex_t update_Mutex; // add a mutex object into the structure we want to protect

	pthread_cond_t button_CondVar; // needs to be set to PTHREAD_COND_INITIALIZER;
	pthread_cond_t command_CondVar; // needs to be set to PTHREAD_COND_INITIALIZER;
	pthread_cond_t update_CondVar; // needs to be set to PTHREAD_COND_INITIALIZER;

	int channel_ID; //ID of established channel for something
	int buttonPressed; //conditional variable used for checking if button has been pressed
	int newCommand;// used for conditional variable sending commands to server
	int newUpdate; //used for conditional variable for gettign updates from server
	int serverUpdate;

	int buttonData; //button data shared between button press thread and FSM
	int *keypadData; //value from interrupt interpreted from data


	packet_data nodeParameter;
	packet_reply_data update_Node; //update status from railway node
} thread_data;



void PedStatePrintState(enum pedStates *CurState);
void TrafficLightPrintState(enum trafficLightStates *CurState);
void button_CondVar(thread_data * condition_Data, int * buttonRead);

void *button_thread(void *data); //thread for console input
void *menuFSM_thread(void*data); //thread for finite state machien
void *clientThread_Send1(void *data); //thread for CC act as controller and send to server
void *serverThread(void *data); //server thread for receiving updates

// hardware specific defines
int buttonSetup();
const struct sigevent* Inthandler( void* area, int id );
void *interruptKeyPadThread(void *data);
void *LCDthread_ex (void *data);

int main(int argc, char *argv[]) {
	pthread_t serverpthread;
	pthread_t keyPadThread;
	pthread_t clientSend1;
	pthread_t stateMachine_pthread;
	pthread_t button_pthread;
	thread_data resource_data;

	void *pthread_end;

	//Initialization
	pthread_mutex_init(&resource_data.button_Mutex, NULL); //mutex for button press
	pthread_mutex_init(&resource_data.update_Mutex, NULL); //mutex for update server
	pthread_mutex_init(&resource_data.command_Mutex, NULL); //mutex for sending commands to tarffic nodes
	pthread_cond_init(&resource_data.button_CondVar,NULL); //--
	pthread_cond_init(&resource_data.update_CondVar,NULL); //--
	pthread_cond_init(&resource_data.command_CondVar,NULL); //--
	resource_data.buttonPressed = 0; //Set to 0 for button mutex to button hasnt been pressed yet
	resource_data.newCommand = 0;
	resource_data.newUpdate = 1;
	resource_data.serverUpdate = 0;

	int result = keypadSetup();
	if(result == EXIT_SUCCESS)
	{
		pthread_create(&keyPadThread, NULL, interruptKeyPadThread, &resource_data);
	}
	else
	{
		printf("Periperhals cannot be initialized\n");
	}

//	pthread_create(&serverpthread, NULL, serverThread, &resource_data);
//	pthread_create(&clientSend1, NULL, clientThread_Send1, &resource_data);
//	pthread_create(&stateMachine_pthread,NULL,menuFSM_thread,&resource_data);
//	pthread_create(&button_pthread,NULL,button_thread,&resource_data);

	pthread_join(keyPadThread, &pthread_end);
//	pthread_join(serverpthread, &pthread_end);
//	pthread_join(clientSend1, &pthread_end);
//	pthread_join(stateMachine_pthread,&pthread_end);
//	pthread_join(button_pthread,&pthread_end);

	printf("\nMain Thread Terminating....");
	return EXIT_SUCCESS;
}


void PedStatePrintState(enum pedStates *CurState){
	int localState = *CurState;
	switch(*CurState){
		case NSEWPRed:
			printf("In Pedestrian current state: NSEWPedestrianRed\n");
			break;
		case NPGreen:
			printf("In Pedestrian current state: NPGreen\n");
			break;
		case NPFlash:
			printf("In Pedestrian current state: NPFlash\n");
			break;
		case SPGreen:
			printf("In Pedestrian current state: SPGreen\n");
			break;
		case SPFlash:
			printf("In Pedestrian current state: SPFlash\n");
			break;
		case EPGreen:
			printf("In Pedestrian current state: EPGreen\n");
			break;
		case EPFLash:
			printf("In Pedestrian current state: EPFlash\n");
			break;
		case WPGreen:
			printf("In Pedestrian current state: WPGreen\n");
			break;
		case WPFlash:
			printf("In Pedestrian current state: WPFlash\n");
			break;
		case NP_SPGreen:
			printf("In Pedestrian current state: NP_SPGreen\n");
			break;
		case NP_SPFlash:
			printf("In Pedestrian current state: NP_SPFlash\n");
			break;
		case EP_WPGreen:
			printf("In Pedestrian current state: EP_WPGreen\n");
			break;
		case EP_WPFlash:
			printf("In Pedestrian current state: EP_WPFlash\n");
			break;
		default:
			printf("Pedestrian State Not Handled, value was: %d", localState);
			break;
	}
}


void TrafficLightPrintState(enum trafficLightStates *CurState){
	//printf("In Do Something 1\n");
	int localState = *CurState;
	//printf("In raw state: %d\n", localState);
	switch (*CurState){
		case NSEWRed_NorthSouth:
			printf("In current state: NSEWRed_NorthSouth\n");
			break;
		case NSEWRed_NorthSouth_Special_NoTrain:
			printf("In current state: NSEWRed_NorthSouth_Special_NoTrain\n");
			break;
		case NSEWRed_NorthSouth_Special_TrainCond:
			printf("In current state: NSEWRed_NorthSouth_Special_TrainCond\n");
			break;
		case NSEWRed_EastWest:
			printf("In current state: NSEWRed_EastWest\n");
			break;
		case NSEWRed_EastWest_Special_NoTrain:
			printf("In current state: NSEWRed_EastWest_Special_NoTrain\n");
			break;
//		case NSEWRed_EastWest_Special_TrainCond:
//			printf("In current state: NSEWRed_EastWest_Special_TrainCond\n");
//			break;
		case NSGreen:
			printf("In current state: NSGreen\n");
			break;
		case NSYellow:
			printf("In current state: NSYellow\n");
			break;
		case EWGreen:
			printf("In current state: EWGreen\n");
			break;
		case EWYellow:
			printf("In current state: EWYellow\n");
			break;
		case NR_SRGreen:
			printf("In current state: NR_SRGreen\n");
			break;
		case NR_SRYellow:
			printf("In current state: NR_SRYellow\n");
			break;
		case ER_WRGreen:
			printf("In current state: ER_WRGreen\n");
			break;
		case N_NRGreen:
			printf("In current state: N_NRGreen\n");
			break;
		case NGreen_NRYellow:
			printf("In current state: NGreen_NRYellow\n");
			break;
		case NGreen:
			printf("In current state: NGreen\n");
			break;
		case S_SRGreen:
			printf("In current state: S_SRGreen\n");
			break;
		case SGreen_SRYellow:
			printf("In current state: SGreen_SRYellow\n");
			break;
		case SYellow:
			printf("In current state: SYellow\n");
			break;
		case SGreen:
			printf("In current state: SGreen\n");
			break;
		case E_ERGreen:
			printf("In current state: E_ERGreen\n");
			break;
		case EGreen_ERYellow:
			printf("In current state: EGreen_ERYellow\n");
			break;
		case EGreen:
			printf("In current state: EGreen\n");
			break;
		case W_WRGreen:
			printf("In current state: W_WRGreen\n");
			break;
		case WGreen_WRYellow:
			printf("In current state: WGreen_WRYellow\n");
			break;
		case WGreen:
			printf("In current state: WGreen\n");
			break;
		case ER_WRYellow:
			printf("In current state: ER_WRYellow\n");
			break;
		default:
			printf("State Not Handled, Value is: %d\n", localState);
			break;
	}
}

void button_CondVar(thread_data * condition_Data, int * buttonRead)
{	//Used for going into the condition variable setion

	pthread_mutex_lock(&condition_Data->button_Mutex);
	while (!condition_Data->buttonPressed)
	{
		pthread_cond_wait(&condition_Data->button_CondVar,
				&condition_Data->button_Mutex);

		//if(!condition_Data->serverUpdate){ continue; }
	}

	*buttonRead = condition_Data->buttonData;
	condition_Data->buttonPressed = 0;

	pthread_cond_signal(&condition_Data->button_CondVar);
	pthread_mutex_unlock(&condition_Data->button_Mutex);
}

void server_CondVar(thread_data * update_Data, int * buttonRead)
{	//Used for going into the condition variable setion
	pthread_mutex_lock(&update_Data->update_Mutex);

	while (!update_Data->serverUpdate)
	{
		pthread_cond_wait(&update_Data->update_CondVar,
				&update_Data->update_Mutex);
	}

	*buttonRead = update_Data->buttonData;
	update_Data->buttonPressed = 0;
	update_Data->serverUpdate = 0;

	pthread_cond_signal(&update_Data->update_CondVar);
	pthread_mutex_unlock(&update_Data->update_Mutex);
}

void *button_thread(void *data) { //used to read the button
	printf("-----------Button Thread Started\n");
	thread_data * button_Resources = (thread_data*) data;
	int button_Press;

	while (1) {
		scanf("%d", &button_Press); //wait for user input
		pthread_mutex_lock(&button_Resources->button_Mutex);
		/*
		while (button_Resources->buttonPressed) {
			//printf("Button Wait\n");
			pthread_cond_wait(&button_Resources->button_CondVar,
					&button_Resources->button_Mutex);
		}
	*/
	//	button_Resources->buttonData = button_Press;
	//	button_Resources->buttonPressed = 1;

		pthread_cond_signal(&button_Resources->button_CondVar);
		pthread_mutex_unlock(&button_Resources->button_Mutex);


		pthread_mutex_lock(&button_Resources->update_Mutex);
		pthread_cond_signal(&button_Resources->update_CondVar);
		button_Resources->buttonPressed = 1;
		button_Resources->serverUpdate = 1;
		pthread_mutex_unlock(&button_Resources->update_Mutex);

	}
}


void *menuFSM_thread(void*data)	{
	printf("---------Menu Thread Started\n");

	thread_data * FSM_Resources = (thread_data*) data; //contains mutexs and etc to for resource sharing

	enum controllerFSM FSM = main_Menu; //State machine to be used
	int buttonInput; //hold buttoninput
	char node_String[20] = "yes";



	while (1) {
		switch (FSM) {
		case main_Menu:
			printf("Main Menu State:\n-Status Display:1\n-Control Status:2\n\n");
			/*
			 * Main Menu State
			 * -Status Display:1
			 * -Control Status:2
			 */

			button_CondVar(FSM_Resources, &buttonInput);

			if (buttonInput == 1)
			{
				FSM = status_Display;
			} else if (buttonInput == 2)
			{
				FSM = control_Menu;
			}
			break;

		case status_Display:
			printf("Status Display State:\n-Connection Status:1\n-Functional Status:2\n-Back:9\n\n");
			/*
			 * Status Display State
			 * -Connection Status:1
			 * -Functional Status:2
			 * -Back:9
			 */
			button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 1)
			{
				FSM = conn_Status;
			}
			else if(buttonInput == 2)
			{
				FSM = func_Status;
			}
			else if (buttonInput == 9)
			{
				FSM = main_Menu;
			}

			break;
		case conn_Status:
			printf("Connection Status\n");
			printf("-Node 1: Status| Node 2: Status\n");
			printf("-Node 3: Status| Back:9\n\n");
			/*
			 *Connection Status
			 *-Node 1: Status| Node 2: Status
			 *-Node 3: Status| Back:9
			 */
			button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 9) {
				FSM = status_Display;
			}

			break;

		case func_Status:
			//printf("Functional Status\n-Node 1: EW| Node 2: E\n-Node 3: N| Back: 9\n\n");
			/*
			 *Functional Status
			 *-Node 1: EW| Node 2: E
			 *-Node 3: N| Back:9
			 */

			while(1)
			{
				server_CondVar(FSM_Resources, &buttonInput);

				printf("Functional Status Mode\n");
				if(	(buttonInput == 9))
				{
					FSM = status_Display;
					break;
				}
				else
				{
					TrafficLightPrintState(&FSM_Resources->nodeParameter.data[2]);
					printf("Entity ID: %d, State: %d\n", FSM_Resources->nodeParameter.data[1], FSM_Resources->nodeParameter.data[2]);
				}
			}

			break;

		case control_Menu:
			printf("Control Menu State:\n");
			printf("-Get Schedule/Mode:1\n");
			printf("-Set Schedule/Mode:2\n");
			printf("Back:9\n-Scroll Down:0\n\n");
			/*
			 * Control Menu State
			 * -Get Schedule/Mode:1
			 * -Set Schedule/Mode:2
			 * -Back:9
			 * -Scroll Down:0
			 */

			button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 1)
			{
				FSM = get_Sched_or_Mode;
			}
			else if(buttonInput == 2)
			{
				FSM = set_Sched_or_Mode;
			}
			else if (buttonInput == 9)
			{
				FSM = main_Menu;
			}
			else if (buttonInput == 0)
			{
				FSM = control_Menu2;
			}
			break;

		case get_Sched_or_Mode:
			printf("Get Schedule Mode\n");
			button_CondVar(FSM_Resources, &buttonInput);

			if(buttonInput == 9)
			{
				FSM = control_Menu;
			}
			break;

		case set_Sched_or_Mode:
			printf("Set Schedule: 1\n");
			printf("Set Mode: 2\n");
			printf("Back: 9\n");
		//	printf("What mode?\nNight Mode:1\nDefault Mode:2\nPeak Mode:3\n\n");
			button_CondVar(FSM_Resources, &buttonInput);
			if(buttonInput == 1)
			{
				FSM = set_Sched_or_Mode;
			}
			else if(buttonInput == 9)
			{
				FSM = control_Menu;
			}
			break;

		case control_Menu2:
			printf(	"Control Menu State\n");
			printf("-Force/Cancel State:1\n");
			printf("-Force/Cancel Mode:2\n-Back:9\n-Scroll Up:0\n\n");
			/*
			 * Control Menu State
			 * -Force/Cancel State:1
			 * -Force/Cancel Mode:2
			 * -Back:9
			 * -Scroll Up:0
			 */
			button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 9)
			{
				FSM = control_Menu2;
			}

			break;

		case Force_or_Canc_State:
			printf("Force State: 1\n");
			printf("Cancel State: 2\n");
			printf("Back: 9\n");


			button_CondVar(FSM_Resources, &buttonInput);

			if (buttonInput == 9)
			{
				FSM = control_Menu2;
			}

			break;

		case Force_or_Canc_Mode:
			printf("Force Mode: 1\n");
			printf("Canc Mode: 1\n");
			printf("Back: 9\n");
			button_CondVar(FSM_Resources, &buttonInput);
			break;

		default:
			printf("------------Error in state\n, buttonInput is: %d\n", buttonInput);
			break;
		}

	}
}


void *clientThread_Send1(void *data)  {
	thread_data * Command_Resources = (thread_data*)data;
	packet_data commandPacket1;
	packet_reply_data replyData;

	int packet_Input;

	commandPacket1.ClientID = 1; // unique number for this client (optional)

	int serverConn;

	if ((serverConn = name_open(CC_ATTACH_POINT, 0)) == -1) {
		printf("\n    ERROR, could not connect to server!\n\n");
		return EXIT_FAILURE;
	}

	commandPacket1.hdr.type = 0x00;
	commandPacket1.hdr.subtype = 0x00;

	while (1) {
		//scanf("%d", &packet_Input);
		///do condvarrrrrrrrrrrrrrrrrrrrrrr



		fflush(stdout);
		if (MsgSend(serverConn, &commandPacket1, sizeof(commandPacket1), &replyData, sizeof(replyData)) == -1)
		{
		}
		else
		{
			/*
			if(replyData.reply==0x81)
			{
				printf("Successfully Completed\n");
			}
			else
			{
				printf("Command could not be completed\n");
			}
			 */
		}
	}
	// Close the connection

	printf("\n Sending message to server to tell it to close the connection\n");

	name_close(serverConn);

	return EXIT_SUCCESS;
}


void *serverThread(void *data)  {
	thread_data * Update_Resources = (thread_data*)data;

	packet_data dataReceive_Server; //packet to send to server
	packet_reply_data replyData_Server; //packet to send to server

	int server_Recv = 0; // receive code when channel signal comes back

	name_attach_t * serverAttach;

   if ((serverAttach = name_attach(NULL, CC_ATTACH_POINT, 0)) == NULL)
   {
	   printf("\n Possibly another server with the same name is already running !\n");
	   return EXIT_FAILURE;
   }


   replyData_Server.hdr.type = 0x01;
   replyData_Server.hdr.subtype = 0x00;
   //replyData_Server.packetData = 0x81;

   while (1)
   {
	   /*
	   pthread_mutex_lock(&Update_Resources->update_Mutex);
	    while(Update_Resources->newUpdate)
	    {
	    	pthread_cond_signal(&Update_Resources->update_CondVar);
	    }
	    Update_Resources->newUpdate = 1;
	    */


	   printf("\n--------Server: Now waiting\n");
	   server_Recv = MsgReceive(serverAttach->chid, &dataReceive_Server, sizeof(dataReceive_Server), NULL);

	   if (server_Recv == -1)
	   {
		   printf("\nFailed to dataReceive_ServerReceive\n");
		   break;
	   }

	   if (server_Recv == 0)
	   {
		   switch (dataReceive_Server.hdr.code)
		   {
				   default:
				   // Some other pulse sent by one of your processes or the kernel
				   printf("\nServer got some other pulse %d\n", dataReceive_Server.hdr.code);
				   break;

		   }
		   continue;// go back to top of while loop
	   }

	   // for messages:
	   if(server_Recv > 0) // if true then A message was received
	   {

		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (dataReceive_Server.hdr.type == _IO_CONNECT )
		   {
			   MsgReply( server_Recv, EOK, NULL, 0 );
			   printf("\n gns service is running....");
			   continue;
		   }

		   // Some other I/O message was received; reject it
		   if (dataReceive_Server.hdr.type > _IO_BASE && dataReceive_Server.hdr.type <= _IO_MAX )
		   {
			   MsgError( server_Recv, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   continue;	// go back to top of while loop
		   }

		   uint8_t PacketID;
		   PacketID = dataReceive_Server.data[0];

		   Update_Resources->nodeParameter = dataReceive_Server;
		   MsgReply(server_Recv, EOK, &replyData_Server, sizeof(replyData_Server));

		   pthread_mutex_lock(&Update_Resources->update_Mutex);
		   Update_Resources->serverUpdate = 1;
		   pthread_cond_signal(&Update_Resources->update_CondVar);
		   pthread_mutex_unlock(&Update_Resources->update_Mutex);

	   }
	   else
	   {
		   printf("\nERROR: Server received something, but could not handle it correctly\n");
	   }
	   //pthread_mutex_unlock(&Update_Resources->update_Mutex);
   }

   // Remove the attach point name from the file system (i.e. /dev/name/local/<myname>)
   name_detach(serverAttach, 0);


	printf("\n Sending message to server to tell it to close the connection\n");
	return EXIT_SUCCESS;
}


#define AM335X_CONTROL_MODULE_BASE   (uint64_t) 0x44E10000
#define AM335X_CONTROL_MODULE_SIZE   (size_t)   0x00001448
#define AM335X_GPIO_SIZE             (uint64_t) 0x00001000
#define AM335X_GPIO1_BASE            (size_t)   0x4804C000

#define LED0          (1<<21)   // GPIO1_21
#define LED1          (1<<22)   // GPIO1_22
#define LED2          (1<<23)   // GPIO1_23
#define LED3          (1<<24)   // GPIO1_24

#define SD0 (1<<28)  // SD0 is connected to GPIO1_28
#define SCL (1<<16)  // SCL is connected to GPIO1_16


#define GPIO_OE        0x134
#define GPIO_DATAIN    0x138
#define GPIO_DATAOUT   0x13C

#define GPIO_IRQSTATUS_SET_1 0x38   // enable interrupt generation
#define GPIO_IRQWAKEN_1      0x48   // Wakeup Enable for Interrupt Line
#define GPIO_FALLINGDETECT   0x14C  // set falling edge trigger
#define GPIO_CLEARDATAOUT    0x190  // clear data out Register
#define GPIO_IRQSTATUS_1     0x30   // clear any prior IRQs

#define GPIO1_IRQ 99  // TRG page 465 list the IRQs for the am335x


#define P9_12_pinConfig 0x878 //  conf_gpmc_ben1 (TRM pp 1364) for GPIO1_28,  P9_12

// GPMC_A1_Configuration
#define PIN_MODE_0   0x00
#define PIN_MODE_1   0x01
#define PIN_MODE_2   0x02
#define PIN_MODE_3   0x03
#define PIN_MODE_4   0x04
#define PIN_MODE_5   0x05
#define PIN_MODE_6   0x06
#define PIN_MODE_7   0x07

// PIN MUX Configuration strut values  (page 1420 from TRM)
#define PU_ENABLE    0x00
#define PU_DISABLE   0x01
#define PU_PULL_UP   0x01
#define PU_PULL_DOWN 0x00
#define RECV_ENABLE  0x01
#define RECV_DISABLE 0x00
#define SLEW_FAST    0x00
#define SLEW_SLOW    0x01

typedef union _CONF_MODULE_PIN_STRUCT   // See TRM Page 1420
{
  unsigned int d32;
  struct {    // name: field size
           unsigned int conf_mmode : 3;       // LSB
           unsigned int conf_puden : 1;
           unsigned int conf_putypesel : 1;
           unsigned int conf_rxactive : 1;
           unsigned int conf_slewctrl : 1;
           unsigned int conf_res_1 : 13;      // reserved
           unsigned int conf_res_2 : 12;      // reserved MSB
         } b;
} _CONF_MODULE_PIN;

void strobe_SCL(uintptr_t gpio_port_add) {
   uint32_t PortData;
   PortData = in32(gpio_port_add + GPIO_DATAOUT);// value that is currently on the GPIO port
   PortData &= ~(SCL);
   out32(gpio_port_add + GPIO_DATAOUT, PortData);// Clock low
   delaySCL();

   PortData  = in32(gpio_port_add + GPIO_DATAOUT);// get port value
   PortData |= SCL;// Clock high
   out32(gpio_port_add + GPIO_DATAOUT, PortData);
   delaySCL();
}

// Thread used to Flash the 4 LEDs on the BeagleBone for 100ms
void *Flash_LED0_ex(void *notused)
{
	pthread_detach(pthread_self());  // no need for this thread to join
	uintptr_t gpio1_port = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	uintptr_t val;
	// Write GPIO data output register
	val  = in32(gpio1_port + GPIO_DATAOUT);
	val |= (LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	usleep(100000);  // 100 ms wait
	//sched_yield();  // if used without the usleep, this line will flash the LEDS for ~4ms

	val  = in32(gpio1_port + GPIO_DATAOUT);
	val &= ~(LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	munmap_device_io(gpio1_port, AM335X_GPIO_SIZE);

}

void delaySCL()  {// Small delay used to get timing correct for BBB
  volatile int i, a;
  for(i=0;i<0x1F;i++) // 0x1F results in a delay that sets F_SCL to ~480 kHz
  {   // i*1 is faster than i+1 (i+1 results in F_SCL ~454 kHz, whereas i*1 is the same as a=i)
     a = i;
  }
  // usleep(1);  //why doesn't this work? Ans: Results in a period of 4ms as
  // fastest time, which is 250Hz (This is to slow for the TTP229 chip as it
  // requires F_SCL to be between 1 kHz and 512 kHz)
}

uint32_t KeypadReadIObit(uintptr_t gpio_base, uint32_t BitsToRead)  {
   volatile uint32_t val = 0;
   val  = in32(gpio_base + GPIO_DATAIN);// value that is currently on the GPIO port

   val &= BitsToRead; // mask bit
   //val = val >> (BitsToRead % 2);
   //return val;
   if(val==BitsToRead)
	   return 1;
   else
	   return 0;
}

void DecodeKeyValue(uint32_t word, int *keypadData)
{
	switch(word)
	{
		case 0x01:
			printf("Key  1 pressed\n");
			*keypadData = 1;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x02:
			printf("Key  2 pressed\n");
			*keypadData = 2;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x04:
			printf("Key  3 pressed\n");
			*keypadData = 3;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x08:
			printf("Key  4 pressed\n");
			*keypadData = 4;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x10:
			printf("Key  5 pressed\n");
			*keypadData = 5;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x20:
			printf("Key  6 pressed\n");
			*keypadData = 6;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x40:
			printf("Key  7 pressed\n");
			*keypadData = 7;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x80:
			printf("Key  8 pressed\n");
			*keypadData = 8;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x100:
			printf("Key  9 pressed\n");
			*keypadData = 9;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x200:
			printf("Key 10 pressed\n");
			*keypadData = 10;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x400:
			printf("Key 11 pressed\n");
			*keypadData = 11;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x800:
			printf("Key 12 pressed\n");
			*keypadData = 12;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x1000:
			printf("Key 13 pressed\n");
			*keypadData = 13;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x2000:
			printf("Key 14 pressed\n");
			*keypadData = 14;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x4000:
			printf("Key 15 pressed\n");
			*keypadData = 15;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x8000:
			printf("Key 16 pressed\n");
			*keypadData = 16;
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x00:  // key release event (do nothing)
			break;
		default:
			printf("Key pressed could not be determined - %lu\n", word);
	}
}
/*
 *
 *
 * Here is the ISR
 *
 *
 *
 */
typedef struct
{
	int count_thread;
	uintptr_t gpio1_base;
	struct sigevent pevent; // remember to fill in "event" structure in main
}ISR_data;

// create global struct to share data between threads
ISR_data ISR_area_data;

const struct sigevent* Inthandler( void* area, int id )
{
	// 	"Do not call any functions in ISR that call kernerl - including printf()
	//struct sigevent *pevent = (struct sigevent *) area;
	ISR_data *p_ISR_data = (ISR_data *) area;

	InterruptMask(GPIO1_IRQ, id);  // Disable all hardware interrupt

	// must do this in the ISR  (else stack over flow and system will crash
	out32(p_ISR_data->gpio1_base + GPIO_IRQSTATUS_1, SD0); //clear IRQ

	// do this to tell us how many times this handler gets called
	p_ISR_data->count_thread++;
	// got IRQ.
	// work out what it came from

    InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

    // return a pointer to an event structure (preinitialized
    // by main) that contains SIGEV_INTR as its notification type.
    // This causes the InterruptWait in "int_thread" to unblock.
	return (&p_ISR_data->pevent);
}

uintptr_t gpio1_base;
uintptr_t control_module;
//volatile uint32_t val = 0;

int keypadSetup(){
		control_module = mmap_device_io(AM335X_CONTROL_MODULE_SIZE,
			  AM335X_CONTROL_MODULE_BASE);
	  gpio1_base = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	  // initalise the global stuct
	  ISR_area_data.count_thread = 0;
	  ISR_area_data.gpio1_base = gpio1_base;

	  memset(&ISR_area_data.pevent, 0, sizeof(ISR_area_data.pevent));
	  SIGEV_INTR_INIT (&ISR_area_data.pevent);
	  ISR_area_data.pevent.sigev_notify = SIGEV_INTR;  // Setup for external interrupt

		// we also need to have the PROCMGR_AID_INTERRUPT and PROCMGR_AID_IO abilities enabled. For more information, see procmgr_ability().
	  ThreadCtl( _NTO_TCTL_IO_PRIV , 1);// Request I/O privileges  for QNX7;

	  procmgr_ability( 0, PROCMGR_AID_INTERRUPT | PROCMGR_AID_IO);

	  volatile uint32_t val = 0;

	  if( (control_module)&&(gpio1_base) )
	  {
		// set DDR for LEDs to output and GPIO_28 to input
		val = in32(gpio1_base + GPIO_OE); // read in current setup for GPIO1 port
		val |= 1<<28;                     // set IO_BIT_28 high (1=input, 0=output)
		out32(gpio1_base + GPIO_OE, val); // write value to input enable for data pins
		val &= ~(LED0|LED1|LED2|LED3);    // write value to output enable
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for LED pins

		val = in32(gpio1_base + GPIO_OE);
		val &= ~SCL;                      // 0 for output
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for data pins

		val = in32(gpio1_base + GPIO_DATAOUT);
		val |= SCL;              // Set Clock Line High as per TTP229-BSF datasheet
		out32(gpio1_base + GPIO_DATAOUT, val); // for 16-Key active-Low timing diagram

		in32s(&val, 1, control_module + P9_12_pinConfig );
		printf("Original pinmux configuration for GPIO1_28 = %#010x\n", val);

		// set up pin mux for the pins we are going to use  (see page 1354 of TRM)
		volatile _CONF_MODULE_PIN pinConfigGPMC; // Pin configuration strut
		pinConfigGPMC.d32 = 0;
		// Pin MUX register default setup for input (GPIO input, disable pull up/down - Mode 7)
		pinConfigGPMC.b.conf_slewctrl = SLEW_SLOW;    // Select between faster or slower slew rate
		pinConfigGPMC.b.conf_rxactive = RECV_ENABLE;  // Input enable value for the PAD
		pinConfigGPMC.b.conf_putypesel= PU_PULL_UP;   // Pad pullup/pulldown type selection
		pinConfigGPMC.b.conf_puden = PU_ENABLE;       // Pad pullup/pulldown enable
		pinConfigGPMC.b.conf_mmode = PIN_MODE_7;      // Pad functional signal mux select 0 - 7

		// Write to PinMux registers for the GPIO1_28
		out32(control_module + P9_12_pinConfig, pinConfigGPMC.d32);
		in32s(&val, 1, control_module + P9_12_pinConfig);   // Read it back
		printf("New configuration register for GPIO1_28 = %#010x\n", val);

		// Setup IRQ for SD0 pin ( see TRM page 4871 for register list)
		out32(gpio1_base + GPIO_IRQSTATUS_SET_1, SD0);	// Write 1 to GPIO_IRQSTATUS_SET_1
		out32(gpio1_base + GPIO_IRQWAKEN_1, SD0);    	// Write 1 to GPIO_IRQWAKEN_1
		out32(gpio1_base + GPIO_FALLINGDETECT, SD0);    // set falling edge
		out32(gpio1_base + GPIO_CLEARDATAOUT, SD0);     // clear GPIO_CLEARDATAOUT
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);      // clear any prior IRQs
		// all good to go periperhals can be set
		return EXIT_SUCCESS;
	}
	  else{
		  // periperhals cannot be initialized
		return EXIT_FAILURE;
	}

}

void *interruptKeyPadThread(void *data)
{
	thread_data * Key_Resources = (thread_data*)data;
	// Main code starts here
	//The thread that calls InterruptWait() must be the one that called InterruptAttach().
	//    id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK | _NTO_INTR_FLAGS_NO_UNMASK | _NTO_INTR_FLAGS_END);
	 int id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK );

	InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

	volatile uint32_t val = 0;

	int i = 0;
	printf( "Main entering loop and will call InterruptWait\n");
	for(;;)
	{
		// Block main until we get a sigevent of type: 	ISR_area_data.pevent
		InterruptWait( 0, NULL );   // block this thread until an interrupt occurs  (Wait for a hardware interrupt)
		InterruptDisable();
		// printf("do interrupt work here...\n");

		volatile uint32_t word = 0;
		//  confirm that SD0 is still low (that is a valid Key press event has occurred)
		val = KeypadReadIObit(gpio1_base, SD0);  // read SD0 (means data is ready)

		if(val == 0)  // start reading key value form the keypad
		{
			 word = 0;  // clear word variable

			 delaySCL(); // wait a short period of time before reading the data Tw  (10 us)

			 for(i=0;i<16;i++)           // get data from SD0 (16 bits)
			 {
				strobe_SCL(gpio1_base);  // strobe the SCL line so we can read in data bit

				val = KeypadReadIObit(gpio1_base, SD0); // read in data bit
				val = ~val & 0x01;                      // invert bit and mask out everything but the LSB
				//printf("val[%u]=%u, ",i, val);
				word = word | (val<<i);  // add data bit to word in unique position (build word up bit by bit)
			 }
			 //printf("word=%u\n",word);
			 pthread_mutex_lock(&Key_Resources->button_Mutex);
			 DecodeKeyValue(word, &Key_Resources->buttonData);

			 pthread_mutex_unlock(&Key_Resources->button_Mutex);
			 //printf("Button Value: %i\n", Key_Resources->buttonData);
			 //printf("Interrupt count = %i\n", ISR_area_data.count_thread);
		}
		//sched_yield();
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);
		InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt
		InterruptEnable();
	}
	 munmap_device_io(control_module, AM335X_CONTROL_MODULE_SIZE);
}
