/*
 * Traffic_Light_System_Def.h
 *
 *  Created on: 12Oct.,2018
 *      Author: Michael Christoe
 */

#ifndef SRC_TRAFFIC_LIGHT_SYSTEM_DEF_H_
#define SRC_TRAFFIC_LIGHT_SYSTEM_DEF_H_

// hostname RMIT_BBB_v5_07 // 192.168.201.138 (next to chris)
// hostname RMIT_BBB_v5_06 // 192.168.201.71
// hostname RMIT_BBB_v5_03 // 192.168.201.24
// hostname RMIT_BBB_v5_09 // 192.168.201.149

#define RW_ATTACH_POINT "/net/RMIT_BBB_v5_09/dev/name/local/RailwayCrossing"
#define CC_ATTACH_POINT  "CentralControl"  // hostname using full path, change myname to the name used for server
#define TL1_ATTACH_POINT  "/net/RMIT_BBB_v5_06/dev/name/local/TrafficNode1"  // hostname using full path, change myname to the name used for server
#define TL2_ATTACH_POINT  "/net/RMIT_BBB_v5_03/dev/name/local/TrafficNode2"  // hostname using full path, change myname to the name used for server
#define BUF_SIZE 100

#define STATUS_UPDATE_ID			0xFF
#define COMMAND_FORCE_STATE			0x01
#define COMMAND_FORCE_STATE_CANCEL	0x02
#define COMMAND_FORCE_MODE			0x03
#define COMMAND_FORCE_MODE_CANCEL	0x04
#define COMMAND_GET_MODE_SCHED		0x05
#define COMMAND_SET_MODE_SCHED		0x06
#define ACK_FORCE_STATE				0x81
#define ACK_FORCE_STATE_CANCEL		0x82
#define ACK_FORCE_MODE				0x83
#define ACK_FORCE_MODE_CANCEL		0x84
#define ACK_GET_MODE_SCHED			0x85
#define ACK_SET_MODE_SCHED			0x86

#define MODE_ID_DEFAULT 0x01
#define MODE_ID_PEAK_TIME 0x02
#define MODE_ID_NIGHT_TIME 0x03

#define ENTITY_CC	0x01
#define ENTITY_TL1	0x02
#define ENTITY_TL2	0x03
#define ENTITY_RW	0x04

enum pedStates{
	NSEWPRed,
	NPGreen,
	NPFlash,
	SPGreen,
	SPFlash,
	EPGreen,
	EPFLash,
	WPGreen,
	WPFlash,
	NP_SPGreen,
	NP_SPFlash,
	EP_WPGreen,
	EP_WPFlash
};

enum trafficLightStates {
	NSEWRed_NorthSouth,
	NSEWRed_NorthSouth_Special_NoTrain,
	NSEWRed_NorthSouth_Special_TrainCond,
	NSEWRed_EastWest,
	NSEWRed_EastWest_Special_NoTrain,
	//NSEWRed_EastWest_Special_TrainCond,
	NSGreen,
	NSYellow,
	EWGreen,
	EWYellow,
	NR_SRGreen,
	NR_SRYellow,
	ER_WRGreen,
	ER_WRYellow,
	N_NRGreen,
	NGreen_NRYellow,
	NGreen,
	S_SRGreen,
	SGreen_SRYellow,
	SYellow,
	SGreenNoTrain,
	SGreenTrain,
	E_ERGreen,
	EGreen_ERYellow,
	EGreenNoTrain,
	EGreenTrain,
	W_WRGreen,
	WGreen_WRYellow,
	WGreen
};

enum Rail_Crossing_States {
	Train_Present,
	Train_NotPresent
};

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
    uint8_t data[BUF_SIZE];     // our data
} packet_data; //


typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
    uint8_t data[BUF_SIZE];// Message we send back to clients to tell them the messages was processed correctly.
} packet_reply_data;

#endif /* SRC_TRAFFIC_LIGHT_SYSTEM_DEF_H_ */
