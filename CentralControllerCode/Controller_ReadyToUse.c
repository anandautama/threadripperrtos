#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <sys/procmgr.h>
#include <hw/inout.h>      // for in32() and out32();
#include <sys/mman.h>      // for mmap_device_io();
#include <stdint.h>        // for unit32 types
#include <share.h>
#include <time.h>
#include <errno.h>

#include "stdint.h"
#include <devctl.h>
#include <hw/i2c.h>
#include <unistd.h>

#include "Traffic_Light_System_Def.h"

#define MY_PULSE_CODE  _PULSE_CODE_MINAVAIL //Pulse for timer
#define BUF_SIZE 100

//for lcd
#define DATA_SEND 0x40  // sets the Rs value high
#define Co_Ctrl   0x00  // mode to tell LCD we are sending a single command


enum controllerFSM
{ //State machine for menu
	main_Menu, //Initial main menu to go into control or status menu
	status_Display, //Choose which status to show
	conn_Status, //Show connection status
	func_Status, //Show functional status
	func_StatusTrain,
	control_Menu, //Menu for selecting schedule or mode
	control_Menu2,
	force_Setting, //Enter force mode
	schedule_Setting, //Set schedule, has nested state machine here
	mode_Setting, //Set mode, has nested state machine here
};

enum forceFSM
{
	force_SelectNode,
	force_TL,
	force_Train,
	force_Success,
	force_Failure,
	force_TL_Menu
};

enum scheduleFSM
{ //FSM for setting the schedule
	schedule_SelectNode, //Select which node to use schedule on
	select_Schedule, //Select set, get, or cancel schedule
	get_Schedule, //receive the node's schedule
	send_Schedule,
	edit_Schedule, //success
	cancel_Schedule, //cancel the schedule on the node
	set_Schedule //set the schedule on the node
};

enum modeFSM
{ //FSM for setting the mode
	mode_SelectNode, //Select which node
	train_Mode, //select train node options
	traffic_Mode, //select traffic node options
	mode_Success, //Mode that outputs information upon success
	mode_Failure //Mode that outputs information upon failure
};

typedef union { //used for getting pulse for timer

	struct _pulse pulse;
} timer_data;


typedef struct {
	pthread_mutex_t button_Mutex; // add a mutex object into the structure we want to protect
	pthread_mutex_t command_Mutex; // add a mutex object into the structure we want to protect
	pthread_mutex_t update_Mutex; // add a mutex object into the structure we want to protect
	pthread_mutex_t reply_Mutex; // mutex for reply section on client thread
	pthread_mutex_t error_Mutex; // error mutex for error vale
	pthread_mutex_t i2c_Mutex;


	pthread_cond_t button_CondVar; //cond variable for keypad
	pthread_cond_t command_CondVar[3]; // cond variable for client thread for sending commands
	pthread_cond_t update_CondVar; // cond variable for server thread
	pthread_cond_t reply_CondVar[3]; //cond variable at end of client to set reply packet


	int channel_ID; //ID of established channel for something
	int buttonPressed; //conditional variable used for checking if button has been pressed
	int newCommand[3]; //used for consumer/producer variable for condvar for client
	int serverUpdate; //used for consumer/producer variable for condvar for server
	int replyReceived; //used at end of client thread to put packet into resource

	int buttonData; //button data shared between button press thread and FSM
	int *keypadData; //value from interrupt interpreted from data


	packet_data nodeParameter; //used to send commands and get updates
	packet_reply_data replyData; //reply back to clients

	int errorSend[3]; //Used to tell if error in sending

	int I2C_Reference;
	int I2C_Address;
	int I2C_Mode;
	char I2C_String[20];
	int cursorX;
	int cursorY;
	int I2C_BufferSize;


} thread_data;

typedef struct {
	int thread_num;
	thread_data * resource_data;
	const char *attach_point;
	int resource;
	int connectionStatus;
} client_data;


void PedStatePrintState(enum pedStates *CurState);
void TrafficLightPrintState(enum trafficLightStates *CurState, thread_data * TLData);

void *button_thread(void *data); //thread for console input
void *menuFSM_thread(void*data); //thread for finite state machien
void *clientThread_Send1(void *data); //thread for CC act as controller and send to server
void *serverThread(void *data); //server thread for receiving updates

// hardware specific defines
int buttonSetup();
const struct sigevent* Inthandler( void* area, int id );
void *interruptKeyPadThread(void *data);
void LCDthread_ex (thread_data *td);
void LCDthread_ex_lineDynamic (thread_data *tdDynamic);

int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData);
void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column);
void Initialise_LCD (int fd, _Uint32t LCDi2cAdd);


int main(int argc, char *argv[]) {
	pthread_t serverpthread; //pthread for server
	pthread_t keyPadThread; //pthread for keypad thread
	pthread_t clientSend0, clientSend1, clientSend2, clientSend[3]; //pthread for sending commands
	pthread_t stateMachine_pthread; //pthread for the menu thread
	pthread_t button_pthread; //pthread for the button
	thread_data resource_data;

	void *pthread_end;

	//Initialization
	pthread_mutex_init(&resource_data.button_Mutex, NULL); //mutex for button press
	pthread_mutex_init(&resource_data.update_Mutex, NULL); //mutex for update server
	pthread_mutex_init(&resource_data.command_Mutex, NULL); //mutex for sending commands to tarffic nodes
	pthread_mutex_init(&resource_data.reply_Mutex, NULL); //mutex for reply on server
	pthread_mutex_init(&resource_data.error_Mutex, NULL);
	pthread_mutex_init(&resource_data.i2c_Mutex, NULL);

	pthread_cond_init(&resource_data.button_CondVar,NULL);
	pthread_cond_init(&resource_data.update_CondVar,NULL); //--
	pthread_cond_init(&resource_data.command_CondVar[0],NULL); //--
	pthread_cond_init(&resource_data.command_CondVar[1],NULL); //--
	pthread_cond_init(&resource_data.command_CondVar[2],NULL); //--
	pthread_cond_init(&resource_data.reply_CondVar,NULL);

	resource_data.buttonPressed = 0; //Set to 0 for button mutex to button hasnt been pressed yet
	resource_data.newCommand[0] = 0;
	resource_data.newCommand[1] = 0;
	resource_data.newCommand[2] = 0;
	resource_data.serverUpdate = 0; //used as consumer/producer for server ondvar
	resource_data.replyReceived = 0;


	client_data client_data[3];
	client_data[0].resource_data = &resource_data;
	client_data[0].attach_point = RW_ATTACH_POINT;
	client_data[0].thread_num = 0;
	client_data[1].resource_data = &resource_data;
	client_data[1].attach_point = TL1_ATTACH_POINT;
	client_data[1].thread_num = 1;
	client_data[2].resource_data = &resource_data;
	client_data[2].attach_point = TL2_ATTACH_POINT;
	client_data[2].thread_num = 2;
	client_data[0].connectionStatus = 0;
	client_data[1].connectionStatus = 0;
	client_data[2].connectionStatus = 0;


	///I2C
	int file;
	int error;
	volatile uint8_t LCDi2cAdd = 0x3C;
	_Uint32t speed = 10000; // nice and slow (will work with 200000)

	uint8_t	LCDdata[21] = {};

	// Open I2C resource and set it up
	if ((file = open("/dev/i2c1",O_RDWR)) < 0)	  // OPEN I2C1
		printf("Error while opening Device File.!!\n");
	else
		printf("I2C1 Opened Successfully\n");

	error = devctl(file,DCMD_I2C_SET_BUS_SPEED,&(speed),sizeof(speed),NULL);  // Set Bus speed
	if (error)
	{
		fprintf(stderr, "Error setting the bus speed: %d\n",strerror ( error ));
		exit(EXIT_FAILURE);
	}
	else
		printf("Bus speed set = %d\n", speed);

	pthread_mutex_lock(&resource_data.i2c_Mutex);
	resource_data.I2C_Reference = file;
	resource_data.I2C_Address = LCDi2cAdd;
	resource_data.I2C_Mode = DATA_SEND;
	resource_data.cursorX = 0;
	resource_data.cursorY = 0;
	pthread_mutex_unlock(&resource_data.i2c_Mutex);

	Initialise_LCD(file, LCDi2cAdd);

	//keypad initialization
	int result = keypadSetup();
	if(result == EXIT_SUCCESS) //check if keypad has been initialized
	{
		pthread_create(&keyPadThread, NULL, interruptKeyPadThread, &resource_data);
	}
	else
	{
		printf("Peripherals cannot be initialized\n");
	}

	pthread_create(&serverpthread, NULL, serverThread, &resource_data);
	usleep(1000000);
	pthread_create(&clientSend[0], NULL, clientThread_Send1, &client_data[0]);
	usleep(1000000);
	pthread_create(&clientSend[1], NULL, clientThread_Send1, &client_data[1]);
	usleep(1000000);
	pthread_create(&clientSend[2], NULL, clientThread_Send1, &client_data[2]);
	pthread_create(&stateMachine_pthread,NULL,menuFSM_thread,&resource_data);
	//pthread_create(&button_pthread,NULL,button_thread,&resource_data);

	pthread_join(keyPadThread, &pthread_end);
	pthread_join(clientSend[0], &pthread_end);
	pthread_join(clientSend[1], &pthread_end);
 	pthread_join(clientSend[2], &pthread_end);
	pthread_join(stateMachine_pthread,&pthread_end);
	pthread_join(serverpthread, &pthread_end);
	//pthread_join(button_pthread,&pthread_end);

	printf("\nMain Thread Terminating....");
	return EXIT_SUCCESS;
}

void TrafficLightPrintState(enum trafficLightStates *CurState, thread_data * TLData){
	//printf("In Do Something 1\n");
	int localState = *CurState;
	//printf("In raw state: %d\n", localState);

	switch (localState){
		case NSEWRed_NorthSouth:
			printf("In current state: NSEWRed_NorthSouth\n");
			sprintf(TLData->I2C_String,"NSEWRed_NorthSouth");
			break;
		case NSEWRed_NorthSouth_Special_NoTrain:
			printf("In current state: NSEWRed_NorthSouth_Special_NoTrain\n");
			sprintf(TLData->I2C_String,"NSEWRed_NS_SPNoTrain");
			break;
		case NSEWRed_NorthSouth_Special_TrainCond:
			printf("In current state: NSEWRed_NorthSouth_Special_TrainCond\n");
			sprintf(TLData->I2C_String,"NSEWRed_NS_SP_Train");
			break;
		case NSEWRed_EastWest:
			printf("In current state: NSEWRed_EastWest\n");
			sprintf(TLData->I2C_String,"NSEWRed_EastWest");
			break;
		case NSEWRed_EastWest_Special_NoTrain:
			printf("In current state: NSEWRed_EastWest_Special_NoTrain\n");
			sprintf(TLData->I2C_String,"NSEWRed_EW_SPNoTrain");
			break;
		case NSGreen:
			printf("In current state: NSGreen\n");
			sprintf(TLData->I2C_String,"NSGreen");
			break;
		case NSYellow:
			printf("In current state: NSYellow\n");
			sprintf(TLData->I2C_String,"NSYellow");
			break;
		case EWGreen:
			printf("In current state: EWGreen\n");
			sprintf(TLData->I2C_String,"EWGreen");
			break;
		case EWYellow:
			printf("In current state: EWYellow\n");
			sprintf(TLData->I2C_String,"EWYellow");
			break;
		case NR_SRGreen:
			printf("In current state: NR_SRGreen\n");
			sprintf(TLData->I2C_String,"NR_SRGreen");
			break;
		case NR_SRYellow:
			printf("In current state: NR_SRYellow\n");
			sprintf(TLData->I2C_String,"NR_SRYellow");
			break;
		case ER_WRGreen:
			printf("In current state: ER_WRGreen\n");
			sprintf(TLData->I2C_String,"ER_WRGreen");
			break;
		case N_NRGreen:
			printf("In current state: N_NRGreen\n");
			sprintf(TLData->I2C_String,"N_NRGreen");
			break;
		case NGreen_NRYellow:
			printf("In current state: NGreen_NRYellow\n");
			sprintf(TLData->I2C_String,"NGreen_NRYellow");
			break;
		case NGreen:
			printf("In current state: NGreen\n");
			sprintf(TLData->I2C_String,"NGreen");
			break;
		case S_SRGreen:
			printf("In current state: S_SRGreen\n");
			sprintf(TLData->I2C_String,"S_SRGreen");
			break;
		case SGreen_SRYellow:
			printf("In current state: SGreen_SRYellow\n");
			sprintf(TLData->I2C_String,"SGr_SRYellow");
			break;
		case SYellow:
			printf("In current state: SYellow\n");
			sprintf(TLData->I2C_String,"SYellow");
			break;
		case SGreenTrain:
			printf("In current state: SGreenTrain\n");
			sprintf(TLData->I2C_String,"SGreenTrain");
			break;
		case SGreenNoTrain:
			printf("In current state: SGreenNoTrain\n");
			sprintf(TLData->I2C_String,"SGreenNoTrain");
			break;
		case E_ERGreen:
			printf("In current state: E_ERGreen\n");
			sprintf(TLData->I2C_String,"E_ERGreen");
			break;
		case EGreen_ERYellow:
			printf("In current state: EGreen_ERYellow\n");
			sprintf(TLData->I2C_String,"EGr_ERYellow");
			break;
		case EGreenTrain:
			printf("In current state: EGreenTrain\n");
			sprintf(TLData->I2C_String,"EGreenTrain");
			break;
		case EGreenNoTrain:
			printf("In current state: EGreenNoTrain\n");
			sprintf(TLData->I2C_String,"EGreenNoTrain");
			break;
		case W_WRGreen:
			printf("In current state: W_WRGreen\n");
			sprintf(TLData->I2C_String,"W_WRGreen");
			break;
		case WGreen_WRYellow:
			printf("In current state: WGreen_WRYellow\n");
			sprintf(TLData->I2C_String,"WGr_WRYellow");
			break;
		case WGreen:
			printf("In current state: WGreen\n");
			sprintf(TLData->I2C_String,"WGreen");
			break;
		case ER_WRYellow:
			printf("In current state: ER_WRYellow\n");
			sprintf(TLData->I2C_String,"ER_WRYellow");
			break;
		default:
			printf("State Not Handled\n");
			sprintf(TLData->I2C_String,"Unknown");
			break;
	}
}


void *button_thread(void *data) { //used to read input from console
	printf("-----------Button Thread Started\n");
	thread_data * button_Resources = (thread_data*) data;
	int button_Press;

	while (1) {
		scanf("%d", &button_Press); //wait for user input
		pthread_mutex_lock(&button_Resources->button_Mutex);

		while (button_Resources->buttonPressed) {
			//printf("Button Wait\n");
			pthread_cond_wait(&button_Resources->button_CondVar,
					&button_Resources->button_Mutex);
		}

		button_Resources->buttonData = button_Press;
		button_Resources->buttonPressed = 1;

		pthread_cond_broadcast(&button_Resources->button_CondVar);
		pthread_mutex_unlock(&button_Resources->button_Mutex);
	}
}


void LCDthread_ex (thread_data *td)
{
	uint8_t	LCDdata[20] = {};
	SetCursor(td->I2C_Reference, td->I2C_Address,td->cursorY,td->cursorX); // set cursor on LCD to first position first line
	sprintf(LCDdata,td->I2C_String);
	I2cWrite_(td->I2C_Reference, td->I2C_Address, DATA_SEND, &LCDdata[0], 20);// write new data to I2C
}

void LCDthread_ex_lineDynamic (thread_data *tdDynamic)
{
	uint8_t	LCDdata[20] = {};
	SetCursor(tdDynamic->I2C_Reference, tdDynamic->I2C_Address,tdDynamic->cursorY,tdDynamic->cursorX); // set cursor on LCD to first position first line
	sprintf(LCDdata,tdDynamic->I2C_String);
	I2cWrite_(tdDynamic->I2C_Reference, tdDynamic->I2C_Address, DATA_SEND, &LCDdata[0], strlen(tdDynamic->I2C_String));		// write new data to I2C
}


/*
void LCDthread_ex (thread_data *td)
{
	pthread_detach(pthread_self());

	pthread_mutex_lock(&td->i2c_Mutex);     //lock the function to make sure the variables are protected
	uint8_t	LCDdata[19] = {};
	SetCursor(td->I2C_Reference, td->I2C_Address,td->cursorX,td->cursorY); // set cursor on LCD to first position first line
	sprintf(LCDdata,td->I2C_String);
	I2cWrite_(td->I2C_Reference, td->I2C_Address, DATA_SEND, &LCDdata[0], sizeof(LCDdata));		// write new data to I2C

	pthread_mutex_unlock(&td->i2c_Mutex);	//unlock the functions to release the variables for use by other functions
	usleep(1000000); // 1.0 seconds
}
*/
void *menuFSM_thread(void*data)	{
	printf("---------Menu Thread Started\n");

	thread_data * FSM_Resources = (thread_data*) data; //contains mutexs and etc to for resource sharing

	int statusMode = 0;

	enum modeFSM mode_FSM; //State machine for setting mode
	int modeInput; //Variable used for controlling state flow in modeFSM

	enum scheduleFSM schedule_FSM; //Statem acine for setting schedule
	int scheduleInput = 99; //Variable used for controlling state flow in scheduleFSM
	int schedulePacketEdit = 99; //declare which field to edit in edit_schedule case
	int inputBuffer; //Used to hold field if user want to put more numbers
	int editScheduleMultipler;// after every input multiple value by 10
	int locationIncrement = 0;

	enum forceFSM force_FSM;
	int forceInput = 0;
	int forceState = 0;

	enum controllerFSM FSM = main_Menu; //State machine to be used
	int buttonInput = 99; //Local button keypad data

	int errorSend = 0;
	packet_data localPacket; //Local data packet
	packet_reply_data localReplyPacket;


	while (1) { //Start of FSM

		//This switch case only prints output of current state
		//Processing for next state logic done at end of this while lop
		switch (FSM)
		{
		case main_Menu:
			printf("Main Menu State:\n-Status Display:1\n-Control Status:2\n\n");
			/*
			 * Main Menu State
			 * -Status Display:1
			 * -Control Status:2
			 */


			pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 0;
			sprintf(FSM_Resources->I2C_String, "Status Display: 1");
			LCDthread_ex(FSM_Resources);

			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 1;
			sprintf(FSM_Resources->I2C_String, "Control Menu: 2");
			LCDthread_ex(FSM_Resources);
			pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);

			break;

		case status_Display:
			printf("Status Display State:\n-Connection Status:1\n-Functional Status:2\n-Back:9\n\n");
			/*
			 * Status Display State
			 * -Connection Status:1
			 * -Functional Status:2
			 * -Back:9
			 */
			pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 0;
			sprintf(FSM_Resources->I2C_String, "Connect Status: 1");
			LCDthread_ex(FSM_Resources);

			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 1;
			sprintf(FSM_Resources->I2C_String, "Function Status: 2");
			LCDthread_ex(FSM_Resources);
			pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);

			break;
		case conn_Status:
			printf("Connection Status\n");
			printf("-Node 1: Status| Node 2: Status\n");
			printf("-Node 3: Status| Back:9\n\n");
			/*
			 *Connection Status
			 *-Node 1: Status| Node 2: Status
			 *-Node 3: Status| Back:9
			 */
			pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 0;
			sprintf(FSM_Resources->I2C_String, "Connecton Statuses");
			LCDthread_ex(FSM_Resources);

			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 1;
			sprintf(FSM_Resources->I2C_String, "");
			LCDthread_ex(FSM_Resources);
			pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);

			break;

		case func_Status:
			printf("Functional Status State\n");
			/*
			 *Functional Status
			 *-Node 1: EW| Node 2: E
			 *-Node 3: N| Back:9
			 */

			pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 0;
			sprintf(FSM_Resources->I2C_String, "TL1:-");
			LCDthread_ex(FSM_Resources);

			usleep(1000);

			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 1;
			sprintf(FSM_Resources->I2C_String, "TL2:-");
			LCDthread_ex(FSM_Resources);
			pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);

			if(localPacket.data[1] == ENTITY_TL1)
			{
				pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
				FSM_Resources->cursorX = 4;
				FSM_Resources->cursorY = 0;
				TrafficLightPrintState(&FSM_Resources->nodeParameter.data[2], FSM_Resources);
				LCDthread_ex(FSM_Resources);
				pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
			}
			else if(localPacket.data[1] == ENTITY_TL2)
			{
				pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
				FSM_Resources->cursorX = 4;
				FSM_Resources->cursorY = 1;
				TrafficLightPrintState(&FSM_Resources->nodeParameter.data[2], FSM_Resources);
				LCDthread_ex(FSM_Resources);
				pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
			}

			break;

		case func_StatusTrain:
			pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 0;
			sprintf(FSM_Resources->I2C_String, "RW:");
			LCDthread_ex(FSM_Resources);

			usleep(1000);

			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 1;
			sprintf(FSM_Resources->I2C_String, "");
			LCDthread_ex(FSM_Resources);
			pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);

			if(localPacket.data[1] == ENTITY_RW)
			{
				pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
				FSM_Resources->cursorX = 3;
				FSM_Resources->cursorY = 0;
				if(localPacket.data[1] == Train_Present){sprintf(FSM_Resources->I2C_String, "RW:Train");}
				else if(localPacket.data[1] == Train_Present){sprintf(FSM_Resources->I2C_String,"RW:NoTrain");}
				LCDthread_ex(FSM_Resources);
			}
			printf("Entity ID: %d, State: %d\n\n", FSM_Resources->nodeParameter.data[1], FSM_Resources->nodeParameter.data[2]);


			break;

		case control_Menu:
			printf("Control Menu State \n");
			printf("Force Settings:1 \n");
			printf("Mode Settings:2 \n\n");
			/*
			 * Control Menu State
			 * -Get Schedule/Mode:1
			 * -Set Schedule/Mode:2
			 * -
			 * -Back:9
			 * -Scroll Down:0
			 */
			pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 0;

			//sprintf(FSM_Resources->I2C_String, "Schedule Settings:1");
			sprintf(FSM_Resources->I2C_String, "Force Settings:1");
			LCDthread_ex(FSM_Resources);

			FSM_Resources->cursorX = 0;
			FSM_Resources->cursorY = 1;
			sprintf(FSM_Resources->I2C_String, "Mode Settings:2");
			LCDthread_ex(FSM_Resources);
			pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
			break;

		case force_Setting:
			switch(force_FSM)
			{
				case force_SelectNode:
					printf("Force Mode\n");
					printf("Train: 1\n");
					printf("TL: 2\n");
					printf("TL: 3\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "Select Node:");
					LCDthread_ex(FSM_Resources);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "RW:1 TL1:2 TL2:3");

					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case force_TL:
					printf("Select State\n");
					printf("Cancel State\n");

					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;
					sprintf(FSM_Resources->I2C_String, "Select State: 1");
					LCDthread_ex(FSM_Resources);

					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Cancel State: 2");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case force_TL_Menu:
					printf("Logic:Force STat\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;
					TrafficLightPrintState(&forceState, FSM_Resources);
					printf("-----------\n");
					LCDthread_ex(FSM_Resources);

					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;

					if(forceState == 25)
					{
						sprintf(FSM_Resources->I2C_String, "");
						LCDthread_ex(FSM_Resources);
					}
					else
					{
						usleep(1000);
						forceState=forceState + 1;
						TrafficLightPrintState(&forceState, FSM_Resources);
						printf("-----------\n");
						forceState=forceState- 1;
						LCDthread_ex(FSM_Resources);
					}
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case force_Train:
					printf("Force Gate Close: 1\n");
					printf("Force Gate Cancel: 2\n");

					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "Force Gate Close: 1");
					LCDthread_ex(FSM_Resources);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Force Cancel: 2");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);

					break;

				case force_Success:
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					printf("Successfully Forced");
					sprintf(FSM_Resources->I2C_String, "Successfully Forced");
					LCDthread_ex(FSM_Resources);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Back: 9");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case force_Failure:
					printf("Packet could not be sent! Back: 9\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "Unsuccessful");
					LCDthread_ex(FSM_Resources);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Back: 9");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;
			}
			break;

		case schedule_Setting:
			switch(schedule_FSM)
			{
				case schedule_SelectNode:
					printf("Select Node\n");
					printf("Traffic Node 1: 1\n");
					printf("Traffic Node 2: 2\n");
					printf("Back: 9\n\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "Select Node");
					LCDthread_ex(FSM_Resources);

					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "TF:1   TF:2");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);

					break;

				case select_Schedule:
					printf("Get Schedule: 1\n");
					printf("Set Schedule: 2\n");
					printf("Back: 9 \n\n");

					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "Get Schedule:1");
					LCDthread_ex(FSM_Resources);

					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Set Schedule:2");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case get_Schedule:
					printf("Schedule is: %d\n", localReplyPacket.data[1]);
					printf("Schedule is: %d\n", localReplyPacket.data[2]);
					printf("Schedule is: %d\n", localReplyPacket.data[3]);
					printf("Schedule is: %d\n", localReplyPacket.data[4]);
					printf("Schedule is: %d\n", localReplyPacket.data[5]);
					printf("Schedule is: %d\n", localReplyPacket.data[6]);
					printf("Back: 9 \n\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);

					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "ID1:%i ID2:%i ID3:%i", localReplyPacket.data[1], localReplyPacket.data[2], localReplyPacket.data[3]);
					LCDthread_ex(FSM_Resources);

					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String,  "ID4:%i ID5:%i ID6:%i", localReplyPacket.data[4], localReplyPacket.data[5], localReplyPacket.data[6]);
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case set_Schedule:
					printf("Current Schedule\n");
					printf("Schedule is: %d\n", localReplyPacket.data[1]);
					printf("Schedule is: %d\n", localReplyPacket.data[2]);
					printf("Schedule is: %d\n", localReplyPacket.data[3]);
					printf("Schedule is: %d\n", localReplyPacket.data[4]);
					printf("Schedule is: %d\n", localReplyPacket.data[5]);
					printf("Schedule is: %d\n", localReplyPacket.data[6]);

					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;
					sprintf(FSM_Resources->I2C_String, "A:%i B:%i C:%i ", localReplyPacket.data[1], localReplyPacket.data[2], localReplyPacket.data[3]);
					LCDthread_ex(FSM_Resources);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String,  "D:%i E:%i F:%i", localReplyPacket.data[4], localReplyPacket.data[5], localReplyPacket.data[6]);
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case edit_Schedule:
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;
					sprintf(FSM_Resources->I2C_String, "Input for timer %i", schedulePacketEdit);
					LCDthread_ex(FSM_Resources);


					FSM_Resources->cursorX = locationIncrement;
					locationIncrement++;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "%i", buttonInput);
					LCDthread_ex_lineDynamic(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				default:

					break;
			}
			break;

		case mode_Setting:
			switch(mode_FSM)
			{
				case mode_SelectNode:
					printf("Select Node\n");
					printf("Traffic Node 1: 1\n");
					printf("Traffic Node 2: 2\n");

					printf("Back: 9\n\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;
					sprintf(FSM_Resources->I2C_String, "Select Node");
					LCDthread_ex(FSM_Resources);

					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "TL1:1 TL2:2");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case traffic_Mode:
					printf("Peak: 1\n");
					printf("Night: 2\n");
					printf("Default: 3\n");
					printf("Back: 9\n\n");

					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;
					sprintf(FSM_Resources->I2C_String, "Peak:1  Night:2");
					LCDthread_ex(FSM_Resources);
					usleep(1000);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Default:3");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case mode_Success:
					printf("Successfully Sent\n\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "Successfully Set");
					LCDthread_ex(FSM_Resources);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Back: 9");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;

				case mode_Failure:
					printf("Packet could not be sent! Back: 9\n");
					pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 0;

					sprintf(FSM_Resources->I2C_String, "Unsuccessful");
					LCDthread_ex(FSM_Resources);
					FSM_Resources->cursorX = 0;
					FSM_Resources->cursorY = 1;
					sprintf(FSM_Resources->I2C_String, "Back: 9");
					LCDthread_ex(FSM_Resources);
					pthread_mutex_unlock(&FSM_Resources->i2c_Mutex);
					break;
			}
			break;

		default:
			printf("------------Error in state\n, buttonInput is: %d\n\n", buttonInput);
			break;
		}


////////////////////////////////////////////////////////////////////

		//Condition Variable code for this thread, server input, and keypad input
		pthread_mutex_lock(&FSM_Resources->button_Mutex);

		while ((!FSM_Resources->buttonPressed) && (!(FSM_Resources->serverUpdate) || (statusMode == 0)))
		{
			pthread_cond_wait(&FSM_Resources->button_CondVar, &FSM_Resources->button_Mutex);
		}

		if(FSM_Resources->buttonPressed)
		{
			buttonInput = FSM_Resources->buttonData;
			FSM_Resources->buttonPressed = 0;
			FSM_Resources->serverUpdate = 0;
		}
		else if(FSM_Resources->serverUpdate)
		{
			localPacket = FSM_Resources->nodeParameter;
			FSM_Resources->buttonPressed = 0;
			FSM_Resources->serverUpdate = 0;
		}


		pthread_mutex_unlock(&FSM_Resources->button_Mutex);



/////////////////////////////////////////////////////////////////


		switch (FSM) {
		case main_Menu:
			if(buttonInput==1)
			{
				FSM = status_Display;
			}
			else if(buttonInput==2)
			{
				FSM = control_Menu;
			}
			break;

		case status_Display:
			if (buttonInput == 1)
			{
				FSM = conn_Status;
				statusMode = 1;
			}
			else if(buttonInput == 2)
			{
				FSM = func_Status;
				statusMode = 1;
			}
			else if (buttonInput == 9)
			{
				FSM = main_Menu;
			}
			break;

		case conn_Status:
			if (buttonInput == 9)
			{
				FSM = status_Display;
				statusMode = 0;
			}
			break;

		case func_Status:
			if(buttonInput == 9)
			{
				FSM = status_Display;
				statusMode = 0;
			}
			else if(buttonInput == 11)
			{
				FSM = func_StatusTrain;
			}
			break;

		case func_StatusTrain:
			if(buttonInput == 9)
			{
				FSM = status_Display;
				statusMode = 0;
			}
			else if(buttonInput == 10)
			{
				FSM = func_Status;
			}
			break;

		case control_Menu:
			if(buttonInput == 3)
			{
				FSM = schedule_Setting;
			}
			else if(buttonInput == 2 )
			{
				FSM = mode_Setting;
			}
			else if(buttonInput == 1)
			{
				FSM = force_Setting;
			}
			else if(buttonInput == 9)
			{
				FSM = main_Menu;
			}
			break;

		case force_Setting:
			switch(force_FSM)
			{
				case force_SelectNode:
					if(buttonInput == 1) //train
					{
						force_FSM = force_Train;
						forceInput = 0;
					}
					else if(buttonInput == 2)
					{
						force_FSM = force_TL;
						forceInput = 1;
					}
					else if(buttonInput == 3)
					{
						force_FSM = force_TL;
						forceInput = 2;
					}
					else if(buttonInput == 9)
					{
						FSM = control_Menu;
						force_FSM = force_SelectNode;
					}
					break;

				case force_Train:
					if(buttonInput == 1)
					{
						force_FSM = force_Success;
						localPacket.data[0] = COMMAND_FORCE_STATE;
						localPacket.data[1] = 1;

						pthread_mutex_lock(&FSM_Resources->command_Mutex);
						FSM_Resources->nodeParameter = localPacket; //packet set into resource for client to access
						FSM_Resources->newCommand[forceInput] = 1;
						pthread_cond_signal(&FSM_Resources->command_CondVar[forceInput]); //signals client thread to run

						pthread_mutex_unlock(&FSM_Resources->command_Mutex);

						printf("----Before Mutex\n");
						pthread_mutex_lock(&FSM_Resources->reply_Mutex);

						while(!FSM_Resources->replyReceived)
						{
							printf("Before condvar wait\n");
							pthread_cond_wait(&FSM_Resources->reply_CondVar[forceInput],&FSM_Resources->reply_Mutex);
							printf("After Send on FSM: Reply is %i\n", FSM_Resources->replyReceived);
						}

						printf("After Send on FSM\n");

						FSM_Resources->replyReceived = 0;
						localReplyPacket = FSM_Resources->replyData;
						pthread_cond_broadcast(&FSM_Resources->reply_CondVar[forceInput]);
						pthread_mutex_unlock(&FSM_Resources->reply_Mutex);
					}
					else if(buttonInput == 2)
					{
						force_FSM = force_Success;
						localPacket.data[0] = COMMAND_FORCE_STATE_CANCEL;
						localPacket.data[1] = 0;

						pthread_mutex_lock(&FSM_Resources->command_Mutex);
						FSM_Resources->nodeParameter = localPacket; //packet set into resource for client to access
						FSM_Resources->newCommand[forceInput] = 1;
						pthread_cond_signal(&FSM_Resources->command_CondVar[forceInput]); //signals client thread to run

						pthread_mutex_unlock(&FSM_Resources->command_Mutex);

						printf("----Before Mutex\n");
						pthread_mutex_lock(&FSM_Resources->reply_Mutex);

						while(!FSM_Resources->replyReceived)
						{
							printf("Before condvar wait\n");
							pthread_cond_wait(&FSM_Resources->reply_CondVar[forceInput],&FSM_Resources->reply_Mutex);
							printf("After Send on FSM: Reply is %i\n", FSM_Resources->replyReceived);
						}

						printf("After Send on FSM\n");

						FSM_Resources->replyReceived = 0;
						localReplyPacket = FSM_Resources->replyData;
						pthread_cond_broadcast(&FSM_Resources->reply_CondVar[forceInput]);
						pthread_mutex_unlock(&FSM_Resources->reply_Mutex);

					}

					pthread_mutex_lock(&FSM_Resources->error_Mutex);
					errorSend = FSM_Resources->errorSend[forceInput];
					pthread_mutex_unlock(&FSM_Resources->error_Mutex);


					//Error check
					if(buttonInput == 9) //buttoninput should have higher priority here
					{
						force_FSM = force_SelectNode;
					}
					else if(errorSend == 1)
					{
						force_FSM = force_Failure;
						pthread_mutex_lock(&FSM_Resources->error_Mutex);
						FSM_Resources->errorSend[forceInput] = 0;
						pthread_mutex_unlock(&FSM_Resources->error_Mutex);
					}

					break;

				case force_TL:
					if(buttonInput == 1) ////////////////////////////////////////////
					{
						force_FSM = force_TL_Menu;
					}
					else if(buttonInput == 2) //Send Packet
					{
						force_FSM = force_Success;
						localPacket.data[0] = COMMAND_FORCE_STATE;
						localPacket.data[1] = 0;

						pthread_mutex_lock(&FSM_Resources->command_Mutex);
						FSM_Resources->nodeParameter = localPacket; //packet set into resource for client to access
						FSM_Resources->newCommand[forceInput] = 1;
						pthread_cond_signal(&FSM_Resources->command_CondVar[forceInput]); //signals client thread to run

						pthread_mutex_unlock(&FSM_Resources->command_Mutex);

						printf("----Before Mutex\n");
						pthread_mutex_lock(&FSM_Resources->reply_Mutex);

						while(!FSM_Resources->replyReceived)
						{
							printf("Before condvar wait\n");
							pthread_cond_wait(&FSM_Resources->reply_CondVar[forceInput],&FSM_Resources->reply_Mutex);
							printf("After Send on FSM: Reply is %i\n", FSM_Resources->replyReceived);
						}

						printf("After Send on FSM\n");

						FSM_Resources->replyReceived = 0;
						localReplyPacket = FSM_Resources->replyData;
						pthread_cond_broadcast(&FSM_Resources->reply_CondVar[forceInput]);
						pthread_mutex_unlock(&FSM_Resources->reply_Mutex);

						pthread_mutex_lock(&FSM_Resources->error_Mutex);
						errorSend = FSM_Resources->errorSend[forceInput];
						pthread_mutex_unlock(&FSM_Resources->error_Mutex);


						if(errorSend)
						{
							force_FSM = force_Failure;
						}
					}
					else if(buttonInput == 9)
					{
						force_FSM = force_SelectNode;
					}
					break;

				case force_TL_Menu:
					printf("forceState: %i\n", forceState);
					if(buttonInput == 1)
					{
						forceState = forceState + 1;

						if(forceState > 25)
						{forceState = 25;}
					}
					if(buttonInput == 2)
					{
						forceState = forceState - 1;

						if(forceState < 0)
						{forceState = 0;}
					}
					if(buttonInput == 11)
					{
						force_FSM = force_Success;
						localPacket.data[0] = COMMAND_FORCE_STATE;
						localPacket.data[1] = forceState;

						pthread_mutex_lock(&FSM_Resources->command_Mutex);
						FSM_Resources->nodeParameter = localPacket; //packet set into resource for client to access
						FSM_Resources->newCommand[forceInput] = 1;
						pthread_cond_signal(&FSM_Resources->command_CondVar[forceInput]); //signals client thread to run

						pthread_mutex_unlock(&FSM_Resources->command_Mutex);

						printf("----Before Mutex\n");
						pthread_mutex_lock(&FSM_Resources->reply_Mutex);

						while(!FSM_Resources->replyReceived)
						{
							printf("Before condvar wait\n");
							pthread_cond_wait(&FSM_Resources->reply_CondVar[forceInput],&FSM_Resources->reply_Mutex);
							printf("After Send on FSM: Reply is %i\n", FSM_Resources->replyReceived);
						}

						printf("After Send on FSM\n");

						FSM_Resources->replyReceived = 0;
						localReplyPacket = FSM_Resources->replyData;
						pthread_cond_broadcast(&FSM_Resources->reply_CondVar[forceInput]);
						pthread_mutex_unlock(&FSM_Resources->reply_Mutex);

						pthread_mutex_lock(&FSM_Resources->error_Mutex);
						errorSend = FSM_Resources->errorSend[forceInput];
						pthread_mutex_unlock(&FSM_Resources->error_Mutex);

						if(errorSend)
						{
							force_FSM = force_Failure;
						}

						forceInput = 0;
					}

					if(buttonInput == 9)
					{
						force_FSM = force_TL;
					}
					break;

				case force_Success:
					if(buttonInput == 9)
					{
						force_FSM = force_SelectNode;
					}
					break;

				case force_Failure:
					if(buttonInput == 9)
					{
						force_FSM = force_SelectNode;
					}
					break;
			}
			break;


		case schedule_Setting:
			switch(schedule_FSM)
			{
				case schedule_SelectNode:
					if(buttonInput == 1)
					{
						scheduleInput = 1;
						schedule_FSM = select_Schedule;
					}
					else if(buttonInput == 2)
					{
						scheduleInput = 2;
						schedule_FSM = select_Schedule;
					}
					else if(buttonInput == 9)
					{
						schedule_FSM = schedule_SelectNode;
						FSM = control_Menu;
					}
					break;

				case select_Schedule:
					if(buttonInput == 1)
					{
						//need to make if statement for sending on which client thread to send
						schedule_FSM = get_Schedule;

						//send packet information

					}
					else if(buttonInput == 2)
					{
						schedule_FSM = set_Schedule;
						localPacket.data[0] = COMMAND_GET_MODE_SCHED;

					}
					else if(buttonInput == 9)
					{
						schedule_FSM = select_Schedule;
					}
					break;

				case get_Schedule:
					if(buttonInput == 9)
					{
						schedule_FSM = schedule_SelectNode;
					}
					break;

				case set_Schedule:
					if((buttonInput == 1) || (buttonInput == 2) || (buttonInput == 3)||
							(buttonInput == 4)||(buttonInput == 5)||(buttonInput == 6))
					{
						schedule_FSM = edit_Schedule;
						schedulePacketEdit = buttonInput;

						pthread_mutex_lock(&FSM_Resources->i2c_Mutex);
						FSM_Resources->cursorY = 1;
						sprintf(FSM_Resources->I2C_String, "");
						LCDthread_ex(FSM_Resources);
					}
					else if(buttonInput == 9)
					{
						schedule_FSM = schedule_SelectNode;
					}
					else if(buttonInput == 11)
					{

					}
					break;

				case edit_Schedule:
					inputBuffer = buttonInput;
					localReplyPacket.data[schedulePacketEdit] = buttonInput;

					if(buttonInput == 9)
					{
						schedule_FSM = set_Schedule;
						locationIncrement = 0;
					}
					break;

				default:
					break;
			}
			break;

		case mode_Setting:
			switch(mode_FSM)
			{
				case mode_SelectNode:
					if(buttonInput == 1)
					{
						modeInput = 1;
						mode_FSM = traffic_Mode;
					}
					else if(buttonInput == 2)
					{
						modeInput = 2;
						mode_FSM = traffic_Mode;
					}
					else if(buttonInput == 9)
					{
						mode_FSM = mode_SelectNode;
						FSM = control_Menu;
					}
					break;

				case traffic_Mode:
					if(buttonInput == 1) //peak
					{ //if statement needed to choose client by mode input
						mode_FSM = mode_Success;
						localPacket.data[0] = COMMAND_SET_MODE_SCHED;
						localPacket.data[1] = MODE_ID_PEAK_TIME;

						pthread_mutex_lock(&FSM_Resources->command_Mutex);
						FSM_Resources->nodeParameter = localPacket; //packet set into resource for client to access
						FSM_Resources->newCommand[modeInput] = 1;
						pthread_cond_signal(&FSM_Resources->command_CondVar[modeInput]); //signals client thread to run

						pthread_mutex_unlock(&FSM_Resources->command_Mutex);

						printf("----Before Mutex\n");
						pthread_mutex_lock(&FSM_Resources->reply_Mutex);

						while(!FSM_Resources->replyReceived)
						{
							printf("Before condvar wait\n");
							pthread_cond_wait(&FSM_Resources->reply_CondVar[modeInput],&FSM_Resources->reply_Mutex);
							printf("After Send on FSM: Reply is %i\n", FSM_Resources->replyReceived);
						}

						printf("After Send on FSM\n");

						FSM_Resources->replyReceived = 0;
						localReplyPacket = FSM_Resources->replyData;
						pthread_cond_broadcast(&FSM_Resources->reply_CondVar[modeInput]);
						pthread_mutex_unlock(&FSM_Resources->reply_Mutex);
					}
					else if (buttonInput == 2) //night
					{	//if statement needed to choose client by mode input
						mode_FSM = mode_Success;
						localPacket.data[0] = COMMAND_SET_MODE_SCHED;
						localPacket.data[1] = MODE_ID_NIGHT_TIME;

						pthread_mutex_lock(&FSM_Resources->command_Mutex);
						FSM_Resources->nodeParameter = localPacket; //packet set into resource for client to access
						FSM_Resources->newCommand[modeInput] = 1;
						pthread_cond_signal(&FSM_Resources->command_CondVar[modeInput]); //signals client thread to run

						pthread_mutex_unlock(&FSM_Resources->command_Mutex);

						printf("----Before Mutex\n");
						pthread_mutex_lock(&FSM_Resources->reply_Mutex);

						while(!FSM_Resources->replyReceived)
						{
							printf("Before condvar wait\n");
							pthread_cond_wait(&FSM_Resources->reply_CondVar[modeInput],&FSM_Resources->reply_Mutex);
							printf("After Send on FSM: Reply is %i\n", FSM_Resources->replyReceived);
						}

						printf("After Send on FSM\n");

						FSM_Resources->replyReceived = 0;
						localReplyPacket = FSM_Resources->replyData;
						pthread_cond_broadcast(&FSM_Resources->reply_CondVar[modeInput]);
						pthread_mutex_unlock(&FSM_Resources->reply_Mutex);
					}
					else if(buttonInput == 3) //default
					{	//if statement needed to choose client by mode input
						mode_FSM = mode_Success;
						localPacket.data[0] = COMMAND_SET_MODE_SCHED;
						localPacket.data[1] = MODE_ID_DEFAULT;

						pthread_mutex_lock(&FSM_Resources->command_Mutex);
						FSM_Resources->nodeParameter = localPacket; //packet set into resource for client to access
						FSM_Resources->newCommand[modeInput] = 1;
						pthread_cond_signal(&FSM_Resources->command_CondVar[modeInput]); //signals client thread to run

						pthread_mutex_unlock(&FSM_Resources->command_Mutex);

						printf("----Before Mutex\n");
						pthread_mutex_lock(&FSM_Resources->reply_Mutex);

						while(!FSM_Resources->replyReceived)
						{
							printf("Before condvar wait\n");
							pthread_cond_wait(&FSM_Resources->reply_CondVar[modeInput],&FSM_Resources->reply_Mutex);
							printf("After Send on FSM: Reply is %i\n", FSM_Resources->replyReceived);
						}

						printf("After Send on FSM\n");

						FSM_Resources->replyReceived = 0;
						localReplyPacket = FSM_Resources->replyData;
						pthread_cond_broadcast(&FSM_Resources->reply_CondVar[modeInput]);
						pthread_mutex_unlock(&FSM_Resources->reply_Mutex);
					}

					pthread_mutex_lock(&FSM_Resources->error_Mutex);
					errorSend = FSM_Resources->errorSend[modeInput];
					pthread_mutex_unlock(&FSM_Resources->error_Mutex);

					if(buttonInput == 9) //buttoninput should have higher priority here
					{
						mode_FSM = mode_SelectNode;
					}
					else if(errorSend == 1)
					{
						mode_FSM = mode_Failure;
						pthread_mutex_lock(&FSM_Resources->error_Mutex);
						FSM_Resources->errorSend[modeInput] = 0;
						pthread_mutex_unlock(&FSM_Resources->error_Mutex);
					}
					break;

				case mode_Success:
					if(buttonInput == 9)
					{
						mode_FSM = mode_SelectNode;
					}
					break;
				case mode_Failure:
					if(buttonInput == 9)
					{
						mode_FSM = mode_SelectNode;
					}
				default:
					break;
			}

		default:
			break;
		}



	}
}


void *clientThread_Send1(void *data)
{
	client_data * command_Resources = (client_data*)data;
	packet_data commandPacket1;
	packet_reply_data replyData;
	int serverConn; //used for server connection

	printf("-------Client Thread: %i\n", command_Resources->thread_num);



	if ((serverConn = name_open(command_Resources->attach_point, 0)) == -1) {
		printf("\n    ERROR, could not connect to server!\n\n");
		printf("-------Client Thread is could not open connection: %i\n", command_Resources->thread_num);
	}
	else
	{
		printf("-------Client Thread is connected to server: %i\n", command_Resources->thread_num);
	}
	commandPacket1.ClientID = ENTITY_CC;
	commandPacket1.hdr.type = 0x00;
	commandPacket1.hdr.subtype = 0x00;

	//Timer section
	timer_t client_timer_id;
	struct itimerspec client_itime;
	timer_data time_msg;

	// initializer
	struct sigevent event;
	int chid;
	chid = ChannelCreate(0);
	printf("ChannelID: %i\n", chid);
	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf(stderr, "Could not connect to channel for client thread 1\n");
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);

	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &client_timer_id) == -1)
	{
	   printf (stderr, "Could not create clock for client thread 1\n");
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// modification added here
	// 1 second delay interval timer
	client_itime.it_value.tv_sec = 1;	   // 1 second
	client_itime.it_value.tv_nsec = 0;    // 0 second
	client_itime.it_interval.tv_sec = 2;  // 1 second
	client_itime.it_interval.tv_nsec = 0; // 0 second

	//Used for server reconnection
	int rcvid=0; //used for timer channel checking

	while (1)
	{
		printf("Client in beginning of while loop %i\n", command_Resources->thread_num);
		if(command_Resources->connectionStatus)
		{
			pthread_mutex_lock(&command_Resources->resource_data->command_Mutex);
			printf("Client before convar lop: %i\n", command_Resources->thread_num);
			while(!command_Resources->resource_data->newCommand[command_Resources->thread_num])
			{
				printf("Client waiting on condar: %i\n", command_Resources->thread_num);
				pthread_cond_wait(&command_Resources->resource_data->command_CondVar[command_Resources->thread_num],&command_Resources->resource_data->command_Mutex);
				printf("Thread num: %i\n", command_Resources->thread_num);
			}
			printf("Out of command loop\n");

			commandPacket1 = command_Resources->resource_data->nodeParameter; //read resource into local packet
			command_Resources->resource_data->newCommand[command_Resources->thread_num] = 0;
			pthread_mutex_unlock(&command_Resources->resource_data->command_Mutex);

			fflush(stdout);

			//Send command
			if (MsgSend(serverConn, &commandPacket1, sizeof(commandPacket1), &replyData, sizeof(replyData)) == -1)
			{
				printf("--------Command Packet Not Sent\n\n");
				command_Resources->connectionStatus= 0;

				//Mutex for setting error
				pthread_mutex_lock(&command_Resources->resource_data->error_Mutex);
				command_Resources->resource_data->errorSend[command_Resources->thread_num] = 1;
				pthread_mutex_unlock(&command_Resources->resource_data->error_Mutex);
			}
			else
			{
				printf("-------------Packet_Sent on %i\n", command_Resources->thread_num);
				//Reply mutex section
				pthread_mutex_lock(&command_Resources->resource_data->reply_Mutex);
				while(command_Resources->resource_data->replyReceived)
				{
					printf("-------Inside command resource while loop\n");
					pthread_cond_wait(&command_Resources->resource_data->reply_CondVar[command_Resources->thread_num],&command_Resources->resource_data->reply_Mutex);
				}
				printf("-------After command resource while loop\n");
				command_Resources->resource_data->replyReceived = 1;
				command_Resources->resource_data->replyData = replyData;
				pthread_cond_broadcast(&command_Resources->resource_data->reply_CondVar[command_Resources->thread_num]);
				pthread_mutex_unlock(&command_Resources->resource_data->reply_Mutex);

				//Mutexx for setting error
				pthread_mutex_lock(&command_Resources->resource_data->error_Mutex);
				command_Resources->resource_data->errorSend[command_Resources->thread_num] = 0;
				pthread_mutex_unlock(&command_Resources->resource_data->error_Mutex);
			}
		}
		else
		{
			timer_settime(client_timer_id, 0, &client_itime, NULL);
			printf("Beginning of reconnect code: %i\n", command_Resources->thread_num);

			while(((serverConn = name_open(command_Resources->attach_point, 0)) == -1))
			{
				printf("Try to reconnect: %i\n", command_Resources->thread_num);
				rcvid = MsgReceive(chid, &time_msg, sizeof(time_msg), NULL);
				if (rcvid == 0)
				{
					if(time_msg.pulse.code == MY_PULSE_CODE)
					{ //signals for any condvar in case it gets stuck

						pthread_mutex_lock(&command_Resources->resource_data->reply_Mutex);
						command_Resources->resource_data->replyReceived = 1;
						pthread_cond_broadcast(&command_Resources->resource_data->reply_CondVar[command_Resources->thread_num]);
						pthread_mutex_unlock(&command_Resources->resource_data->reply_Mutex);
						/*
						pthread_mutex_lock(&command_Resources->resource_data->command_Mutex);
						command_Resources->resource_data->newCommand[command_Resources->thread_num] = 0;
						pthread_cond_signal(&command_Resources->resource_data->command_CondVar[command_Resources->thread_num]);
						pthread_mutex_unlock(&command_Resources->resource_data->command_Mutex);
*/
						pthread_mutex_lock(&command_Resources->resource_data->error_Mutex);
						command_Resources->resource_data->errorSend[command_Resources->thread_num] = 1;
						pthread_mutex_unlock(&command_Resources->resource_data->error_Mutex);

						//only for button
						/*
						pthread_mutex_lock(&command_Resources->resource_data->button_Mutex);
						command_Resources->resource_data->buttonPressed = 0;
						pthread_cond_signal(&command_Resources->resource_data->button_CondVar);
						pthread_mutex_unlock(&command_Resources->resource_data->button_Mutex);
*/
					}
				}
				fflush(stdout);

				printf("Failed to reconnect: %i\n", command_Resources->thread_num);
			}
			pthread_mutex_lock(&command_Resources->resource_data->error_Mutex);
			command_Resources->resource_data->errorSend[command_Resources->thread_num] = 0;
			pthread_mutex_unlock(&command_Resources->resource_data->error_Mutex);

			command_Resources->connectionStatus = 1;
			printf("Connection established to: %s\n", command_Resources->attach_point);

		}


	}

	// Close the connection
	printf("\n Sending message to server to tell it to close the connection\n");
	name_close(serverConn);
	return EXIT_SUCCESS;
}


void *serverThread(void *data)  {
	thread_data * Update_Resources = (thread_data*)data;

	packet_data dataReceive_Server; //packet to send to server
	packet_reply_data replyData_Server; //packet to send to server

	int server_Recv = 0; // receive code when channel signal comes back

	name_attach_t * serverAttach;

	if ((serverAttach = name_attach(NULL, CC_ATTACH_POINT, 0)) == NULL)
   {
	   printf("\n Possibly another server with the same name is already running !\n");
	   return EXIT_FAILURE;
   }

	int packet_ID; //For case statement to set ackknowledge

   replyData_Server.hdr.type = 0x01;
   replyData_Server.hdr.subtype = 0x00;

   while (1)
   {
	   server_Recv = MsgReceive(serverAttach->chid, &dataReceive_Server, sizeof(dataReceive_Server), NULL);

	   if (server_Recv == -1)
	   {
		   printf("\nFailed to receive data\n");
		   break;
	   }

	   if (server_Recv == 0)
	   {
		   switch (dataReceive_Server.hdr.code)
		   {
				   default:
				   // Some other pulse sent by one of your processes or the kernel
				   printf("\nServer got some other pulse %d\n", dataReceive_Server.hdr.code);
				   break;

		   }
		   continue;// go back to top of while loop
	   }

	   // for messages:
	   if(server_Recv > 0) // if true then A message was received
	   {

		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (dataReceive_Server.hdr.type == _IO_CONNECT )
		   {
			   MsgReply( server_Recv, EOK, NULL, 0 );
			   printf("\n gns service is running....");
			   continue;
		   }

		   // Some other I/O message was received; reject it
		   if (dataReceive_Server.hdr.type > _IO_BASE && dataReceive_Server.hdr.type <= _IO_MAX )
		   {
			   MsgError( server_Recv, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   continue;	// go back to top of while loop
		   }


		   packet_ID = dataReceive_Server.data[0];
		   switch(packet_ID)
		   {
		   case COMMAND_FORCE_STATE:
		      replyData_Server.data[0] = ACK_FORCE_STATE;
		      break;
		   case COMMAND_FORCE_STATE_CANCEL:
		      replyData_Server.data[0] = ACK_FORCE_STATE_CANCEL;
		      break;
		   case COMMAND_FORCE_MODE:
		      replyData_Server.data[0] = ACK_FORCE_MODE;
		      break;
		   case COMMAND_FORCE_MODE_CANCEL:
		      replyData_Server.data[0] = ACK_FORCE_MODE_CANCEL;
		      break;
		   case COMMAND_GET_MODE_SCHED:
		      replyData_Server.data[0] = ACK_GET_MODE_SCHED;
		      break;
		   case COMMAND_SET_MODE_SCHED:
		      replyData_Server.data[0] = ACK_SET_MODE_SCHED;
		      break;
		   }

		   replyData_Server.data[1] = 20;

		   printf("----Server: %d\n", packet_ID);
		   printf("----Server2ndByte: %d\n",  dataReceive_Server.data[1]);
		   MsgReply(server_Recv, EOK, &replyData_Server, sizeof(replyData_Server));
		   pthread_mutex_lock(&Update_Resources->button_Mutex);
		   Update_Resources->nodeParameter = dataReceive_Server;
		   Update_Resources->serverUpdate = 1;
		   pthread_cond_broadcast(&Update_Resources->button_CondVar);
		   pthread_mutex_unlock(&Update_Resources->button_Mutex);

	   }
	   else
	   {
		   printf("\nERROR: Server received something, but could not handle it correctly\n");
	   }
	   //pthread_mutex_unlock(&Update_Resources->update_Mutex);
   }

   // Remove the attach point name from the file system (i.e. /dev/name/local/<myname>)
   name_detach(serverAttach, 0);


	printf("\n Sending message to server to tell it to close the connection\n");
	return EXIT_SUCCESS;
}


#define AM335X_CONTROL_MODULE_BASE   (uint64_t) 0x44E10000
#define AM335X_CONTROL_MODULE_SIZE   (size_t)   0x00001448
#define AM335X_GPIO_SIZE             (uint64_t) 0x00001000
#define AM335X_GPIO1_BASE            (size_t)   0x4804C000

#define LED0          (1<<21)   // GPIO1_21
#define LED1          (1<<22)   // GPIO1_22
#define LED2          (1<<23)   // GPIO1_23
#define LED3          (1<<24)   // GPIO1_24

#define SD0 (1<<28)  // SD0 is connected to GPIO1_28
#define SCL (1<<16)  // SCL is connected to GPIO1_16


#define GPIO_OE        0x134
#define GPIO_DATAIN    0x138
#define GPIO_DATAOUT   0x13C

#define GPIO_IRQSTATUS_SET_1 0x38   // enable interrupt generation
#define GPIO_IRQWAKEN_1      0x48   // Wakeup Enable for Interrupt Line
#define GPIO_FALLINGDETECT   0x14C  // set falling edge trigger
#define GPIO_CLEARDATAOUT    0x190  // clear data out Register
#define GPIO_IRQSTATUS_1     0x30   // clear any prior IRQs

#define GPIO1_IRQ 99  // TRG page 465 list the IRQs for the am335x


#define P9_12_pinConfig 0x878 //  conf_gpmc_ben1 (TRM pp 1364) for GPIO1_28,  P9_12

// GPMC_A1_Configuration
#define PIN_MODE_0   0x00
#define PIN_MODE_1   0x01
#define PIN_MODE_2   0x02
#define PIN_MODE_3   0x03
#define PIN_MODE_4   0x04
#define PIN_MODE_5   0x05
#define PIN_MODE_6   0x06
#define PIN_MODE_7   0x07

// PIN MUX Configuration strut values  (page 1420 from TRM)
#define PU_ENABLE    0x00
#define PU_DISABLE   0x01
#define PU_PULL_UP   0x01
#define PU_PULL_DOWN 0x00
#define RECV_ENABLE  0x01
#define RECV_DISABLE 0x00
#define SLEW_FAST    0x00
#define SLEW_SLOW    0x01

typedef union _CONF_MODULE_PIN_STRUCT   // See TRM Page 1420
{
  unsigned int d32;
  struct {    // name: field size
           unsigned int conf_mmode : 3;       // LSB
           unsigned int conf_puden : 1;
           unsigned int conf_putypesel : 1;
           unsigned int conf_rxactive : 1;
           unsigned int conf_slewctrl : 1;
           unsigned int conf_res_1 : 13;      // reserved
           unsigned int conf_res_2 : 12;      // reserved MSB
         } b;
} _CONF_MODULE_PIN;

void strobe_SCL(uintptr_t gpio_port_add) {
   uint32_t PortData;
   PortData = in32(gpio_port_add + GPIO_DATAOUT);// value that is currently on the GPIO port
   PortData &= ~(SCL);
   out32(gpio_port_add + GPIO_DATAOUT, PortData);// Clock low
   delaySCL();

   PortData  = in32(gpio_port_add + GPIO_DATAOUT);// get port value
   PortData |= SCL;// Clock high
   out32(gpio_port_add + GPIO_DATAOUT, PortData);
   delaySCL();
}

// Thread used to Flash the 4 LEDs on the BeagleBone for 100ms
void *Flash_LED0_ex(void *notused)
{
	pthread_detach(pthread_self());  // no need for this thread to join
	uintptr_t gpio1_port = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	uintptr_t val;
	// Write GPIO data output register
	val  = in32(gpio1_port + GPIO_DATAOUT);
	val |= (LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	usleep(100000);  // 100 ms wait
	//sched_yield();  // if used without the usleep, this line will flash the LEDS for ~4ms

	val  = in32(gpio1_port + GPIO_DATAOUT);
	val &= ~(LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	munmap_device_io(gpio1_port, AM335X_GPIO_SIZE);

}

void delaySCL()  {// Small delay used to get timing correct for BBB
  volatile int i, a;
  for(i=0;i<0x1F;i++) // 0x1F results in a delay that sets F_SCL to ~480 kHz
  {   // i*1 is faster than i+1 (i+1 results in F_SCL ~454 kHz, whereas i*1 is the same as a=i)
     a = i;
  }
  // usleep(1);  //why doesn't this work? Ans: Results in a period of 4ms as
  // fastest time, which is 250Hz (This is to slow for the TTP229 chip as it
  // requires F_SCL to be between 1 kHz and 512 kHz)
}

uint32_t KeypadReadIObit(uintptr_t gpio_base, uint32_t BitsToRead)  {
   volatile uint32_t val = 0;
   val  = in32(gpio_base + GPIO_DATAIN);// value that is currently on the GPIO port

   val &= BitsToRead; // mask bit
   //val = val >> (BitsToRead % 2);
   //return val;
   if(val==BitsToRead)
	   return 1;
   else
	   return 0;
}

//void DecodeKeyValue(uint32_t word, int *keypadData)
void DecodeKeyValue(uint32_t word, thread_data * keyDecode_Resources)
{
	switch(word)
	{
		case 0x01:
			printf("Key  1 pressed\n");

			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 1;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x02:
			printf("Key  2 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 2;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1);
			break;
		case 0x04:
			printf("Key  3 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 3;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x08:
			printf("Key  4 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 4;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x10:
			printf("Key  5 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 5;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x20:
			printf("Key  6 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 6;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x40:
			printf("Key  7 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 7;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x80:
			printf("Key  8 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 8;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x100:
			printf("Key  9 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 9;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x200:
			printf("Key 10 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 10;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x400:
			printf("Key 11 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 11;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x800:
			printf("Key 12 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 12;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x1000:
			printf("Key 13 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 13;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x2000:
			printf("Key 14 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 14;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x4000:
			printf("Key 15 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 15;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x8000:
			printf("Key 16 pressed\n");
			pthread_mutex_lock(&keyDecode_Resources->button_Mutex);
			keyDecode_Resources->buttonData = 16;
			keyDecode_Resources->buttonPressed = 1;
			pthread_cond_broadcast(&keyDecode_Resources->button_CondVar);
			pthread_mutex_unlock(&keyDecode_Resources->button_Mutex);
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			break;
		case 0x00:  // key release event (do nothing)
			break;
		default:
			printf("Key pressed could not be determined - %lu\n", word);
	}
}
/*
 *
 *
 * Here is the ISR
 *
 *
 *
 */
typedef struct
{
	int count_thread;
	uintptr_t gpio1_base;
	struct sigevent pevent; // remember to fill in "event" structure in main
}ISR_data;

// create global struct to share data between threads
ISR_data ISR_area_data;

const struct sigevent* Inthandler( void* area, int id )
{
	// 	"Do not call any functions in ISR that call kernerl - including printf()
	//struct sigevent *pevent = (struct sigevent *) area;
	ISR_data *p_ISR_data = (ISR_data *) area;

	InterruptMask(GPIO1_IRQ, id);  // Disable all hardware interrupt

	// must do this in the ISR  (else stack over flow and system will crash
	out32(p_ISR_data->gpio1_base + GPIO_IRQSTATUS_1, SD0); //clear IRQ

	// do this to tell us how many times this handler gets called
	p_ISR_data->count_thread++;
	// got IRQ.
	// work out what it came from

    InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

    // return a pointer to an event structure (preinitialized
    // by main) that contains SIGEV_INTR as its notification type.
    // This causes the InterruptWait in "int_thread" to unblock.
	return (&p_ISR_data->pevent);
}

uintptr_t gpio1_base;
uintptr_t control_module;
//volatile uint32_t val = 0;

int keypadSetup(){
		control_module = mmap_device_io(AM335X_CONTROL_MODULE_SIZE,
			  AM335X_CONTROL_MODULE_BASE);
	  gpio1_base = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	  // initalise the global stuct
	  ISR_area_data.count_thread = 0;
	  ISR_area_data.gpio1_base = gpio1_base;

	  memset(&ISR_area_data.pevent, 0, sizeof(ISR_area_data.pevent));
	  SIGEV_INTR_INIT (&ISR_area_data.pevent);
	  ISR_area_data.pevent.sigev_notify = SIGEV_INTR;  // Setup for external interrupt

		// we also need to have the PROCMGR_AID_INTERRUPT and PROCMGR_AID_IO abilities enabled. For more information, see procmgr_ability().
	  ThreadCtl( _NTO_TCTL_IO_PRIV , 1);// Request I/O privileges  for QNX7;

	  procmgr_ability( 0, PROCMGR_AID_INTERRUPT | PROCMGR_AID_IO);

	  volatile uint32_t val = 0;

	  if( (control_module)&&(gpio1_base) )
	  {
		// set DDR for LEDs to output and GPIO_28 to input
		val = in32(gpio1_base + GPIO_OE); // read in current setup for GPIO1 port
		val |= 1<<28;                     // set IO_BIT_28 high (1=input, 0=output)
		out32(gpio1_base + GPIO_OE, val); // write value to input enable for data pins
		val &= ~(LED0|LED1|LED2|LED3);    // write value to output enable
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for LED pins

		val = in32(gpio1_base + GPIO_OE);
		val &= ~SCL;                      // 0 for output
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for data pins

		val = in32(gpio1_base + GPIO_DATAOUT);
		val |= SCL;              // Set Clock Line High as per TTP229-BSF datasheet
		out32(gpio1_base + GPIO_DATAOUT, val); // for 16-Key active-Low timing diagram

		in32s(&val, 1, control_module + P9_12_pinConfig );
		printf("Original pinmux configuration for GPIO1_28 = %#010x\n", val);

		// set up pin mux for the pins we are going to use  (see page 1354 of TRM)
		volatile _CONF_MODULE_PIN pinConfigGPMC; // Pin configuration strut
		pinConfigGPMC.d32 = 0;
		// Pin MUX register default setup for input (GPIO input, disable pull up/down - Mode 7)
		pinConfigGPMC.b.conf_slewctrl = SLEW_SLOW;    // Select between faster or slower slew rate
		pinConfigGPMC.b.conf_rxactive = RECV_ENABLE;  // Input enable value for the PAD
		pinConfigGPMC.b.conf_putypesel= PU_PULL_UP;   // Pad pullup/pulldown type selection
		pinConfigGPMC.b.conf_puden = PU_ENABLE;       // Pad pullup/pulldown enable
		pinConfigGPMC.b.conf_mmode = PIN_MODE_7;      // Pad functional signal mux select 0 - 7

		// Write to PinMux registers for the GPIO1_28
		out32(control_module + P9_12_pinConfig, pinConfigGPMC.d32);
		in32s(&val, 1, control_module + P9_12_pinConfig);   // Read it back
		printf("New configuration register for GPIO1_28 = %#010x\n", val);

		// Setup IRQ for SD0 pin ( see TRM page 4871 for register list)
		out32(gpio1_base + GPIO_IRQSTATUS_SET_1, SD0);	// Write 1 to GPIO_IRQSTATUS_SET_1
		out32(gpio1_base + GPIO_IRQWAKEN_1, SD0);    	// Write 1 to GPIO_IRQWAKEN_1
		out32(gpio1_base + GPIO_FALLINGDETECT, SD0);    // set falling edge
		out32(gpio1_base + GPIO_CLEARDATAOUT, SD0);     // clear GPIO_CLEARDATAOUT
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);      // clear any prior IRQs
		// all good to go periperhals can be set
		return EXIT_SUCCESS;
	}
	  else{
		  // periperhals cannot be initialized
		return EXIT_FAILURE;
	}

}

void *interruptKeyPadThread(void *data)
{
	thread_data * Key_Resources = (thread_data*)data;
	// Main code starts here
	//The thread that calls InterruptWait() must be the one that called InterruptAttach().
	//    id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK | _NTO_INTR_FLAGS_NO_UNMASK | _NTO_INTR_FLAGS_END);
	 int id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK );

	InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

	volatile uint32_t val = 0;

	int i = 0;
	printf( "Main entering loop and will call InterruptWait\n");
	for(;;)
	{
		// Block main until we get a sigevent of type: 	ISR_area_data.pevent
		InterruptWait( 0, NULL );   // block this thread until an interrupt occurs  (Wait for a hardware interrupt)
		InterruptDisable();
		// printf("do interrupt work here...\n");

		volatile uint32_t word = 0;
		//  confirm that SD0 is still low (that is a valid Key press event has occurred)
		val = KeypadReadIObit(gpio1_base, SD0);  // read SD0 (means data is ready)

		if(val == 0)  // start reading key value form the keypad
		{
			 word = 0;  // clear word variable

			 delaySCL(); // wait a short period of time before reading the data Tw  (10 us)

			 for(i=0;i<16;i++)           // get data from SD0 (16 bits)
			 {
				strobe_SCL(gpio1_base);  // strobe the SCL line so we can read in data bit

				val = KeypadReadIObit(gpio1_base, SD0); // read in data bit
				val = ~val & 0x01;                      // invert bit and mask out everything but the LSB
				//printf("val[%u]=%u, ",i, val);
				word = word | (val<<i);  // add data bit to word in unique position (build word up bit by bit)
			 }
			 //printf("word=%u\n",word);
			 /*
			 pthread_mutex_lock(&Key_Resources->button_Mutex);
			 DecodeKeyValue(word, &Key_Resources->buttonData);
			 Key_Resources->buttonPressed = 1;
			 printf("Interrupt count = %i\n", ISR_area_data.count_thread);
			 pthread_cond_broadcast(&Key_Resources->button_CondVar);
			 pthread_mutex_unlock(&Key_Resources->button_Mutex);
*/

			 DecodeKeyValue(word, Key_Resources);



		}
		//sched_yield();
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);
		InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt
		InterruptEnable();
	}
	 munmap_device_io(control_module, AM335X_CONTROL_MODULE_SIZE);
}

#define DATA_SEND 0x40  // sets the Rs value high
#define Co_Ctrl   0x00  // mode to tell LCD we are sending a single command

// Writes to I2C
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData)
{
	i2c_send_t hdr;
    iov_t sv[2];
    int status, i;

    uint8_t LCDpacket[21] = {};  // limited to 21 characters  (1 control bit + 20 bytes)

    // set the mode for the write (control or data)
    LCDpacket[0] = mode;  // set the mode (data or control)

	// copy data to send to send buffer (after the mode bit)
	for (i=0;i<NbData+1;i++)
		LCDpacket[i+1] = *pBuffer++;

    hdr.slave.addr = Address;
    hdr.slave.fmt = I2C_ADDRFMT_7BIT;
    hdr.len = NbData + 1;  // 1 extra for control (mode) bit
    hdr.stop = 1;

    SETIOV(&sv[0], &hdr, sizeof(hdr));
    SETIOV(&sv[1], &LCDpacket[0], NbData + 1); // 1 extra for control (mode) bit
      // int devctlv(int filedes, int dcmd,     int sparts, int rparts, const iov_t *sv, const iov_t *rv, int *dev_info_ptr);
    status = devctlv(fd, 		  DCMD_I2C_SEND, 2,          0,          sv,              NULL,           NULL);

    if (status != EOK)
    	printf("status = %s\n", strerror ( status ));

    return status;
}


void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column)
{
	uint8_t position = 0x80; // SET_DDRAM_CMD (control bit)
	uint8_t rowValue = 0;
	uint8_t	LCDcontrol = 0;
	if (row == 1)
		rowValue = 0x40;     // memory location offset for row 1
	position = (uint8_t)(position + rowValue + column);
	LCDcontrol = position;
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}


void Initialise_LCD (int fd, _Uint32t LCDi2cAdd)
{
	uint8_t	LCDcontrol = 0x00;

	//   Initialise the LCD display via the I2C bus
	LCDcontrol = 0x38;  // data byte for FUNC_SET_TBL1
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x39; // data byte for FUNC_SET_TBL2
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x14; // data byte for Internal OSC frequency
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x79; // data byte for contrast setting
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x50; // data byte for Power/ICON control Contrast set
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x6C; // data byte for Follower control
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x0C; // data byte for Display ON
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x01; // data byte for Clear display
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}
