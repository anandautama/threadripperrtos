#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <errno.h>
#include <stdbool.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <share.h>
#include <time.h>
#include <errno.h>
#include "Traffic_Light_System_Def.h"

// hardware specific include
#include <hw/inout.h>      // for in32() and out32();
#include <sys/mman.h>      // for mmap_device_io();
#include <stdint.h>        // for unit32 types
#include <sys/neutrino.h>  // for ThreadCtl( _NTO_TCTL_IO_PRIV , NULL)
#include <sched.h>
#include <sys/procmgr.h>
// end of hardware specific include

// lcd hardware specific include
#include "stdint.h"
#include <fcntl.h>
#include <devctl.h>
#include <hw/i2c.h>
#include <unistd.h>
#include <sched.h>
// end of lcd hardware specific include

#define DATA_SEND 0x40  // sets the Rs value high
#define Co_Ctrl   0x00  // mode to tell LCD we are sending a single command
typedef struct
{
	int fd;
	uint8_t Address;
	uint8_t mode;
	pthread_mutex_t LCD_mutex;
	int row;
	int col;
	uint8_t	LCDtext[21];

} LCD_Data_struct;

#define ATTACH_POINT "RailwayCrossing"

char * progname = "Railway_Crossing_Controller.c";
#define MY_PULSE_CODE _PULSE_CODE_MINAVAIL

typedef union
{
	struct _pulse   pulse;
    // your other message structures would go here too
} timer_message;

typedef struct {
	enum Rail_Crossing_States Rail_State;
	int state_data_ready[3];
	pthread_mutex_t client_FSM_mutex;
	pthread_cond_t client_FSM_condvar;

	int Train_Counter_A;
	int Train_Counter_B;
	int sensor_data_ready;
	int Force_State;
	int server_data_ready;
	pthread_mutex_t FSM_IO_mutex;
	pthread_cond_t FSM_IO_condvar;

	LCD_Data_struct *LCD_Data;
} thread_data;

typedef struct {
	int thread_num;
	thread_data *resource_data;
	const char *attach_point;
} client_data;

void *Rail_Crossing_FSM_thread(void *data);
void Rail_Crossing_SM(enum Rail_Crossing_States * CurState, thread_data *shared_data);
void *Rail_Crossing_Input_thread(void *data);
void Rail_Crossing_Print_State(enum Rail_Crossing_States *CurState, thread_data *shared_data);
void *client_thread(void *data);
void *server_thread(void* data);

int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData);
void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column);
void Initialise_LCD (int fd, _Uint32t LCDi2cAdd);
int Initialise_I2C_Resource();
void LCD_write(LCD_Data_struct *LCD_Data);

// hardware specific defines
int setup();
const struct sigevent* Inthandler( void* area, int id );
void *interruptKeyPadThread(void *data);

int main(void) {
	printf("Railway Crossing Control Program\n");
	pthread_t Rail_Crossing_pthread, Rail_Input_pthread;
	pthread_t client_thread_pthread[3];
	pthread_t server_thread_pthread;

	//thread_data resource_data;
	thread_data resource_data;
	resource_data.Rail_State = Train_Present;
	for(int i = 0; i < 3; i++)
	{
		resource_data.state_data_ready[i] = 0;
	}
	pthread_mutex_init(&resource_data.client_FSM_mutex,NULL);
	pthread_mutex_init(&resource_data.FSM_IO_mutex,NULL);
	pthread_cond_init(&resource_data.client_FSM_condvar,NULL);
	pthread_cond_init(&resource_data.FSM_IO_condvar,NULL);
	resource_data.Train_Counter_A = 0;
	resource_data.Train_Counter_B = 0;
	resource_data.sensor_data_ready = 0;
	resource_data.Force_State = 0;
	resource_data.server_data_ready = 0;

	client_data client_data[3];
	client_data[0].resource_data = &resource_data;
	client_data[0].attach_point = CC_ATTACH_POINT;
	client_data[0].thread_num = 0;
	client_data[1].resource_data = &resource_data;
	client_data[1].attach_point = TL1_ATTACH_POINT;
	client_data[1].thread_num = 1;
	client_data[2].resource_data = &resource_data;
	client_data[2].attach_point = TL2_ATTACH_POINT;
	client_data[2].thread_num = 2;

	void *pthread_end;

	LCD_Data_struct LCD_Data;
	resource_data.LCD_Data = &LCD_Data;
	int LcdResult = Initialise_I2C_Resource();
	if(LcdResult != EXIT_FAILURE)
	{
		printf("LCD Initialized\n");
		volatile uint8_t LCDi2cAdd = 0x3C;
		LCD_Data.fd     = LcdResult;
		LCD_Data.Address= LCDi2cAdd;
		LCD_Data.mode   = DATA_SEND;
		// make sure we mutex the initial lcd
		pthread_mutex_init(&LCD_Data.LCD_mutex,NULL);
	}
	else {
		printf("LCD Cannot Be Initialized\n");
	}

	pthread_t keyPadThread;
	// launch thread
	int result = setup();
	if(result == EXIT_SUCCESS){
		printf("Key pad initialized \n");
		pthread_create(&keyPadThread, NULL, interruptKeyPadThread, &resource_data);
	}
	else{
		printf("Peripherals cannot be init\n");
		pthread_create(&Rail_Input_pthread, NULL, Rail_Crossing_Input_thread, &resource_data);
	}

	pthread_create(&Rail_Crossing_pthread, NULL, Rail_Crossing_FSM_thread, &resource_data);

	for(int i = 0; i < 3; i++)
	{
		pthread_create(&client_thread_pthread[i], NULL, client_thread, &client_data[i]);
	}
	pthread_create(&server_thread_pthread, NULL, server_thread, &resource_data);

	pthread_join(Rail_Crossing_pthread, &pthread_end);
	pthread_join(Rail_Input_pthread, &pthread_end);

	printf("All good. Main Terminated...\n\n");
	return EXIT_SUCCESS;
}

void *Rail_Crossing_FSM_thread(void *data){
	printf("---------Rail Crossing Thread Started\n");
	thread_data *FSM_Resources = (thread_data*)data;

	enum Rail_Crossing_States CurState = Train_Present;
	while (1)
	{
		Rail_Crossing_SM( &CurState, FSM_Resources);

	}

}

void Rail_Crossing_SM(enum Rail_Crossing_States *CurState, thread_data *shared_data){
	Rail_Crossing_Print_State(CurState, shared_data);
	pthread_mutex_lock(&shared_data->FSM_IO_mutex);

	while(!(shared_data->sensor_data_ready || shared_data->server_data_ready))
	{
		pthread_cond_wait(&shared_data->FSM_IO_condvar,&shared_data->FSM_IO_mutex);
		printf("waiting for sensor\n");
	}

	switch (*CurState){
	case Train_Present:
		if((shared_data->Train_Counter_A == 0) &&
				(shared_data->Train_Counter_B == 0) && (shared_data->Force_State == 0))
		{
			pthread_mutex_lock(&shared_data->client_FSM_mutex);
			*CurState = Train_NotPresent;
			shared_data->Rail_State = *CurState;
			for(int i = 0; i < 3; i++)
			{
				shared_data->state_data_ready[i] = 1;
			}
			pthread_cond_broadcast(&shared_data->client_FSM_condvar);
			pthread_mutex_unlock(&shared_data->client_FSM_mutex);
		}
		break;
	case Train_NotPresent:
		if((shared_data->Train_Counter_A > 0) ||
				(shared_data->Train_Counter_B > 0) || (shared_data->Force_State == 1))
		{
			pthread_mutex_lock(&shared_data->client_FSM_mutex);
			*CurState = Train_Present;
			shared_data->Rail_State = *CurState;
			for(int i = 0; i < 3; i++)
			{
				shared_data->state_data_ready[i] = 1;
			}
			pthread_cond_broadcast(&shared_data->client_FSM_condvar);
			pthread_mutex_unlock(&shared_data->client_FSM_mutex);
		}
		break;
	}
	shared_data->sensor_data_ready = 0;
	shared_data->server_data_ready = 0;
	pthread_mutex_unlock(&shared_data->FSM_IO_mutex);
}

void *Rail_Crossing_Input_thread(void *data){
	printf("-----------Input Thread Started\n");
	thread_data *sensor_Resources = (thread_data*)data;

	while(1)
	{
		char input = 'o';
		scanf("%c", &input); //wait for user input
		printf("char input\n");

		pthread_mutex_lock(&sensor_Resources->FSM_IO_mutex);
		switch(input)
		{
		case 'a':
			sensor_Resources->Train_Counter_A++;
			break;
		case 'b':
			sensor_Resources->Train_Counter_B++;
			break;
		case 'c':
			if(sensor_Resources->Train_Counter_A > 0)
				sensor_Resources->Train_Counter_A--;
			else
				sensor_Resources->Train_Counter_A = 0;
			break;
		case 'd':
			if(sensor_Resources->Train_Counter_B > 0)
				sensor_Resources->Train_Counter_B--;
			else
				sensor_Resources->Train_Counter_B = 0;
			break;
		}
		sensor_Resources->sensor_data_ready = 1;
		pthread_cond_signal(&sensor_Resources->FSM_IO_condvar);
		pthread_mutex_unlock(&sensor_Resources->FSM_IO_mutex);
	}
}

void Rail_Crossing_Print_State(enum Rail_Crossing_States *CurState, thread_data *shared_data){
	switch (*CurState){
		case Train_Present:
			printf("In current state: Train_Present\n");
			pthread_mutex_lock(&shared_data->LCD_Data->LCD_mutex);
			shared_data->LCD_Data->row = 0;
			shared_data->LCD_Data->col = 0;
			sprintf(shared_data->LCD_Data->LCDtext,"Train Present       ");
			LCD_write(shared_data->LCD_Data);
			pthread_mutex_lock(&shared_data->LCD_Data->LCD_mutex);
			break;
		case Train_NotPresent:
			printf("In current state: Train_Present\n");
			pthread_mutex_lock(&shared_data->LCD_Data->LCD_mutex);
			shared_data->LCD_Data->row = 0;
			shared_data->LCD_Data->col = 0;
			sprintf(shared_data->LCD_Data->LCDtext,"Train Not Present   ");
			LCD_write(shared_data->LCD_Data);
			pthread_mutex_lock(&shared_data->LCD_Data->LCD_mutex);
			break;
	}
}

void *client_thread(void *data)
{
	client_data *thread_data = (client_data*)data;
    packet_data msg;
    packet_reply_data reply;

    timer_t client_timer_id;
    struct itimerspec client_itime;

	int rcvid;
	timer_message time_msg;

	// initializer
	struct sigevent event;
	int chid;
	chid = ChannelCreate(0);
	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf(stderr, "%s:  couldn't ConnectAttach to self! \n", progname);
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);

	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &client_timer_id) == -1)
	{
	   printf (stderr, "%s:  couldn't create a timer, errno %d \n", progname, errno);
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// modification added here
	// 1 second delay interval timer
	client_itime.it_value.tv_sec = 1;	   // 1 second
	client_itime.it_value.tv_nsec = 0;    // 0 second
	client_itime.it_interval.tv_sec = 10;  // 1 second
	client_itime.it_interval.tv_nsec = 0; // 0 second

    msg.ClientID = 601 + thread_data->thread_num; // unique number for this client (optional)

    int server_coid;

    // We would have pre-defined data to stuff here
    msg.hdr.type = 0x00;
    msg.hdr.subtype = 0x00;
    int connection_status = 0;

    while(1)
    {
    	if(connection_status)
    	{
			// Do whatever work you wanted with server connection
    		printf("Client %d waiting for data\n",thread_data->thread_num);
			//printf("client loop\n");
			pthread_mutex_lock(&thread_data->resource_data->client_FSM_mutex);
			 // test the condition and wait until it is true
			 while(!thread_data->resource_data->state_data_ready[thread_data->thread_num])
			 {
				 pthread_cond_wait(&thread_data->resource_data->client_FSM_condvar, &thread_data->resource_data->client_FSM_mutex);
				 printf("cons data_ready = %d\n",thread_data->resource_data->state_data_ready[thread_data->thread_num]);
			 }
			 // process data
			 printf ("consumer:  got data from producer\n");
			 msg.data[0] = STATUS_UPDATE_ID; //Status Update Packet
			 msg.data[1] = ENTITY_RW; //Rail Crossing Node
			 msg.data[2] = thread_data->resource_data->Rail_State;

			 // now change the condition and signal that it has changed
			 thread_data->resource_data->state_data_ready[thread_data->thread_num] = 0;

			pthread_mutex_unlock(&thread_data->resource_data->client_FSM_mutex);

			// the data we are sending is in msg.data
			printf("Client (ID:%d), sending data packet\n", msg.ClientID);
			fflush(stdout);

			if (MsgSend(server_coid, &msg, sizeof(msg), &reply, sizeof(reply)) == -1)
			{
				printf("Error data NOT sent to server\n");
					// maybe we did not get a reply from the server
				connection_status = 0;
			}
			else
			{ // now process the reply
				printf("   -->Reply received'\n");
			}
    	}
    	else
    	{
			printf("  ---> Client %d trying to connect to server named: %s\n",
					thread_data->thread_num, thread_data->attach_point);
			timer_settime(client_timer_id, 0, &client_itime, NULL);
			while(((server_coid = name_open(thread_data->attach_point, 0)) == -1))
			{
				printf("\nERROR, Client %d could not connect to server!\n",thread_data->thread_num);
				//return EXIT_FAILURE;
				rcvid = MsgReceive(chid, &time_msg, sizeof(time_msg), NULL);
				if (rcvid == 0){
					if(time_msg.pulse.code == MY_PULSE_CODE){
					}
				}
				fflush(stdout);
			}
			connection_status = 1;
			printf("Connection established to: %s\n", thread_data->attach_point);
			pthread_mutex_lock(&thread_data->resource_data->client_FSM_mutex);
			thread_data->resource_data->state_data_ready[thread_data->thread_num] = 1;
			pthread_mutex_unlock(&thread_data->resource_data->client_FSM_mutex);

    	}
    }

    // Close the connection
    printf("\n Sending message to server to tell it to close the connection\n");
    name_close(server_coid);

    return EXIT_SUCCESS;
}

void *server_thread(void* data)
{
	thread_data *Server_Resources = (thread_data*)data;
   name_attach_t *attach;

   // Create a local name (/dev/name/...)
   if ((attach = name_attach(NULL, ATTACH_POINT, 0)) == NULL)
   {
       printf("\nFailed to name_attach on ATTACH_POINT: %s \n", ATTACH_POINT);
       printf("\n Possibly another server with the same name is already running !\n");
	   return EXIT_FAILURE;
   }

   printf("Server Listening for Clients on ATTACH_POINT: %s \n", ATTACH_POINT);

   	/*
	 *  Server Loop
	 */
   packet_data msg;
   int rcvid=0, msgnum=0;  		// no message received yet
   int Stay_alive=0, living=0;	// server stays running (ignores _PULSE_CODE_DISCONNECT request)

   packet_reply_data replymsg; // replymsg structure for sending back to client
   replymsg.hdr.type = 0x01;
   replymsg.hdr.subtype = 0x00;

   uint8_t packet_ID;

   living =1;
   while (living)
   {
	   // Do your MsgReceive's here now with the chid
       rcvid = MsgReceive(attach->chid, &msg, sizeof(msg), NULL);

       if (rcvid == -1)  // Error condition, exit
       {
           printf("\nFailed to MsgReceive\n");
           break;
       }

       // did we receive a Pulse or message?
       // for Pulses:
       if (rcvid == 0)  //  Pulse received, work out what type
       {
           switch (msg.hdr.code)
           {
			   case _PULSE_CODE_DISCONNECT:
					// A client disconnected all its connections by running
					// name_close() for each name_open()  or terminated
				   /*
				   if( Stay_alive == 0)
				   {
					   ConnectDetach(msg.hdr.scoid);
					   printf("\nServer was told to Detach from ClientID:%d ...\n", msg.ClientID);
					   living = 0; // kill while loop
					   continue;
				   }
				   else
				   {
					   printf("\nServer received Detach pulse from ClientID:%d but rejected it ...\n", msg.ClientID);
				   }
				   */
				   break;

			   case _PULSE_CODE_UNBLOCK:
					// REPLY blocked client wants to unblock (was hit by a signal
					// or timed out).  It's up to you if you reply now or later.
				   printf("\nServer got _PULSE_CODE_UNBLOCK after %d, msgnum\n", msgnum);
				   break;

			   case _PULSE_CODE_COIDDEATH:  // from the kernel
				   printf("\nServer got _PULSE_CODE_COIDDEATH after %d, msgnum\n", msgnum);
				   break;

			   case _PULSE_CODE_THREADDEATH: // from the kernel
				   printf("\nServer got _PULSE_CODE_THREADDEATH after %d, msgnum\n", msgnum);
				   break;

			   default:
				   // Some other pulse sent by one of your processes or the kernel
				   printf("\nServer got some other pulse after %d, msgnum\n", msgnum);
				   break;

           }
           continue;// go back to top of while loop
       }

       // for messages:
       if(rcvid > 0) // if true then A message was received
       {
		   msgnum++;

		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (msg.hdr.type == _IO_CONNECT )
		   {
			   MsgReply( rcvid, EOK, NULL, 0 );
			   printf("\n gns service is running....");
			   continue;	// go back to top of while loop
		   }

		   // Some other I/O message was received; reject it
		   if (msg.hdr.type > _IO_BASE && msg.hdr.type <= _IO_MAX )
		   {
			   MsgError( rcvid, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   continue;	// go back to top of while loop
		   }

		   // A message (presumably ours) received

		   // put your message handling code here and assemble a reply message
		   printf("Server received data packet from client (ID:%d), ", msg.ClientID);
		   fflush(stdout);

		   packet_ID = msg.data[0];
		   switch(packet_ID)
		   {
		   case COMMAND_FORCE_STATE:
			   pthread_mutex_lock(&Server_Resources->FSM_IO_mutex);
			   Server_Resources->Force_State = 1;
			   Server_Resources->server_data_ready = 1;
			   pthread_cond_signal(&Server_Resources->FSM_IO_condvar);
			   pthread_mutex_unlock(&Server_Resources->FSM_IO_mutex);
			   replymsg.data[0] = ACK_FORCE_STATE;
			   break;
		   case COMMAND_FORCE_STATE_CANCEL:
			   pthread_mutex_lock(&Server_Resources->FSM_IO_mutex);
			   Server_Resources->Force_State = 0;
			   Server_Resources->server_data_ready = 1;
			   pthread_cond_signal(&Server_Resources->FSM_IO_condvar);
			   pthread_mutex_unlock(&Server_Resources->FSM_IO_mutex);
			   replymsg.data[0] = ACK_FORCE_STATE_CANCEL;
			   break;
		   case COMMAND_FORCE_MODE:

			   replymsg.data[0] = ACK_FORCE_MODE;
			   break;
		   case COMMAND_FORCE_MODE_CANCEL:

			   replymsg.data[0] = ACK_FORCE_MODE_CANCEL;
			   break;
		   case COMMAND_GET_MODE_SCHED:

			   replymsg.data[0] = ACK_GET_MODE_SCHED;
			   break;
		   case COMMAND_SET_MODE_SCHED:

			   replymsg.data[0] = ACK_SET_MODE_SCHED;
			   break;
		   }

		   printf("\nreplying\n");
		   MsgReply(rcvid, EOK, &replymsg, sizeof(replymsg));
       }
       else
       {
    	   printf("\nERROR: Server received something, but could not handle it correctly\n");
       }

   }

   // Remove the attach point name from the file system (i.e. /dev/name/local/<myname>)
   name_detach(attach, 0);

   return EXIT_SUCCESS;
}

// hardware specific codes downstairs


void strobe_SCL(uintptr_t gpio_port_add) {
   uint32_t PortData;
   PortData = in32(gpio_port_add + GPIO_DATAOUT);// value that is currently on the GPIO port
   PortData &= ~(SCL);
   out32(gpio_port_add + GPIO_DATAOUT, PortData);// Clock low
   delaySCL();

   PortData  = in32(gpio_port_add + GPIO_DATAOUT);// get port value
   PortData |= SCL;// Clock high
   out32(gpio_port_add + GPIO_DATAOUT, PortData);
   delaySCL();
}

// Thread used to Flash the 4 LEDs on the BeagleBone for 100ms
void *Flash_LED0_ex(void *notused)
{
	pthread_detach(pthread_self());  // no need for this thread to join
	uintptr_t gpio1_port = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	uintptr_t val;
	// Write GPIO data output register
	val  = in32(gpio1_port + GPIO_DATAOUT);
	val |= (LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	usleep(100000);  // 100 ms wait
	//sched_yield();  // if used without the usleep, this line will flash the LEDS for ~4ms

	val  = in32(gpio1_port + GPIO_DATAOUT);
	val &= ~(LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	munmap_device_io(gpio1_port, AM335X_GPIO_SIZE);
	return 0;

}

void delaySCL()  {// Small delay used to get timing correct for BBB
  volatile int i, a;
  for(i=0;i<0x1F;i++) // 0x1F results in a delay that sets F_SCL to ~480 kHz
  {   // i*1 is faster than i+1 (i+1 results in F_SCL ~454 kHz, whereas i*1 is the same as a=i)
     a = i;
  }
  // usleep(1);  //why doesn't this work? Ans: Results in a period of 4ms as
  // fastest time, which is 250Hz (This is to slow for the TTP229 chip as it
  // requires F_SCL to be between 1 kHz and 512 kHz)
}

uint32_t KeypadReadIObit(uintptr_t gpio_base, uint32_t BitsToRead)  {
   volatile uint32_t val = 0;
   val  = in32(gpio_base + GPIO_DATAIN);// value that is currently on the GPIO port

   val &= BitsToRead; // mask bit
   //val = val >> (BitsToRead % 2);
   //return val;
   if(val==BitsToRead)
	   return 1;
   else
	   return 0;
}

void DecodeKeyValue(uint32_t word, thread_data *sensor_Resources)
{
	pthread_mutex_lock(&sensor_Resources->FSM_IO_mutex);
	switch(word)
	{
		case 0x01:
			printf("Key  1 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			sensor_Resources->Train_Counter_A++;
			sensor_Resources->sensor_data_ready = 1;
			break;
		case 0x02:
			printf("Key  2 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			if(sensor_Resources->Train_Counter_A > 0)
				sensor_Resources->Train_Counter_A--;
			else
				sensor_Resources->Train_Counter_A = 0;
			sensor_Resources->sensor_data_ready = 1;
			break;
		case 0x04:
			printf("Key  3 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			sensor_Resources->Train_Counter_B++;
			sensor_Resources->sensor_data_ready = 1;
			break;
		case 0x08:
			printf("Key  4 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			if(sensor_Resources->Train_Counter_B > 0)
				sensor_Resources->Train_Counter_B--;
			else
				sensor_Resources->Train_Counter_B = 0;
			sensor_Resources->sensor_data_ready = 1;
			break;
		case 0x10:
			printf("Key  5 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x20:
			printf("Key  6 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x40:
			printf("Key  7 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x80:
			printf("Key  8 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x100:
			printf("Key  9 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x200:
			printf("Key 10 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x400:
			printf("Key 11 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x800:
			printf("Key 12 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		// we have 4 left over butten for something else ?
		case 0x1000:
			printf("Key 13 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x2000:
			printf("Key 14 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x4000:
			printf("Key 15 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x8000:
			printf("Key 16 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED

			break;
		case 0x00:  // key release event (do nothing)
			break;
		default:
			printf("Key pressed could not be determined - %lu\n", word);
	}

	pthread_cond_signal(&sensor_Resources->FSM_IO_condvar);
	pthread_mutex_unlock(&sensor_Resources->FSM_IO_mutex);
	usleep(1); // do this so we only fire once
}
/*
 *
 *
 * Here is the ISR
 *
 *
 *
 */
typedef struct
{
	int count_thread;
	uintptr_t gpio1_base;
	struct sigevent pevent; // remember to fill in "event" structure in main
}ISR_data;

// create global struct to share data between threads
ISR_data ISR_area_data;

const struct sigevent* Inthandler( void* area, int id )
{
	// 	"Do not call any functions in ISR that call kernerl - including printf()
	//struct sigevent *pevent = (struct sigevent *) area;
	ISR_data *p_ISR_data = (ISR_data *) area;

	InterruptMask(GPIO1_IRQ, id);  // Disable all hardware interrupt

	// must do this in the ISR  (else stack over flow and system will crash
	out32(p_ISR_data->gpio1_base + GPIO_IRQSTATUS_1, SD0); //clear IRQ

	// do this to tell us how many times this handler gets called
	p_ISR_data->count_thread++;
	// got IRQ.
	// work out what it came from

    InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

    // return a pointer to an event structure (preinitialized
    // by main) that contains SIGEV_INTR as its notification type.
    // This causes the InterruptWait in "int_thread" to unblock.
	return (&p_ISR_data->pevent);
}

uintptr_t gpio1_base;
uintptr_t control_module;
volatile uint32_t val = 0;

int setup(){
		control_module = mmap_device_io(AM335X_CONTROL_MODULE_SIZE,
			  AM335X_CONTROL_MODULE_BASE);
	  gpio1_base = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	  // initalise the global stuct
	  ISR_area_data.count_thread = 0;
	  ISR_area_data.gpio1_base = gpio1_base;

	  memset(&ISR_area_data.pevent, 0, sizeof(ISR_area_data.pevent));
	  SIGEV_INTR_INIT (&ISR_area_data.pevent);
	  ISR_area_data.pevent.sigev_notify = SIGEV_INTR;  // Setup for external interrupt

		// we also need to have the PROCMGR_AID_INTERRUPT and PROCMGR_AID_IO abilities enabled. For more information, see procmgr_ability().
	  ThreadCtl( _NTO_TCTL_IO_PRIV , 1);// Request I/O privileges  for QNX7;

	  procmgr_ability( 0, PROCMGR_AID_INTERRUPT | PROCMGR_AID_IO);

	  if( (control_module)&&(gpio1_base) )
	  {
		// set DDR for LEDs to output and GPIO_28 to input
		val = in32(gpio1_base + GPIO_OE); // read in current setup for GPIO1 port
		val |= 1<<28;                     // set IO_BIT_28 high (1=input, 0=output)
		out32(gpio1_base + GPIO_OE, val); // write value to input enable for data pins
		val &= ~(LED0|LED1|LED2|LED3);    // write value to output enable
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for LED pins

		val = in32(gpio1_base + GPIO_OE);
		val &= ~SCL;                      // 0 for output
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for data pins

		val = in32(gpio1_base + GPIO_DATAOUT);
		val |= SCL;              // Set Clock Line High as per TTP229-BSF datasheet
		out32(gpio1_base + GPIO_DATAOUT, val); // for 16-Key active-Low timing diagram

		in32s(&val, 1, control_module + P9_12_pinConfig );
		printf("Original pinmux configuration for GPIO1_28 = %#010x\n", val);

		// set up pin mux for the pins we are going to use  (see page 1354 of TRM)
		volatile _CONF_MODULE_PIN pinConfigGPMC; // Pin configuration strut
		pinConfigGPMC.d32 = 0;
		// Pin MUX register default setup for input (GPIO input, disable pull up/down - Mode 7)
		pinConfigGPMC.b.conf_slewctrl = SLEW_SLOW;    // Select between faster or slower slew rate
		pinConfigGPMC.b.conf_rxactive = RECV_ENABLE;  // Input enable value for the PAD
		pinConfigGPMC.b.conf_putypesel= PU_PULL_UP;   // Pad pullup/pulldown type selection
		pinConfigGPMC.b.conf_puden = PU_ENABLE;       // Pad pullup/pulldown enable
		pinConfigGPMC.b.conf_mmode = PIN_MODE_7;      // Pad functional signal mux select 0 - 7

		// Write to PinMux registers for the GPIO1_28
		out32(control_module + P9_12_pinConfig, pinConfigGPMC.d32);
		in32s(&val, 1, control_module + P9_12_pinConfig);   // Read it back
		printf("New configuration register for GPIO1_28 = %#010x\n", val);

		// Setup IRQ for SD0 pin ( see TRM page 4871 for register list)
		out32(gpio1_base + GPIO_IRQSTATUS_SET_1, SD0);	// Write 1 to GPIO_IRQSTATUS_SET_1
		out32(gpio1_base + GPIO_IRQWAKEN_1, SD0);    	// Write 1 to GPIO_IRQWAKEN_1
		out32(gpio1_base + GPIO_FALLINGDETECT, SD0);    // set falling edge
		out32(gpio1_base + GPIO_CLEARDATAOUT, SD0);     // clear GPIO_CLEARDATAOUT
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);      // clear any prior IRQs
		// all good to go periperhals can be set
		return EXIT_SUCCESS;
	}
	  else{
		  // periperhals cannot be initialized
		return EXIT_FAILURE;
	}

}

void *interruptKeyPadThread(void *data){
	thread_data *FSM_Resources = (thread_data*)data;

	// Main code starts here
	//The thread that calls InterruptWait() must be the one that called InterruptAttach().
	//    id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK | _NTO_INTR_FLAGS_NO_UNMASK | _NTO_INTR_FLAGS_END);
	 int id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK );

	InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

	int i = 0;
	printf( "Main entering loop and will call InterruptWait\n");
	for(;;)
	{
		// Block main until we get a sigevent of type: 	ISR_area_data.pevent
		InterruptWait( 0, NULL );   // block this thread until an interrupt occurs  (Wait for a hardware interrupt)
		InterruptDisable();
		// printf("do interrupt work here...\n");

		volatile uint32_t word = 0;
		//  confirm that SD0 is still low (that is a valid Key press event has occurred)
		val = KeypadReadIObit(gpio1_base, SD0);  // read SD0 (means data is ready)

		if(val == 0)  // start reading key value form the keypad
		{
			 word = 0;  // clear word variable

			 delaySCL(); // wait a short period of time before reading the data Tw  (10 us)

			 for(i=0;i<16;i++)           // get data from SD0 (16 bits)
			 {
				strobe_SCL(gpio1_base);  // strobe the SCL line so we can read in data bit

				val = KeypadReadIObit(gpio1_base, SD0); // read in data bit
				val = ~val & 0x01;                      // invert bit and mask out everything but the LSB

				word = word | (val<<i);  // add data bit to word in unique position (build word up bit by bit)
			 }
			 //printf("word=%u\n",word);
			 DecodeKeyValue(word, FSM_Resources);
			 //printf("Interrupt count = %i\n", ISR_area_data.count_thread);
		}
		//sched_yield();
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);
		InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt
		InterruptEnable();
	}
	 munmap_device_io(control_module, AM335X_CONTROL_MODULE_SIZE);
	 return 0;
}

// Writes to I2C
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData)
{
	i2c_send_t hdr;
    iov_t sv[2];
    int status, i;

    uint8_t LCDpacket[21] = {};  // limited to 21 characters  (1 control bit + 20 bytes)

    // set the mode for the write (control or data)
    LCDpacket[0] = mode;  // set the mode (data or control)

	// copy data to send to send buffer (after the mode bit)
	for (i=0;i<NbData+1;i++)
		LCDpacket[i+1] = *pBuffer++;

    hdr.slave.addr = Address;
    hdr.slave.fmt = I2C_ADDRFMT_7BIT;
    hdr.len = NbData + 1;  // 1 extra for control (mode) bit
    hdr.stop = 1;

    SETIOV(&sv[0], &hdr, sizeof(hdr));
    SETIOV(&sv[1], &LCDpacket[0], NbData + 1); // 1 extra for control (mode) bit
      // int devctlv(int filedes, int dcmd,     int sparts, int rparts, const iov_t *sv, const iov_t *rv, int *dev_info_ptr);
    status = devctlv(fd, DCMD_I2C_SEND, 2, 0, sv, NULL, NULL);

    if (status != EOK)
    	printf("status = %s\n", strerror ( status ));

    return status;
}

int Initialise_I2C_Resource(){
	int file;
	int error;
	volatile uint8_t LCDi2cAdd = 0x3C;
	_Uint32t speed = 10000; // nice and slow (will work with 200000)
	LCD_Data_struct td;

	// Create the mutex
	pthread_mutex_init(&td.LCD_mutex,NULL);		// pass NULL as the attr parameter to use the default attributes for the mutex

	// Open I2C resource and set it up
	if ((file = open("/dev/i2c1",O_RDWR)) < 0)	  // OPEN I2C1
		printf("Error while opening Device File.!!\n");
	else
		printf("I2C1 Opened Successfully\n");

	error = devctl(file,DCMD_I2C_SET_BUS_SPEED,&(speed),sizeof(speed),NULL);  // Set Bus speed
	if (error)
	{
		fprintf(stderr, "Error setting the bus speed: %d\n",strerror ( error ));
		return(EXIT_FAILURE);
	}
	else{
		printf("Bus speed set = %d\n", speed);
		Initialise_LCD(file, LCDi2cAdd);
		return(file);
	}
}

void Initialise_LCD (int fd, _Uint32t LCDi2cAdd)
{
	uint8_t	LCDcontrol = 0x00;

	//   Initialise the LCD display via the I2C bus
	LCDcontrol = 0x38;  // data byte for FUNC_SET_TBL1
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x39; // data byte for FUNC_SET_TBL2
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x14; // data byte for Internal OSC frequency
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x79; // data byte for contrast setting
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x50; // data byte for Power/ICON control Contrast set
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x6C; // data byte for Follower control
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x0C; // data byte for Display ON
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x01; // data byte for Clear display
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}

void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column)
{
	uint8_t position = 0x80; // SET_DDRAM_CMD (control bit)
	uint8_t rowValue = 0;
	uint8_t	LCDcontrol = 0;
	if (row == 1)
		rowValue = 0x40;     // memory location offset for row 1
	position = (uint8_t)(position + rowValue + column);
	//printf("Position: %#01x\n", position);
	LCDcontrol = position;
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}


void LCD_write (LCD_Data_struct *LCD_Data)
{
	//LCD_Data_struct* LCD_Data = (LCD_Data_struct*) data;
	SetCursor(LCD_Data->fd, LCD_Data->Address,LCD_Data->row,LCD_Data->col); // set cursor on LCD to first position first line
	I2cWrite_(LCD_Data->fd, LCD_Data->Address, DATA_SEND,
			&LCD_Data->LCDtext[0], sizeof(LCD_Data->LCDtext));		// write new data to I2C

}
