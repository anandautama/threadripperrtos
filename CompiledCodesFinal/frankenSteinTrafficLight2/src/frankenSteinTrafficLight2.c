#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <errno.h>
#include <stdbool.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <share.h>
#include <time.h>
#include <errno.h>
#include <math.h>

// hardware specific include
#include <hw/inout.h>      // for in32() and out32();
#include <sys/mman.h>      // for mmap_device_io();
#include <stdint.h>        // for unit32 types
#include <sys/neutrino.h>  // for ThreadCtl( _NTO_TCTL_IO_PRIV , NULL)
#include <sched.h>
#include <sys/procmgr.h>
// end of hardware specific include

// lcd hardware specific include
#include "stdint.h"
#include <fcntl.h>
#include <devctl.h>
#include <hw/i2c.h>
#include <unistd.h>
#include <sched.h>
// end of lcd hardware specific include

#define DATA_SEND 0x40  // sets the Rs value high
#define Co_Ctrl   0x00  // mode to tell LCD we are sending a single command
typedef struct
{
	int fd;
	uint8_t Address;
	uint8_t mode;
	pthread_mutex_t mutex;
	// extra stuff added from sams
	char LegendChar;
	int CursorFirstPos;
	int CursorSecondPos;
} LCD_connect;

#include "Traffic_Light_System_Def.h"

char * progname = "TrafficLight2.c";
#define MY_PULSE_CODE _PULSE_CODE_MINAVAIL

#define ATTACH_POINT "TrafficNode2"

int count = 0;

typedef union
{
	struct _pulse   pulse;
    // your other message structures would go here too
} timer_message;

typedef struct {
	struct itimerspec itimeGreenStraightNorth;
	struct itimerspec itimeGreenStraightSouth;
	struct itimerspec itimeGreenStraightEast;
	struct itimerspec itimeGreenStraightWest;

	struct itimerspec itimeGreenStraightNorthSouth;
	struct itimerspec itimeGreenStraightEastWest;

	struct itimerspec itimerYellowCond;
	struct itimerspec itimerRedCond;
	//struct itimerspec itimerStraight[4];

	struct itimerspec itimeGreenRightNorth;
	struct itimerspec itimeGreenRightSouth;
	struct itimerspec itimeGreenRightEast;
	struct itimerspec itimeGreenRightWest;

	struct itimerspec itimeGreenRightNorthSouth;
	struct itimerspec itimeGreenRightEastWest;

	struct itimerspec itimerYellowRightCond;
	struct itimerspec itimerRedRightCond;

	//struct itimerspec itimerRight[4];

} timer_list_trafficlight;

typedef struct{
	struct itimerspec itimeGreenPedestrian;
	struct itimerspec itimeFlashPedestrian;
} timer_list_pedestrian;

// current time (start time)
// how long the process state duration
typedef struct {
	enum trafficLightStates TrafficCurState;
	int greenTimingStart;
	int greenTimingDuration;
	int TrafficState_client_ready;
	int TrafficState_lcd_ready;
	int TrafficState_ped_ready;
	pthread_mutex_t trafficStateMutex;
	pthread_cond_t trafficState_FSM_condvar;

	int sensorNewData;
	// were using for sensor based
	int NorthStraightSensor;
	int SouthStraightSensor;
	int EastStraightSensor;
	int WestStraightSensor;
	int NRSensor; // 1 0
	int SRSensor; // 1 0
	int ERSensor; // 1 0
	int WRSensor; // 1 0
	char sensorInput;
	pthread_mutex_t sensorMutex;

	timer_list_trafficlight trafficTimings[3];
	pthread_mutex_t trafficTimingMutex;

	enum pedStates PedsCurState;
	int PedState_data_ready;
	int PedState_lcd_ready;
	pthread_mutex_t pedStateMutex;
	pthread_cond_t pedState_FSM_condvar;

	int pedStateNewData;
	int NPStatus; // 1 0
	int SPStatus; // 1 0
	int EPStatus; // 1 0
	int WPStatus; // 1 0
	char buttonInput;
	pthread_mutex_t buttonMutex; // add a mutex object into the structure we want to protect
	int channel_ID;

	timer_list_pedestrian pedTimings;
	pthread_mutex_t pedTimingMutex;

	int mode_Status;
	pthread_mutex_t mode_Status_mutex;

	// green will be the change 7 of them
	struct itimerspec itime[12];
	pthread_mutex_t itimeMutex;

	enum Rail_Crossing_States TrainCrossingCurState;
	pthread_mutex_t trainCrossingStateMutex;

	int hardware_Status;
	int current_mode_Status;

	int force_state_flag;
	int force_state_target;
	pthread_mutex_t forceModeMutex;

} thread_data;

typedef struct {
	enum Rail_Crossing_States Rail_State;
	int state_data_ready[3];
	pthread_mutex_t client_FSM_mutex;
	pthread_cond_t client_FSM_condvar;

	int Train_Counter_A;
	int Train_Counter_B;
	pthread_mutex_t FSM_IO_mutex;
} railway_thread_data;

typedef struct {
	int thread_num;
	thread_data *resource_data;
	const char *attach_point;
} client_data;

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
	int commandId;
    int packetData;     // our data
} command_packet;

typedef struct{
	int thread_num;
	// 0 for the traffic light
	// 1 for the pedestrian light
	thread_data *resource_data;
	LCD_connect *lcd_resource;
} Hybrid_LCD_FSM_Resource;

void *trafficlightFSM_thread(void *data);
void SingleStep_TrafficLight_SM(enum trafficLightStates *CurState, thread_data *data );
void *traffic_sensor_button_input_thread(void *data);
void TrafficLightPrintState(enum trafficLightStates CurState);

void initializeThread_Resource(thread_data *resource_data);

void *pedestrianFSM_thread(void *data);
void SingleStep_Pedestrian_SM(enum pedStates *pedestrianCurState, enum trafficLightStates *trafficCurState, thread_data *data);
//void *peds_sensor_button_input_thread(void *data);
void PedStatePrintState(enum pedStates CurState);
void defaultTrafficTiming (timer_list_trafficlight *timerlist);
void defaultPedestrianTiming(timer_list_pedestrian *timerlist);
int allowableForceStateSetter(int forceStateArgs, thread_data *FSM_Resources);

void Traffic_Timer_Setter(enum trafficLightStates localState, timer_list_trafficlight timingRules);
void Pedestrian_Timer_Setter(enum pedStates CurState, timer_list_pedestrian timingRules);

void *client_thread(void *data);
void *server_thread(void *data);

// hardware specific defines
int setup();
const struct sigevent* Inthandler( void* area, int id );
void *interruptKeyPadThread(void *data);
void *LCDthread_ex (void *data);
void *LCDThread_TrafficLight(void *data);
void *LCDThread_Pedestrian(void *data);
void *LCDThread_TrainStatus(void *data);
void SingleStatusUpdate(int CursorFirstPos, int CursorSecondPos, Hybrid_LCD_FSM_Resource *thread_data, char input);
void *InitializeLCD_TrafficLight(void *data);
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData);

// clone variables make it global for easy access
timer_t                 timer_idTrafficLight;
timer_t					timer_idPedestrianLight;
// specific timer things
//timer_t                 timer_id;
struct itimerspec       itime;
struct itimerspec       itime2;
struct itimerspec       itime3;
bool check_control_command = false;

time_t time_of_day;

int main(void) {
	printf("Traffic Light 2 With Periodic Timer\n");

	time_of_day = time(NULL);
	printf("It is now: %s", ctime(&time_of_day));

	pthread_t trafficlight_stateMachine_pthread, car_sensor_pthread;

	pthread_t pedestrian_stateMachine_pthread, peds_button_pthread;

	pthread_t trafficLight_server_pthread;

	thread_data resource_data;
	initializeThread_Resource(&resource_data);

	LCD_connect td;
	Hybrid_LCD_FSM_Resource hybridTraffic;
	Hybrid_LCD_FSM_Resource hybridPedestrian;

	resource_data.hardware_Status = 0;
	//int LcdResult = 1000;
	int LcdResult = Initialise_I2C_Resource();
	//if(LcdResult == EXIT_FAILURE){
	if(LcdResult != EXIT_FAILURE){
		printf("LCD Initialized\n");
		// make sure we mutex the initial lcd
		pthread_mutex_init(&td.mutex,NULL);

		volatile uint8_t LCDi2cAdd = 0x3C;
		pthread_mutex_lock(&td.mutex);		//lock the function to make sure the variables are protected
			td.fd     = LcdResult;
			td.Address= LCDi2cAdd;
			td.mode   = DATA_SEND;
			td.CursorFirstPos = 0;
			td.CursorSecondPos = 0;
			td.LegendChar = 'E';
		pthread_mutex_unlock(&td.mutex);	//unlock the functions to release the variables for use by other functions

		hybridTraffic.lcd_resource = &td;
		hybridTraffic.resource_data = &resource_data;
		hybridTraffic.thread_num = 0;

		hybridPedestrian.lcd_resource = &td;
		hybridPedestrian.resource_data = &resource_data;
		hybridPedestrian.thread_num = 1;

		uint8_t LCDdata[2] = {1,5};
		int result = I2cWrite_(td.fd, LCDi2cAdd, DATA_SEND, &LCDdata[0], sizeof(LCDdata));
		if(result != EXIT_FAILURE){
			printf("12cwrite is success");
			resource_data.hardware_Status = 1;
			if(resource_data.hardware_Status){
				pthread_create(NULL,NULL,InitializeLCD_TrafficLight, &hybridPedestrian);
			}
		}
		else{
			printf("12cwrite is not success");
			resource_data.hardware_Status = 0;
		}
	}
	else {
		hybridTraffic.lcd_resource = &td;
		hybridTraffic.resource_data = &resource_data;
		hybridTraffic.thread_num = 0;

		hybridPedestrian.lcd_resource = &td;
		hybridPedestrian.resource_data = &resource_data;
		hybridPedestrian.thread_num = 1;

		resource_data.hardware_Status = 0;
		printf("LCD Cannot Be Initialized\n");
	}

	pthread_t trafficLight_client_pthread_Railway;
	pthread_t trafficLight_client_pthread_CC;

	client_data client_data_CC;
	client_data_CC.resource_data = &resource_data;
	client_data_CC.attach_point = CC_ATTACH_POINT;
	client_data_CC.thread_num = 0;

	client_data client_data_RW;
	client_data_RW.resource_data = &resource_data;
	client_data_RW.attach_point = RW_ATTACH_POINT;
	client_data_RW.thread_num = 1;

	void *pthread_end;
	// end of setup

	pthread_t keyPadThread;
	// launch thread
	int result = setup();
	if(result == EXIT_SUCCESS){
		printf("Keypad initialized \n");
		pthread_create(&keyPadThread, NULL, interruptKeyPadThread, &resource_data);
		pthread_detach(keyPadThread);
	}
	else{
		printf("Keypad cannot be init\n");
		pthread_create(&car_sensor_pthread, NULL, traffic_sensor_button_input_thread, &resource_data);
		pthread_detach(car_sensor_pthread);
	}

	pthread_create(&trafficLight_client_pthread_CC,NULL,client_thread, &client_data_CC);
	pthread_create(&trafficLight_client_pthread_Railway,NULL,client_thread, &client_data_RW);
	pthread_create(&trafficLight_server_pthread,NULL,server_thread, &resource_data);

	if(LcdResult != EXIT_FAILURE){
			pthread_create(&trafficlight_stateMachine_pthread, NULL, trafficlightFSM_thread, &hybridTraffic);
			pthread_create(&pedestrian_stateMachine_pthread, NULL, pedestrianFSM_thread, &hybridPedestrian);
		}
		else {
			printf("LCD Cannot Be Initialized\n");
	}

	pthread_join(trafficlight_stateMachine_pthread, &pthread_end);
	pthread_join(pedestrian_stateMachine_pthread, &pthread_end);

	// communication joining
	pthread_join(trafficLight_client_pthread_CC, &pthread_end);
	pthread_join(trafficLight_client_pthread_Railway, &pthread_end);

	pthread_join(trafficLight_server_pthread, &pthread_end);

	printf("All good. Main Terminated...\n\n");
	return EXIT_SUCCESS;
}

// functionalize the resource init code
void initializeThread_Resource(thread_data *resource_data){
	// initializing the variable
	resource_data->TrafficCurState = NSEWRed_NorthSouth;
	resource_data->greenTimingStart = 0;
	resource_data->greenTimingDuration = 0;
	resource_data->TrafficState_client_ready = 0;
	resource_data->TrafficState_lcd_ready = 0;
	resource_data->TrafficState_ped_ready = 0;
	resource_data->trafficStateMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&resource_data->trafficStateMutex,NULL);
	pthread_cond_init(&resource_data->trafficState_FSM_condvar,NULL);

	resource_data->NorthStraightSensor = 0;
	resource_data->SouthStraightSensor = 0;
	resource_data->EastStraightSensor = 0;
	resource_data->WestStraightSensor = 0;

	resource_data->NRSensor = 0;
	resource_data->SRSensor = 0;
	resource_data->ERSensor = 0;
	resource_data->WRSensor = 0;
	resource_data->sensorInput = 'o';
	resource_data->sensorMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&resource_data->sensorMutex,NULL);

	resource_data->PedsCurState = NSEWPRed;
	resource_data->PedState_data_ready = 0;
	resource_data->pedStateMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&resource_data->pedStateMutex,NULL);
	pthread_cond_init(&resource_data->pedState_FSM_condvar,NULL);

	resource_data->pedStateNewData = 0;
	resource_data->NPStatus = 0;
	resource_data->SPStatus = 0;
	resource_data->EPStatus = 0;
	resource_data->WPStatus = 0;

	resource_data->buttonInput = 'o';
	resource_data->buttonMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&resource_data->buttonMutex,NULL);
	resource_data->channel_ID = ChannelCreate(0);

	resource_data->TrainCrossingCurState = Train_NotPresent;
	resource_data->trainCrossingStateMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&resource_data->trainCrossingStateMutex,NULL);

	resource_data->mode_Status = MODE_ID_DEFAULT;
	pthread_mutex_init(&resource_data->mode_Status_mutex, NULL);

	pthread_mutex_init(&resource_data->trafficTimingMutex, NULL);
	pthread_mutex_init(&resource_data->pedTimingMutex, NULL);
	pthread_mutex_init(&resource_data->mode_Status_mutex, NULL);

	pthread_mutex_lock(&resource_data->mode_Status_mutex);
	resource_data->current_mode_Status = MODE_ID_DEFAULT;
	pthread_mutex_unlock(&resource_data->mode_Status_mutex);


	defaultTrafficTiming(&resource_data->trafficTimings[0]);
	peakTrafficTiming(&resource_data->trafficTimings[1]);
	nightTrafficTiming(&resource_data->trafficTimings[2]);
	defaultPedestrianTiming(&resource_data->pedTimings);

	pthread_mutex_init(&resource_data->forceModeMutex, NULL);
	pthread_mutex_lock(&resource_data->forceModeMutex);
	resource_data->force_state_flag = 0;
	resource_data->force_state_target = 0;
	pthread_mutex_unlock(&resource_data->forceModeMutex);

}

void defaultTrafficTiming (timer_list_trafficlight *timerlist){
	// all the straigth timings
	timerlist->itimeGreenStraightNorth.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightSouth.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightEast.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightEast.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightEast.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightEast.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightWest.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightWest.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightWest.it_interval.tv_nsec = 0;	   // 1 second
	// all the straight timings

	//combo timings
	timerlist->itimeGreenStraightNorthSouth.it_value.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_interval.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightEastWest.it_value.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_interval.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_interval.tv_nsec = 0;	   // 1 second
	// end of combo timings

	// yellow condition
	timerlist->itimerYellowCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerYellowCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowCond.it_interval.tv_nsec = 0;	   // 1 second

	// red condition
	timerlist->itimerRedCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerRedCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerRedCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerRedCond.it_interval.tv_nsec = 0;	   // 1 second


	timerlist->itimeGreenRightNorth.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightNorth.it_value.tv_nsec = 1;	   // 1 second
	timerlist->itimeGreenRightNorth.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightNorth.it_interval.tv_nsec = 1;	   // 1 second

	timerlist->itimeGreenRightSouth.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightSouth.it_value.tv_nsec = 1;	   // 1 second
	timerlist->itimeGreenRightSouth.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightSouth.it_interval.tv_nsec = 1;	   // 1 second

	timerlist->itimeGreenRightEast.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightEast.it_value.tv_nsec = 1;	   // 1 second
	timerlist->itimeGreenRightEast.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightEast.it_interval.tv_nsec = 1;	   // 1 second

	timerlist->itimeGreenRightWest.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightWest.it_value.tv_nsec = 1;	   // 1 second
	timerlist->itimeGreenRightWest.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightWest.it_interval.tv_nsec = 1;	   // 1 second

	// yellow condition on the right
	timerlist->itimerYellowRightCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowRightCond.it_value.tv_nsec = 1;	   // 1 second
	timerlist->itimerYellowRightCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowRightCond.it_interval.tv_nsec = 1;	   // 1 second

	// red condition on the right
	timerlist->itimerRedRightCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerRedRightCond.it_value.tv_nsec = 1;	   // 1 second
	timerlist->itimerRedRightCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerRedRightCond.it_interval.tv_nsec = 1;	   // 1 second

	// combo timings
	timerlist->itimeGreenRightNorthSouth.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightEastWest.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_interval.tv_nsec = 0;	   // 1 second
}


void peakTrafficTiming (timer_list_trafficlight *timerlist){
	// all the straigth timings
	timerlist->itimeGreenStraightNorth.it_value.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_interval.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightSouth.it_value.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_interval.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightEast.it_value.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightEast.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightEast.it_interval.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightEast.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightWest.it_value.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightWest.it_interval.tv_sec = 3;	   // 1 second
	timerlist->itimeGreenStraightWest.it_interval.tv_nsec = 0;	   // 1 second
	// all the straight timings

	//combo timings
	timerlist->itimeGreenStraightNorthSouth.it_value.tv_sec = 5;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_interval.tv_sec = 5;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightEastWest.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_interval.tv_nsec = 0;	   // 1 second
	// end of combo timings

	// yellow condition
	timerlist->itimerYellowCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerYellowCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowCond.it_interval.tv_nsec = 0;	   // 1 second

	// red condition
	timerlist->itimerRedCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerRedCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerRedCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerRedCond.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightNorth.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightNorth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightNorth.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightNorth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightSouth.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightSouth.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightEast.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightEast.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightEast.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightEast.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightWest.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightWest.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenRightWest.it_interval.tv_nsec = 0;	   // 1 second

	// yellow condition on the right
	timerlist->itimerYellowRightCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowRightCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerYellowRightCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowRightCond.it_interval.tv_nsec = 0;	   // 1 second

	// red condition on the right
	timerlist->itimerRedRightCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerRedRightCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerRedRightCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerRedRightCond.it_interval.tv_nsec = 0;	   // 1 second

	// combo timings
	timerlist->itimeGreenRightNorthSouth.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightEastWest.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_interval.tv_nsec = 0;	   // 1 second
}

void nightTrafficTiming (timer_list_trafficlight *timerlist){
	// all the straigth timings
	timerlist->itimeGreenStraightNorth.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightNorth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightSouth.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightEast.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightEast.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightEast.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightEast.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightWest.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightWest.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimeGreenStraightWest.it_interval.tv_nsec = 0;	   // 1 second
	// all the straight timings

	//combo timings
	timerlist->itimeGreenStraightNorthSouth.it_value.tv_sec = 3600;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_interval.tv_sec = 3600;	   // 1 second
	timerlist->itimeGreenStraightNorthSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenStraightEastWest.it_value.tv_sec = 3600;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_interval.tv_sec = 3600;	   // 1 second
	timerlist->itimeGreenStraightEastWest.it_interval.tv_nsec = 0;	   // 1 second
	// end of combo timings

	// yellow condition
	timerlist->itimerYellowCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerYellowCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowCond.it_interval.tv_nsec = 0;	   // 1 second
	// end of yellow

	// red condition
	timerlist->itimerRedCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerRedCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerRedCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerRedCond.it_interval.tv_nsec = 0;	   // 1 second
	//end of red

	timerlist->itimeGreenRightNorth.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightNorth.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightSouth.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightSouth.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightEast.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEast.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightEast.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEast.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightWest.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightWest.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightWest.it_interval.tv_nsec = 0;	   // 1 second

	// yellow condition on the right
	timerlist->itimerYellowRightCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowRightCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerYellowRightCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerYellowRightCond.it_interval.tv_nsec = 0;	   // 1 second

	// red condition on the right
	timerlist->itimerRedRightCond.it_value.tv_sec = 1;	   // 1 second
	timerlist->itimerRedRightCond.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimerRedRightCond.it_interval.tv_sec = 1;	   // 1 second
	timerlist->itimerRedRightCond.it_interval.tv_nsec = 0;	   // 1 second

	// combo timings
	timerlist->itimeGreenRightNorthSouth.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightNorthSouth.it_interval.tv_nsec = 0;	   // 1 second

	timerlist->itimeGreenRightEastWest.it_value.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_value.tv_nsec = 0;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_interval.tv_sec = 2;	   // 1 second
	timerlist->itimeGreenRightEastWest.it_interval.tv_nsec = 0;	   // 1 second
}

void defaultPedestrianTiming(timer_list_pedestrian *timerlist){
	timerlist->itimeFlashPedestrian.it_value.tv_sec = 1;
	timerlist->itimeFlashPedestrian.it_value.tv_nsec = 0;
	timerlist->itimeFlashPedestrian.it_interval.tv_sec = 0;
	timerlist->itimeFlashPedestrian.it_interval.tv_nsec = 0;

	timerlist->itimeGreenPedestrian.it_value.tv_sec = 1;
	timerlist->itimeGreenPedestrian.it_value.tv_nsec = 0;
	timerlist->itimeGreenPedestrian.it_interval.tv_sec = 0;
	timerlist->itimeGreenPedestrian.it_interval.tv_nsec = 0;

}

void *trafficlightFSM_thread(void *data){
	printf("---------Traffic Light Thread Started\n");
	int rcvid;
	timer_message time_msg;
	Hybrid_LCD_FSM_Resource *FSM_Resources = (Hybrid_LCD_FSM_Resource*)data;

	// initializer
	struct sigevent event;
	int chid;
	chid = ChannelCreate(0);
	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf(stderr, "%s:  couldn't ConnectAttach to self! \n", progname);
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);

	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_idTrafficLight) == -1)
	{
	   printf (stderr, "%s:  couldn't create a timer, errno %d \n", progname, errno);
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// modification added here
	// 1 second delay interval timer
	itime.it_value.tv_sec = 1;	   // 1 second
	itime.it_value.tv_nsec = 0;    // 0 second
	itime.it_interval.tv_sec = 1;  // 1 second
	itime.it_interval.tv_nsec = 0; // 0 second

	// 2 second delay interval timer
	itime2.it_value.tv_sec = 2; // 2 second
	itime2.it_value.tv_nsec = 0; // 0 secomd
	itime2.it_interval.tv_sec = 2; // 2 second
	itime2.it_interval.tv_nsec = 0; // 0 second

	// immediate delay interval timer
	itime3.it_value.tv_sec = 0;		// 1 second
	itime3.it_value.tv_nsec = 1;    // 100 million nsecs = .1 nsecs
	itime3.it_interval.tv_sec = 0;  // 1 second
	itime3.it_interval.tv_nsec = 0; // 100 million nsecs = .1 nsecs

	FSM_Resources->resource_data->TrafficCurState = NSEWRed_NorthSouth;
	timer_settime(timer_idTrafficLight, 0, &itime3, NULL);
	while (1){
		rcvid = MsgReceive(chid, &time_msg, sizeof(time_msg), NULL);
		if (rcvid == 0){
			if(time_msg.pulse.code == MY_PULSE_CODE){
				SingleStep_TrafficLight_SM( &FSM_Resources->resource_data->TrafficCurState,
						FSM_Resources->resource_data );

				pthread_mutex_lock(&FSM_Resources->resource_data->pedStateMutex);
				enum trafficLightStates localState = FSM_Resources->resource_data->TrafficCurState;
				pthread_mutex_unlock(&FSM_Resources->resource_data->pedStateMutex);

				TrafficLightPrintStateFlipped(localState);
				if(FSM_Resources->resource_data->hardware_Status){
					pthread_create(NULL,NULL,LCDThread_TrafficLight, FSM_Resources);
					pthread_create(NULL,NULL,LCDThread_TrainStatus, FSM_Resources);
				}

				pthread_mutex_lock(&FSM_Resources->resource_data->mode_Status_mutex);
				int localMode = FSM_Resources->resource_data->mode_Status;
				pthread_mutex_unlock(&FSM_Resources->resource_data->mode_Status_mutex);

				timer_list_trafficlight localTimerList;
				if(localMode == MODE_ID_DEFAULT){
					localTimerList = FSM_Resources->resource_data->trafficTimings[0];
				}
				else if(localMode == MODE_ID_PEAK_TIME){
					localTimerList = FSM_Resources->resource_data->trafficTimings[1];
				}
				else if(localMode == MODE_ID_NIGHT_TIME){
					localTimerList = FSM_Resources->resource_data->trafficTimings[2];
				}

				pthread_mutex_lock(&FSM_Resources->resource_data->forceModeMutex);
				int local_force_state_flag = FSM_Resources->resource_data->force_state_flag;
				int local_force_state_target = FSM_Resources->resource_data->force_state_target;
				pthread_mutex_unlock(&FSM_Resources->resource_data->forceModeMutex);

				printf("local force state flag %d\n", local_force_state_flag);
				printf("local force state flag %d\n", local_force_state_target);
				printf("localState being used %d\n", localState);

				if(local_force_state_flag && (localState == local_force_state_target)){

					printf("Force State Entered\n");

					//timer_list_trafficlight localTimerList = FSM_Resources->resource_data->trafficTimings[2];
					// set a timer for 1 hour

					struct itimerspec itimeGreenStraightNorthSouth;
					itimeGreenStraightNorthSouth.it_value.tv_sec = 3600;	   // 1 second
					itimeGreenStraightNorthSouth.it_value.tv_nsec = 0;	   // 1 second
					itimeGreenStraightNorthSouth.it_interval.tv_sec = 3600;	   // 1 second
					itimeGreenStraightNorthSouth.it_interval.tv_nsec = 0;	   // 1 second


					timer_settime(timer_idTrafficLight, 0 , &itimeGreenStraightNorthSouth, NULL);
				}else{
					Traffic_Timer_Setter(localState, localTimerList);
				}
			}
		}
		fflush(stdout);
	}
}

void Traffic_Timer_Setter(enum trafficLightStates localState, timer_list_trafficlight timingRules){
	switch (localState){
			case NSEWRed_NorthSouth:
				// printf("In current state: NSEWRed_NorthSouth\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerRedCond, NULL);
				break;
			case NSEWRed_NorthSouth_Special_NoTrain:
				// printf("In current state: NSEWRed_NorthSouth_Special_NoTrain\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerRedCond, NULL);
				break;
			case NSEWRed_NorthSouth_Special_TrainCond:
				// printf("In current state: NSEWRed_NorthSouth_Special_TrainCond\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerRedCond, NULL);
				break;
			case NSEWRed_EastWest:
				// printf("In current state: NSEWRed_EastWest\n");
				timer_settime(timer_idTrafficLight, 0, &timingRules.itimerRedCond, NULL);
				break;
			case NSEWRed_EastWest_Special_NoTrain:
				// printf("In current state: NSEWRed_EastWest_Special_NoTrain\n");
				timer_settime(timer_idTrafficLight, 0, &timingRules.itimerRedCond, NULL);
				break;
			case NSGreen:
				// printf("In current state: NSGreen\n");
				timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenStraightNorth, NULL);
				break;
			case NSYellow:
				// printf("In current state: NSYellow\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			case EWGreen:
				// printf("In current state: EWGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenStraightEastWest, NULL);
				break;
			case EWYellow:
				// printf("In current state: EWYellow\n");
				timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			case NR_SRGreen:
				// printf("In current state: NR_SRGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenRightNorthSouth, NULL);
				break;
			case NR_SRYellow:
				// printf("In current state: NR_SRYellow\n");
				timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			case ER_WRGreen:
				// printf("In current state: ER_WRGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenRightEastWest, NULL);
				break;
			case N_NRGreen:
				// printf("In current state: N_NRGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenRightNorth, NULL);
				break;
			case NGreen_NRYellow:
				// printf("In current state: NGreen_NRYellow\n");
				timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			case NGreen:
				// printf("In current state: NGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenStraightNorth, NULL);
				break;
			case S_SRGreen:
				// printf("In current state: S_SRGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenRightSouth, NULL);
				break;
			case SGreen_SRYellow:
				// printf("In current state: SGreen_SRYellow\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			case SYellow:
				// printf("In current state: SYellow\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			// this is the handler for the sgreen timers
			case SGreenTrain:
			case SGreenNoTrain:
				// printf("In current state: SGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenStraightSouth, NULL);
				break;
			case E_ERGreen:
				// printf("In current state: E_ERGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenRightEast, NULL);
				break;
			case EGreen_ERYellow:
				// printf("In current state: EGreen_ERYellow\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			case EGreenTrain:
			case EGreenNoTrain:
				// printf("In current state: EGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenStraightEast, NULL);
				break;
			case W_WRGreen:
				// printf("In current state: W_WRGreen\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenRightWest, NULL);
				break;
			case WGreen_WRYellow:
				// printf("In current state: WGreen_WRYellow\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			case WGreen:
				// printf("In current state: WGreen\n");
				timer_settime(timer_idTrafficLight, 0, &timingRules.itimeGreenStraightWest, NULL);
				break;
			case ER_WRYellow:
				// printf("In current state: ER_WRYellow\n");
                timer_settime(timer_idTrafficLight, 0, &timingRules.itimerYellowCond, NULL);
				break;
			default:
				// printf("State Not Handled\n");
                timer_settime(timer_idTrafficLight, 0, &itime, NULL);
				break;
	}
}

// calculating the next state
void SingleStep_TrafficLight_SM(enum trafficLightStates *CurState, thread_data *data ){

	pthread_mutex_lock(&data->forceModeMutex);
	int local_force_state_flag = data->force_state_flag;
	int local_force_state_target = data->force_state_target;
	pthread_mutex_unlock(&data->forceModeMutex);

	switch (*CurState){
			case NSEWRed_NorthSouth:
				pthread_mutex_lock(&data->trainCrossingStateMutex);
				printf("South Right Sensor: %d\n", data->NRSensor);
				printf("North Right Sensor: %d\n", data->SRSensor);

				if(data->TrainCrossingCurState == Train_NotPresent){
					printf("Train is not Present\n");
					pthread_mutex_lock(&data->sensorMutex);

					if(((data->NRSensor == 1) && (data->SRSensor == 1))
							|| ((local_force_state_flag) && (local_force_state_target == NR_SRGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = NR_SRGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;

						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if (((data->NRSensor == 1) && (data->SRSensor == 0))
							|| ((local_force_state_flag) && (local_force_state_target == N_NRGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = N_NRGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if ((data->NRSensor == 0 && data->SRSensor == 1)
							|| ((local_force_state_flag) && (local_force_state_target == S_SRGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = S_SRGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if ((data->NRSensor == 0 && data->SRSensor == 1)
							|| ((local_force_state_flag) && (local_force_state_target == NSGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = NSGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else{
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = NSGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					pthread_mutex_unlock(&data->sensorMutex);
				}
				else if (data->TrainCrossingCurState == Train_Present){
					printf("Train is present\n");
					pthread_mutex_lock(&data->sensorMutex);

					if(data->NRSensor == 1){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = NR_SRGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;

						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if ((data->NRSensor == 0) && (data->SRSensor == 1)){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = S_SRGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if ((data->NRSensor == 0) && (data->SRSensor == 0)){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = SGreenTrain;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;

						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else{
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = SGreenTrain;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;

						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					pthread_mutex_unlock(&data->sensorMutex);
				}
				pthread_mutex_unlock(&data->trainCrossingStateMutex);
				break;
			case NSEWRed_EastWest:
				// check all the train sensor
				pthread_mutex_lock(&data->trainCrossingStateMutex);
				printf("West Right Sensor: %d\n", data->ERSensor);
				printf("East Right Sensor: %d\n", data->WRSensor);

				if(data->TrainCrossingCurState == Train_NotPresent){
					printf("Train not present\n");
					pthread_mutex_unlock(&data->trainCrossingStateMutex);
					// check all the North South turning sensor
					pthread_mutex_lock(&data->sensorMutex);
					if(((data->ERSensor == 1) && (data->WRSensor == 1))
							|| ((local_force_state_flag) && (local_force_state_target == ER_WRGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = ER_WRGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;

						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if (((data->ERSensor == 1) && (data->WRSensor == 0))
							|| ((local_force_state_flag) && (local_force_state_target == E_ERGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = E_ERGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if (((data->ERSensor == 0) && (data->WRSensor == 1))
							|| ((local_force_state_flag) && (local_force_state_target == W_WRGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = W_WRGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else if (((data->ERSensor == 0) && (data->WRSensor == 0))
							|| ((local_force_state_flag) && (local_force_state_target == EWGreen))
					){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = EWGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else{
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = EWGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					pthread_mutex_unlock(&data->sensorMutex);
				}
				// train does not exist
				else if(data->TrainCrossingCurState == Train_Present){
					printf("Train is present\n");
					pthread_mutex_unlock(&data->trainCrossingStateMutex);
					pthread_mutex_lock(&data->sensorMutex);
					if(data->ERSensor == true ){
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = E_ERGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;

						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					else {
						pthread_mutex_lock(&data->trafficStateMutex);
						*CurState = EWGreen;
						data->TrafficState_client_ready = 1;
                        data->TrafficState_lcd_ready = 1;
						data->TrafficState_ped_ready = 1;
						pthread_cond_broadcast(&data->trafficState_FSM_condvar);
						pthread_mutex_unlock(&data->trafficStateMutex);

					}
					pthread_mutex_unlock(&data->sensorMutex);
				}
				//
				break;
			case E_ERGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EGreen_ERYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case EGreen_ERYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EGreenNoTrain;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case S_SRGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = SGreen_SRYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case SGreen_SRYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = SGreenTrain;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case SGreenTrain:
				pthread_mutex_lock(&data->sensorMutex);
				data->SRSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = SYellow;
				data->TrafficState_client_ready = 1;
				data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case SGreenNoTrain:
				// only clear south during the SGreen Condition
				pthread_mutex_lock(&data->sensorMutex);
				data->SRSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);
				// train not present

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSGreen;
				data->TrafficState_client_ready = 1;
				data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);

				break;
			case SYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSEWRed_EastWest;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NSGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NSYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSEWRed_EastWest;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case EWGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EWYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case EWYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSEWRed_NorthSouth;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NR_SRGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NR_SRYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NR_SRYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSEWRed_NorthSouth_Special_NoTrain;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NSEWRed_NorthSouth_Special_NoTrain:
				pthread_mutex_lock(&data->sensorMutex);
				data->NRSensor = 0;
				data->SRSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case N_NRGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NGreen_NRYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NGreen_NRYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NGreen:
				// clear the north sensor
				pthread_mutex_lock(&data->sensorMutex);
				data->NRSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case W_WRGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = WGreen_WRYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case EGreenTrain:
				// clear the east sensor
				pthread_mutex_lock(&data->sensorMutex);
				data->ERSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EWGreen;
				data->TrafficState_client_ready = 1;
				data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case EGreenNoTrain:
				// clear the east sensor
				pthread_mutex_lock(&data->sensorMutex);
				data->ERSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EWGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case ER_WRGreen:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = ER_WRYellow;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case ER_WRYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = NSEWRed_EastWest_Special_NoTrain;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NSEWRed_EastWest_Special_NoTrain:
				// clear the north and south
				pthread_mutex_lock(&data->sensorMutex);
				data->ERSensor = 0;
				data->WRSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EWGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case WGreen_WRYellow:
				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = WGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case WGreen:
				// clear the west sensor when turning right has been serviced
				pthread_mutex_lock(&data->sensorMutex);
				data->WRSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EWGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
			case NSEWRed_NorthSouth_Special_TrainCond:
				// clear the north sensor when there is a train
				pthread_mutex_lock(&data->sensorMutex);
				data->NRSensor = 0;
				pthread_mutex_unlock(&data->sensorMutex);

				pthread_mutex_lock(&data->trafficStateMutex);
				*CurState = EWGreen;
				data->TrafficState_client_ready = 1;
                data->TrafficState_lcd_ready = 1;
				data->TrafficState_ped_ready = 1;
				pthread_cond_broadcast(&data->trafficState_FSM_condvar);
				pthread_mutex_unlock(&data->trafficStateMutex);
				break;
	}
}

void *traffic_sensor_button_input_thread(void *data){
	printf("-----------Button Thread Started\n");
	thread_data *sensor_Resources = (thread_data*)data;
	while(1)
	{
		char input = 'o';
		scanf("%c", &input); //wait for user input
		char buffer[100];
		printf("char input\n");

		pthread_mutex_lock(&sensor_Resources->sensorMutex);
		sensor_Resources->sensorInput = input;
		sensor_Resources->sensorNewData = true;
		pthread_mutex_unlock(&sensor_Resources->sensorMutex);

		pthread_mutex_lock(&sensor_Resources->buttonMutex);
		sensor_Resources->buttonInput = input;
		sensor_Resources->pedStateNewData = true;
		pthread_mutex_unlock(&sensor_Resources->buttonMutex);

		switch(sensor_Resources->buttonInput){
			case 'a':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->NRSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
			case 'b':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->SRSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
			case 'c':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->ERSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
			case 'd':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->WRSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
			// the straight sensor command
			case 'e':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->NorthStraightSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
			case 'f':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->SouthStraightSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
			case 'g':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->EastStraightSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
			case 'h':
				pthread_mutex_lock(&sensor_Resources->sensorMutex);
				sensor_Resources->WestStraightSensor = 1;
				pthread_mutex_unlock(&sensor_Resources->sensorMutex);
				break;
		}
		switch(sensor_Resources->sensorInput){
			case 'i':
				pthread_mutex_lock(&sensor_Resources->buttonMutex);
				sensor_Resources->NPStatus = 1;
				pthread_mutex_unlock(&sensor_Resources->buttonMutex);
				break;
			case 'j':
				pthread_mutex_lock(&sensor_Resources->buttonMutex);
				sensor_Resources->SPStatus = 1;
				pthread_mutex_unlock(&sensor_Resources->buttonMutex);
				break;
			case 'k':
				pthread_mutex_lock(&sensor_Resources->buttonMutex);
				sensor_Resources->EPStatus = 1;
				pthread_mutex_unlock(&sensor_Resources->buttonMutex);
				break;
			case 'l':
				pthread_mutex_lock(&sensor_Resources->buttonMutex);
				sensor_Resources->WPStatus = 1;
				pthread_mutex_unlock(&sensor_Resources->buttonMutex);
				break;
		}
	}
	}

// flipped
void TrafficLightPrintState(enum trafficLightStates CurState){
	int localState = CurState;
	switch (CurState){
		case NSEWRed_NorthSouth:
			printf("In current state: NSEWRed_NorthSouth\n");
			break;
		case NSEWRed_NorthSouth_Special_NoTrain:
			printf("In current state: NSEWRed_NorthSouth_Special_NoTrain\n");
			break;
		case NSEWRed_NorthSouth_Special_TrainCond:
			printf("In current state: NSEWRed_NorthSouth_Special_TrainCond\n");
			break;
		case NSEWRed_EastWest:
			printf("In current state: NSEWRed_EastWest\n");
			break;
		case NSEWRed_EastWest_Special_NoTrain:
			printf("In current state: NSEWRed_EastWest_Special_NoTrain\n");
			break;
		case NSGreen:
			printf("In current state: NSGreen\n");
			break;
		case NSYellow:
			printf("In current state: NSYellow\n");
			break;
		case EWGreen:
			printf("In current state: EWGreen\n");
			break;
		case EWYellow:
			printf("In current state: EWYellow\n");
			break;
		case NR_SRGreen:
			printf("In current state: NR_SRGreen\n");
			break;
		case NR_SRYellow:
			printf("In current state: NR_SRYellow\n");
			break;
		case ER_WRGreen:
			printf("In current state: ER_WRGreen\n");
			break;
		case N_NRGreen:
			printf("In current state: N_NRGreen\n");
			break;
		case NGreen_NRYellow:
			printf("In current state: NGreen_NRYellow\n");
			break;
		case NGreen:
			printf("In current state: NGreen\n");
			break;
		case S_SRGreen:
			printf("In current state: S_SRGreen\n");
			break;
		case SGreen_SRYellow:
			printf("In current state: SGreen_SRYellow\n");
			break;
		case SYellow:
			printf("In current state: SYellow\n");
			break;
		case SGreenTrain:
			printf("In current state: SGreenTrain\n");
			break;
		case SGreenNoTrain:
			printf("In current state: SGreenNoTrain\n");
			break;
		case E_ERGreen:
			printf("In current state: E_ERGreen\n");
			break;
		case EGreen_ERYellow:
			printf("In current state: EGreen_ERYellow\n");
			break;
		case EGreenTrain:
			printf("In current state: EGreenTrain\n");
			break;
		case EGreenNoTrain:
			printf("In current state: EGreenNoTrain\n");
			break;
		case W_WRGreen:
			printf("In current state: W_WRGreen\n");
			break;
		case WGreen_WRYellow:
			printf("In current state: WGreen_WRYellow\n");
			break;
		case WGreen:
			printf("In current state: WGreen\n");
			break;
		case ER_WRYellow:
			printf("In current state: ER_WRYellow\n");
			break;
		default:
			printf("State Not Handled\n");
			break;
	}
}

void *pedestrianFSM_thread(void *data){
	printf("---------Pedestrian Thread Started\n");
	int rcvid;
	timer_message time_msg;
	Hybrid_LCD_FSM_Resource *FSM_Resources = (Hybrid_LCD_FSM_Resource*)data;

	// initializer
	struct sigevent event;
	int chid;
	chid = ChannelCreate(0);
	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf(stderr, "%s:  couldn't ConnectAttach to self! \n", progname);
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);

	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_idPedestrianLight) == -1)
	{
	   printf (stderr, "%s:  couldn't create a timer, errno %d \n", progname, errno);
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// modification added here
	// 1 second delay interval timer
	itime.it_value.tv_sec = 1;	   // 1 second
	itime.it_value.tv_nsec = 0;    // 0 second
	itime.it_interval.tv_sec = 0;  // 1 second
	itime.it_interval.tv_nsec = 0; // 0 second

	// 2 second delay interval timer
	itime2.it_value.tv_sec = 1; // 2 second
	itime2.it_value.tv_nsec = 0; // 0 secomd
	itime2.it_interval.tv_sec = 1; // 2 second
	itime2.it_interval.tv_nsec = 0; // 0 second

	// immediate delay interval timer
	itime3.it_value.tv_sec = 0;		// 1 second
	itime3.it_value.tv_nsec = 1;    // 100 million nsecs = .1 nsecs
	itime3.it_interval.tv_sec = 0;  // 1 second
	itime3.it_interval.tv_nsec = 0; // 100 million nsecs = .1 nsecs

	FSM_Resources->resource_data->PedsCurState = NSEWPRed;

	timer_settime(timer_idPedestrianLight, 0, &itime3, NULL);
	while (1){
		rcvid = MsgReceive(chid, &time_msg, sizeof(time_msg), NULL);
		if (rcvid == 0){
			//printf("%d\n", time_msg.pulse.code);
			if(time_msg.pulse.code == MY_PULSE_CODE){
				SingleStep_Pedestrian_SM( &FSM_Resources->resource_data->PedsCurState,
						&FSM_Resources->resource_data->TrafficCurState, FSM_Resources->resource_data);

				pthread_mutex_lock(&FSM_Resources->resource_data->pedStateMutex);
				enum pedStates localState = FSM_Resources->resource_data->PedsCurState;
				pthread_mutex_unlock(&FSM_Resources->resource_data->pedStateMutex);

				pthread_mutex_lock(&FSM_Resources->resource_data->pedTimingMutex);
				timer_list_pedestrian localTimerList = FSM_Resources->resource_data->pedTimings;
				pthread_mutex_unlock(&FSM_Resources->resource_data->pedTimingMutex);

				PedStatePrintState(localState);
				if(FSM_Resources->resource_data->hardware_Status){
					pthread_create(NULL,NULL, LCDThread_Pedestrian, FSM_Resources);
				}
				//Pedestrian_Timer_Setter(localState, localTimerList);
			}
		}
		fflush(stdout);
	}
}

void SingleStep_Pedestrian_SM(enum pedStates *pedestrianCurState, enum trafficLightStates *trafficCurState, thread_data *data){
	switch(*pedestrianCurState){
			case NSEWPRed:
					pthread_mutex_lock(&data->trafficStateMutex);

					 // test the condition and wait until it is true
					 while(!data->TrafficState_ped_ready)
					 {
						 pthread_cond_wait(&data->trafficState_FSM_condvar, &data->trafficStateMutex);
						 printf("cons data_ready = %d\n",data->TrafficState_client_ready);
					 }

					pthread_mutex_lock(&data->buttonMutex);
					// flipped
					printf("North Pedestrian Button: %d\n", data->NPStatus);
					printf("South Pedestrian Button: %d\n", data->SPStatus);
					printf("East Pedestrian Button: %d\n", data->EPStatus);
					printf("West Pedestrian Button: %d\n", data->WPStatus);

					switch(*trafficCurState){
						case EWGreen:
							timer_settime(timer_idPedestrianLight, 0, &itime, NULL);

							if(data->NPStatus == true && data->SPStatus == true){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NP_SPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else if(data->NPStatus == true && data->SPStatus == false){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else if(data->NPStatus == false && data->SPStatus == true){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = SPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else if(data->NPStatus == false && data->SPStatus == false){
								timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NSEWPRed;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							break;
						case NSGreen:
							//timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
							if(data->EPStatus == true && data->WPStatus == true){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = EP_WPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else if(data->EPStatus == true && data->WPStatus == false){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = EPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else if(data->EPStatus == false && data->WPStatus == true){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = WPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else if(data->EPStatus == false && data->WPStatus == false){
								timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NSEWPRed;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							break;
						case W_WRGreen:

							if(data->NPStatus == true){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else{
								timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NSEWPRed;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							pthread_mutex_unlock(&data->sensorMutex);
							break;
						case E_ERGreen:
							//timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
							if(data->SPStatus == true){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = SPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else{
								timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NSEWPRed;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							pthread_mutex_unlock(&data->sensorMutex);
							break;
						case N_NRGreen:
							if(data->EPStatus == true){
								timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = EPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else{
								timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NSEWPRed;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							pthread_mutex_unlock(&data->sensorMutex);
							break;
						case S_SRGreen:
							timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
							if(data->WPStatus == true){
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = WPGreen;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							else{
								timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
								pthread_mutex_lock(&data->pedStateMutex);
								*pedestrianCurState = NSEWPRed;
								data->TrafficState_lcd_ready = 1;
								pthread_cond_broadcast(&data->pedState_FSM_condvar);
								pthread_mutex_unlock(&data->pedStateMutex);
							}
							break;
						default:
							timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
							pthread_mutex_lock(&data->pedStateMutex);
							*pedestrianCurState = NSEWPRed;
							data->TrafficState_lcd_ready = 1;
							pthread_cond_broadcast(&data->pedState_FSM_condvar);
							pthread_mutex_unlock(&data->pedStateMutex);
							break;
				}
				pthread_mutex_unlock(&data->buttonMutex);
				data->TrafficState_ped_ready = 0;
				pthread_mutex_unlock(&data->trafficStateMutex);
				fflush(stdout);
				break;
			case NPGreen:
				// release the north
				pthread_mutex_lock(&data->buttonMutex);
				data->NPStatus = 0;
				pthread_mutex_unlock(&data->buttonMutex);

				timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NPFlash;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case NPFlash:
				timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NSEWPRed;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case SPGreen:
				// release the south
				pthread_mutex_lock(&data->buttonMutex);
				data->SPStatus = 0;
				pthread_mutex_unlock(&data->buttonMutex);

				timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = SPFlash;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case SPFlash:
				timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NSEWPRed;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case EPGreen:
				// release the east
				pthread_mutex_lock(&data->buttonMutex);
				data->EPStatus = 0;
				pthread_mutex_unlock(&data->buttonMutex);

				timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = EPFLash;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case EPFLash:
				timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NSEWPRed;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case WPGreen:
				// release the east
				pthread_mutex_lock(&data->buttonMutex);
				data->WPStatus = 0;
				pthread_mutex_unlock(&data->buttonMutex);

				timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = WPFlash;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case WPFlash:
				timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NSEWPRed;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case NP_SPGreen:
				// release the north and south
				pthread_mutex_lock(&data->buttonMutex);
				data->NPStatus = 0;
				data->SPStatus = 0;
				pthread_mutex_unlock(&data->buttonMutex);

				timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NP_SPFlash;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case NP_SPFlash:
				timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NSEWPRed;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case EP_WPGreen:
				pthread_mutex_lock(&data->buttonMutex);
				data->EPStatus = 0;
				data->WPStatus = 0;
				pthread_mutex_unlock(&data->buttonMutex);

				timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = EP_WPFlash;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			case EP_WPFlash:
				timer_settime(timer_idPedestrianLight, 0, &itime2, NULL);
				pthread_mutex_lock(&data->pedStateMutex);
				*pedestrianCurState = NSEWPRed;
				data->TrafficState_lcd_ready = 1;
				pthread_cond_broadcast(&data->pedState_FSM_condvar);
				pthread_mutex_unlock(&data->pedStateMutex);
				break;
			default:
				printf("State not handled");
				break;
		}
}

void PedStatePrintState(enum pedStates CurState){
	int localState = CurState;
	switch(CurState){
		case NSEWPRed:
			printf("In Pedestrian current state: NSEWPedestrianRed\n");
			break;
		case NPGreen:
			printf("In Pedestrian current state: NPGreen\n");
			break;
		case NPFlash:
			printf("In Pedestrian current state: NPFlash\n");
			break;
		case SPGreen:
			printf("In Pedestrian current state: SPGreen\n");
			break;
		case SPFlash:
			printf("In Pedestrian current state: SPFlash\n");
			break;
		case EPGreen:
			printf("In Pedestrian current state: EPGreen\n");
			break;
		case EPFLash:
			printf("In Pedestrian current state: EPFlash\n");
			break;
		case WPGreen:
			printf("In Pedestrian current state: WPGreen\n");
			break;
		case WPFlash:
			printf("In Pedestrian current state: WPFlash\n");
			break;
		case NP_SPGreen:
			printf("In Pedestrian current state: NP_SPGreen\n");
			break;
		case NP_SPFlash:
			printf("In Pedestrian current state: NP_SPFlash\n");
			break;
		case EP_WPGreen:
			printf("In Pedestrian current state: EP_WPGreen\n");
			break;
		case EP_WPFlash:
			printf("In Pedestrian current state: EP_WPFlash\n");
			break;
		default:
			printf("Pedestrian State Not Handled");
			break;
	}
}

void Pedestrian_Timer_Setter(enum pedStates CurState, timer_list_pedestrian timingRules){
	int localState = CurState;
	switch(CurState){
		case NSEWPRed:
			//printf("In Pedestrian current state: NSEWPedestrianRed\n");
            //timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeRedCond, NULL);
			break;
		case NPGreen:
			//printf("In Pedestrian current state: NPGreen\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeGreenPedestrian, NULL);
			break;
		case NPFlash:
			//printf("In Pedestrian current state: NPFlash\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeFlashPedestrian, NULL);
			break;
		case SPGreen:
			//printf("In Pedestrian current state: SPGreen\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeGreenPedestrian, NULL);
			break;
		case SPFlash:
			//printf("In Pedestrian current state: SPFlash\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeFlashPedestrian, NULL);
			break;
		case EPGreen:
			//printf("In Pedestrian current state: EPGreen\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeGreenPedestrian, NULL);
			break;
		case EPFLash:
			//printf("In Pedestrian current state: EPFlash\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeFlashPedestrian, NULL);
			break;
		case WPGreen:
			//printf("In Pedestrian current state: WPGreen\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeGreenPedestrian, NULL);
			break;
		case WPFlash:
			//printf("In Pedestrian current state: WPFlash\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeFlashPedestrian, NULL);
			break;
		case NP_SPGreen:
			//printf("In Pedestrian current state: NP_SPGreen\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeGreenPedestrian, NULL);
			break;
		case NP_SPFlash:
			//printf("In Pedestrian current state: NP_SPFlash\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeFlashPedestrian, NULL);
			break;
		case EP_WPGreen:
			//printf("In Pedestrian current state: EP_WPGreen\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeGreenPedestrian, NULL);
			break;
		case EP_WPFlash:
			//printf("In Pedestrian current state: EP_WPFlash\n");
            timer_settime(timer_idPedestrianLight, 0, &timingRules.itimeGreenPedestrian, NULL);
			break;
		default:
			printf("Pedestrian State Not Handled");
            //timer_settime(timer_idPedestrianLight, 0, &itime, NULL);
			break;
	}
}

void TrafficLightPrintStateFlipped(enum trafficLightStates CurState){
	int localState = CurState;
	switch (CurState){
		case NSEWRed_NorthSouth:
			printf("In current state: NSEWRed_NorthSouth\n");
			break;
		case NSEWRed_NorthSouth_Special_NoTrain:
			printf("In current state: NSEWRed_NorthSouth_Special_NoTrain\n");
			break;
		case NSEWRed_NorthSouth_Special_TrainCond:
			printf("In current state: NSEWRed_NorthSouth_Special_TrainCond\n");
			break;
		case NSEWRed_EastWest:
			printf("In current state: NSEWRed_EastWest\n");
			break;
		case NSEWRed_EastWest_Special_NoTrain:
			printf("In current state: NSEWRed_EastWest_Special_NoTrain\n");
			break;
		case NSGreen:
			printf("In current state: NSGreen\n");
			break;
		case NSYellow:
			printf("In current state: NSYellow\n");
			break;
		case EWGreen:
			printf("In current state: EWGreen\n");
			break;
		case EWYellow:
			printf("In current state: EWYellow\n");
			break;
		case NR_SRGreen:
			printf("In current state: NR_SRGreen\n");
			break;
		case NR_SRYellow:
			printf("In current state: NR_SRYellow\n");
			break;
		case ER_WRGreen:
			printf("In current state: ER_WRGreen\n");
			break;
		// flipped
		case N_NRGreen:
			//printf("In current state: N_NRGreen\n");
			printf("In current state: S_SRGreen\n");
			break;
		// flipped
		case NGreen_NRYellow:
			//printf("In current state: NGreen_NRYellow\n");
			printf("In current state: SGreen_SRYellow\n");
			break;
		// flipped
		case NGreen:
			//printf("In current state: NGreen\n");
			printf("In current state: SGreen\n");
			break;
		// flipped
		case S_SRGreen:
			//printf("In current state: S_SRGreen\n");
			printf("In current state: N_NRGreen\n");
			break;
		// flipped
		case SGreen_SRYellow:
			//printf("In current state: SGreen_SRYellow\n");
			printf("In current state: NGreen_NRYellow\n");
			break;
		// flipped
		case SYellow:
			//printf("In current state: SYellow\n");
			printf("In current state: NYellow\n");
			break;
		// flipped
		case SGreenTrain:
			//printf("In current state: SGreenTrain\n");
			printf("In current state: NGreenTrain\n");
			break;
		case SGreenNoTrain:
			//printf("In current state: SGreenNoTrain\n");
			printf("In current state: NGreenNoTrain\n");
			break;
		case E_ERGreen:
			//printf("In current state: E_ERGreen\n");
			printf("In current state: W_WRGreen\n");
			break;
		case EGreen_ERYellow:
			//printf("In current state: EGreen_ERYellow\n");
			printf("In current state: WGreen_WRYellow\n");
			break;
		case EGreenTrain:
			//printf("In current state: EGreenTrain\n");
			printf("In current state: WGreenTrain\n");
			break;
		case EGreenNoTrain:
			//printf("In current state: EGreenNoTrain\n");
			printf("In current state: WGreenNoTrain\n");
			break;
		case W_WRGreen:
			//printf("In current state: W_WRGreen\n");
			printf("In current state: E_ERGreen\n");
			break;
		case WGreen_WRYellow:
			//printf("In current state: WGreen_WRYellow\n");
			printf("In current state: EGreen_ERYellow\n");
			break;
		case WGreen:
			//printf("In current state: WGreen\n");
			printf("In current state: EGreen\n");
			break;
		case ER_WRYellow:
			printf("In current state: ER_WRYellow\n");
			break;
		default:
			printf("State Not Handled\n");
			break;
	}
}


// client code
void *client_thread(void *data){
	client_data *thread_data = (client_data*)data;

    packet_data msg;
    packet_reply_data reply;

    timer_t client_timer_id;
	struct itimerspec client_itime;

	int rcvid;
	timer_message time_msg;

	// initializer
	struct sigevent event;
	int chid;
	chid = ChannelCreate(0);
	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf(stderr, "%s:  couldn't ConnectAttach to self! \n", progname);
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);

	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &client_timer_id) == -1)
	{
	   printf (stderr, "%s:  couldn't create a timer, errno %d \n", progname, errno);
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// modification added here
	// 1 second delay interval timer
	client_itime.it_value.tv_sec = 1;	   // 1 second
	client_itime.it_value.tv_nsec = 0;    // 0 second
	client_itime.it_interval.tv_sec = 10;  // 1 second
	client_itime.it_interval.tv_nsec = 0; // 0 second

	msg.ClientID = 601 + thread_data->thread_num; // unique number for this client (optional)

	int server_coid;

	// We would have pre-defined data to stuff here
	msg.hdr.type = 0x00;
	msg.hdr.subtype = 0x00;
	int connection_status = 0;

    // Do whatever work you wanted with server connection
    while(1)
    {
    	if(connection_status){
			pthread_mutex_lock(&thread_data->resource_data->trafficStateMutex);
			 // test the condition and wait until it is true
			 while(!thread_data->resource_data->TrafficState_client_ready)
			 {
				 pthread_cond_wait(&thread_data->resource_data->trafficState_FSM_condvar, &thread_data->resource_data->trafficStateMutex);
				 printf("cons data_ready = %d\n",thread_data->resource_data->TrafficState_client_ready);
			 }
			 // process data
			 printf ("consumer:  got data from producer\n");
			 msg.data[0] = STATUS_UPDATE_ID; //Status Update Packet
			 msg.data[1] = ENTITY_TL2; //Rail Crossing Node
			 msg.data[2] = thread_data->resource_data->TrafficCurState;

			 // now change the condition and signal that it has changed
			 thread_data->resource_data->TrafficState_client_ready = 0;

			pthread_mutex_unlock(&thread_data->resource_data->trafficStateMutex);

			// the data we are sending is in msg.data
			printf("Client (ID:%d), sending data packet\n", msg.ClientID);
			fflush(stdout);

			if (MsgSend(server_coid, &msg, sizeof(msg), &reply, sizeof(reply)) == -1)
			{
				printf(" Error data NOT sent to server\n");
					// maybe we did not get a reply from the server
				printf("  ---> Trying to connect to server named: %s\n", thread_data->attach_point);
				while (!(server_coid = name_open(thread_data->attach_point, 0)) == -1)
				{
					printf("\n    ERROR, could not connect to server!\n\n");
				}
				printf("Reconnection established to: %s\n", thread_data->attach_point);
			}
			else
			{ // now process the reply
				printf("   -->Reply received'\n");
			}
    	}
    	else{
			printf("  ---> Client %d trying to connect to server named: %s\n",
					thread_data->thread_num, thread_data->attach_point);
			timer_settime(client_timer_id, 0, &client_itime, NULL);
			while(((server_coid = name_open(thread_data->attach_point, 0)) == -1))
			{
				printf("\nERROR, Client %d could not connect to server!\n",thread_data->thread_num);
				//return EXIT_FAILURE;
				rcvid = MsgReceive(chid, &time_msg, sizeof(time_msg), NULL);
				if (rcvid == 0){
					if(time_msg.pulse.code == MY_PULSE_CODE){
					}
				}
				fflush(stdout);
			}
			connection_status = 1;
			printf("Connection established to: %s\n", thread_data->attach_point);
		}
    }

    // Close the connection
    printf("\n Sending message to server to tell it to close the connection\n");
    name_close(server_coid);

    return EXIT_SUCCESS;
}

/*** Server code ***/
void *server_thread(void *data){
   name_attach_t *attach;

   thread_data *FSM_Resources = (thread_data*)data;

   if ((attach = name_attach(NULL, ATTACH_POINT, 0)) == NULL)
   {
       printf("\nFailed to name_attach on ATTACH_POINT: %s \n", ATTACH_POINT);
       printf("\n Possibly another server with the same name is already running !\n");
	   return EXIT_FAILURE;
   }

   printf("Server Listening for Clients on ATTACH_POINT: %s \n\n", ATTACH_POINT);

   	/*
	 *  Server Loop
	 */
   packet_data msg;
   int rcvid=0, msgnum=0;  		// no message received yet
   int Stay_alive=0, living=0;	// server stays running (ignores _PULSE_CODE_DISCONNECT request)

   packet_reply_data replymsg; // replymsg structure for sending back to client
   replymsg.hdr.type = 0x01;
   replymsg.hdr.subtype = 0x00;

   uint8_t packet_ID;

   while (1)
   {
	   // Do your MsgReceive's here now with the chid
       rcvid = MsgReceive(attach->chid, &msg, sizeof(msg), NULL);
       /*
        * Wait for message or pulse on channel
        * (channe id, where to store data, size of buffer, info)
        * blocks until message received
        */

       if (rcvid == -1)  // Error condition, exit
       {
           printf("\nFailed to MsgReceive\n");
           break;
       }

       if (rcvid == 0)  //  Pulse received, work out what type
       {
           switch (msg.hdr.code)
           {
			   case _PULSE_CODE_UNBLOCK:
				   printf("\nServer got _PULSE_CODE_UNBLOCK after %d, msgnum\n", msgnum);
				   break;

			   case _PULSE_CODE_COIDDEATH:  // from the kernel
				   printf("\nServer got _PULSE_CODE_COIDDEATH after %d, msgnum\n", msgnum);
				   break;

			   case _PULSE_CODE_THREADDEATH: // from the kernel
				   printf("\nServer got _PULSE_CODE_THREADDEATH after %d, msgnum\n", msgnum);
				   break;

			   default:
				   // Some other pulse sent by one of your processes or the kernel
				   printf("\nServer got some other pulse after %d\n", msg.hdr.code);
				   break;
           }
           continue;// go back to top of while loop
       }

       // for messages:
       if(rcvid > 0) // if true then A message was received
       {
		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (msg.hdr.type == _IO_CONNECT )
		   {
			   MsgReply( rcvid, EOK, NULL, 0 );
			   printf("\n gns service is running....");
			   continue;
		   }

		   // Some other I/O message was received; reject it
		   if (msg.hdr.type > _IO_BASE && msg.hdr.type <= _IO_MAX )
		   {
			   MsgError( rcvid, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   continue;	// go back to top of while loop
		   }

		   printf("Command ID: %d\n", msg.ClientID);
		   fflush(stdout);

		   packet_ID = msg.data[0];
		   printf("First Byte:");
		   printf("%d\n", msg.data[0]);
		   printf("Second Byte:");
		   printf("%d\n", msg.data[1]);

		   printf("\n");
		   printf("\n");
		   switch(packet_ID){
			   case COMMAND_FORCE_STATE:
				   printf("Force State Received\n");
				   int status = allowableForceStateSetter(msg.data[1], FSM_Resources);
				   if(status){
					   printf("Force State Success\n");
					   replymsg.data[0] = ACK_FORCE_STATE;
					   replymsg.data[1] = 1; // success
				   }
				   else{
					   printf("Force State Failed\n");
					   replymsg.data[0] = ACK_FORCE_STATE;
					   replymsg.data[1] = 0; // failure
				   }
				   break;
			   case COMMAND_FORCE_STATE_CANCEL:
				   printf("Command Force State Received\n");
				   replymsg.data[0] = ACK_FORCE_STATE_CANCEL;

				   pthread_mutex_lock(&FSM_Resources->forceModeMutex);
				   FSM_Resources->force_state_flag = 0;
				   FSM_Resources->force_state_target = -1;
				   pthread_mutex_unlock(&FSM_Resources->forceModeMutex);
				   // dont forget to call the timer again
				   timer_settime(timer_idTrafficLight, 0, &itime3,NULL);
				   break;
			   case COMMAND_FORCE_MODE:
				   printf("Command Force Mode Received\n");
				   replymsg.data[0] = ACK_FORCE_MODE;
				   replymsg.data[1] = 0; // failed
				   break;
			   case COMMAND_FORCE_MODE_CANCEL:
				   printf("Command Force Mode Cancel Received\n");
				   replymsg.data[0] = ACK_FORCE_MODE_CANCEL;
				   replymsg.data[1] = 0; // failed
				   break;
			   case COMMAND_GET_MODE_SCHED:
				   printf("Command Set Mode Received\n");
				   replymsg.data[0] = ACK_GET_MODE_SCHED;
				   replymsg.data[1] = FSM_Resources->mode_Status;
 				   break;
			   case COMMAND_SET_MODE_SCHED:
				   printf("Command Set Mode Received\n");
				   if(!(msg.data[1] > MODE_ID_NIGHT_TIME)){
					   switch(msg.data[1]){
					   	   case MODE_ID_DEFAULT:
							   printf("default mode received\n");
							   timer_settime(timer_idTrafficLight, 0, &itime3,NULL);
							   break;
						   case MODE_ID_PEAK_TIME:
							   printf("peak time mode received\n");
							   timer_settime(timer_idTrafficLight, 0, &itime3,NULL);
							   break;
						   case MODE_ID_NIGHT_TIME:
							   printf("night time mode received\n");
							break;
					   }
					   pthread_mutex_lock(&FSM_Resources->mode_Status_mutex);
					   FSM_Resources->mode_Status = msg.data[1];
					   pthread_mutex_unlock(&FSM_Resources->mode_Status_mutex);

					   replymsg.data[0] = ACK_SET_MODE_SCHED;
					   replymsg.data[1] = 1; // success
				   }
				   else{
					   replymsg.data[0] = ACK_SET_MODE_SCHED;
					   replymsg.data[1] = 0; // failed
				   }
				   break;
			   case STATUS_UPDATE_ID:
				   if(msg.data[1] == ENTITY_RW){
					   pthread_mutex_lock(&FSM_Resources->trainCrossingStateMutex);
					   FSM_Resources->TrainCrossingCurState = (enum Rail_Crossing_States) msg.data[2];
					   pthread_mutex_unlock(&FSM_Resources->trainCrossingStateMutex);
					   printf("get Data from the railway\n");
				   }
//				   else if (msg.data[1] == ENTITY_CC){
////					   pthread_mutex_lock(&FSM_Resources->trainCrossingStateMutex);
////					   FSM_Resources->TrainCrossingCurState = (enum Rail_Crossing_States) msg.data[2];
////					   pthread_mutex_unlock(&FSM_Resources->trainCrossingStateMutex);
////					   printf("get Data from the railway\n");
//				   }
				   break;
		   }
		   MsgReply(rcvid, EOK, &replymsg, sizeof(replymsg));
       }
       else
       {
		   printf("\nERROR: Server received something, but could not handle it correctly\n");
       }
   }

   // Remove the attach point name from the file system (i.e. /dev/name/local/<myname>)
   name_detach(attach, 0);

   return EXIT_SUCCESS;
}

int allowableForceStateSetter(int forceStateArgs, thread_data *FSM_Resources){
	int allowed = 0;
	switch(forceStateArgs){
		case NSEWRed_NorthSouth:
		case NSEWRed_EastWest:
		case NSGreen:
		case EWGreen:
		case NR_SRGreen:
		case ER_WRGreen:
		case N_NRGreen:
		case S_SRGreen:
		case E_ERGreen:
		case W_WRGreen:
			allowed = 1;
			pthread_mutex_lock(&FSM_Resources->forceModeMutex);
			FSM_Resources->force_state_flag = 1;
			FSM_Resources->force_state_target = forceStateArgs;
			pthread_mutex_unlock(&FSM_Resources->forceModeMutex);
			break;
		// not allowed force state
		case WGreen:
		case NGreen:
		case SGreenTrain:
		case SGreenNoTrain:
		case EGreenTrain:
		case EGreenNoTrain:
		case NSYellow:
		case EWYellow:
		case NR_SRYellow:
		case NGreen_NRYellow:
		case SYellow:
		case SGreen_SRYellow:
		case EGreen_ERYellow:
		case WGreen_WRYellow:
		case ER_WRYellow:
		case NSEWRed_NorthSouth_Special_NoTrain:
		case NSEWRed_NorthSouth_Special_TrainCond:
		case NSEWRed_EastWest_Special_NoTrain:
			allowed = 0;
			break;
		default:
			allowed = 0;
			break;
	}
	return allowed;
}


// hardware specific codes downstairs
#define AM335X_CONTROL_MODULE_BASE   (uint64_t) 0x44E10000
#define AM335X_CONTROL_MODULE_SIZE   (size_t)   0x00001448
#define AM335X_GPIO_SIZE             (uint64_t) 0x00001000
#define AM335X_GPIO1_BASE            (size_t)   0x4804C000

#define LED0          (1<<21)   // GPIO1_21
#define LED1          (1<<22)   // GPIO1_22
#define LED2          (1<<23)   // GPIO1_23
#define LED3          (1<<24)   // GPIO1_24

#define SD0 (1<<28)  // SD0 is connected to GPIO1_28
#define SCL (1<<16)  // SCL is connected to GPIO1_16


#define GPIO_OE        0x134
#define GPIO_DATAIN    0x138
#define GPIO_DATAOUT   0x13C

#define GPIO_IRQSTATUS_SET_1 0x38   // enable interrupt generation
#define GPIO_IRQWAKEN_1      0x48   // Wakeup Enable for Interrupt Line
#define GPIO_FALLINGDETECT   0x14C  // set falling edge trigger
#define GPIO_CLEARDATAOUT    0x190  // clear data out Register
#define GPIO_IRQSTATUS_1     0x30   // clear any prior IRQs

#define GPIO1_IRQ 99  // TRG page 465 list the IRQs for the am335x


#define P9_12_pinConfig 0x878 //  conf_gpmc_ben1 (TRM pp 1364) for GPIO1_28,  P9_12

// GPMC_A1_Configuration
#define PIN_MODE_0   0x00
#define PIN_MODE_1   0x01
#define PIN_MODE_2   0x02
#define PIN_MODE_3   0x03
#define PIN_MODE_4   0x04
#define PIN_MODE_5   0x05
#define PIN_MODE_6   0x06
#define PIN_MODE_7   0x07

// PIN MUX Configuration strut values  (page 1420 from TRM)
#define PU_ENABLE    0x00
#define PU_DISABLE   0x01
#define PU_PULL_UP   0x01
#define PU_PULL_DOWN 0x00
#define RECV_ENABLE  0x01
#define RECV_DISABLE 0x00
#define SLEW_FAST    0x00
#define SLEW_SLOW    0x01

typedef union _CONF_MODULE_PIN_STRUCT   // See TRM Page 1420
{
  unsigned int d32;
  struct {    // name: field size
           unsigned int conf_mmode : 3;       // LSB
           unsigned int conf_puden : 1;
           unsigned int conf_putypesel : 1;
           unsigned int conf_rxactive : 1;
           unsigned int conf_slewctrl : 1;
           unsigned int conf_res_1 : 13;      // reserved
           unsigned int conf_res_2 : 12;      // reserved MSB
         } b;
} _CONF_MODULE_PIN;

void strobe_SCL(uintptr_t gpio_port_add) {
   uint32_t PortData;
   PortData = in32(gpio_port_add + GPIO_DATAOUT);// value that is currently on the GPIO port
   PortData &= ~(SCL);
   out32(gpio_port_add + GPIO_DATAOUT, PortData);// Clock low
   delaySCL();

   PortData  = in32(gpio_port_add + GPIO_DATAOUT);// get port value
   PortData |= SCL;// Clock high
   out32(gpio_port_add + GPIO_DATAOUT, PortData);
   delaySCL();
}

// Thread used to Flash the 4 LEDs on the BeagleBone for 100ms
void *Flash_LED0_ex(void *notused)
{
	pthread_detach(pthread_self());  // no need for this thread to join
	uintptr_t gpio1_port = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	uintptr_t val;
	// Write GPIO data output register
	val  = in32(gpio1_port + GPIO_DATAOUT);
	val |= (LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	usleep(100000);  // 100 ms wait
	//sched_yield();  // if used without the usleep, this line will flash the LEDS for ~4ms

	val  = in32(gpio1_port + GPIO_DATAOUT);
	val &= ~(LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	munmap_device_io(gpio1_port, AM335X_GPIO_SIZE);
}

void delaySCL()  {// Small delay used to get timing correct for BBB
  volatile int i, a;
  for(i=0;i<0x1F;i++) // 0x1F results in a delay that sets F_SCL to ~480 kHz
  {   // i*1 is faster than i+1 (i+1 results in F_SCL ~454 kHz, whereas i*1 is the same as a=i)
     a = i;
  }
  // usleep(1);  //why doesn't this work? Ans: Results in a period of 4ms as
  // fastest time, which is 250Hz (This is to slow for the TTP229 chip as it
  // requires F_SCL to be between 1 kHz and 512 kHz)
}

uint32_t KeypadReadIObit(uintptr_t gpio_base, uint32_t BitsToRead)  {
   volatile uint32_t val = 0;
   val  = in32(gpio_base + GPIO_DATAIN);// value that is currently on the GPIO port

   val &= BitsToRead; // mask bit
   //val = val >> (BitsToRead % 2);
   //return val;
   if(val==BitsToRead)
	   return 1;
   else
	   return 0;
}

void DecodeKeyValue(uint32_t word, thread_data *sensor_Resources)
{
	switch(word)
	{
		case 0x01:
			printf("Key  1 pressed\n");
			printf("NR Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->NRSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x02:
			printf("Key  2 pressed\n");
			printf("SR Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->SRSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x04:
			printf("Key  3 pressed\n");
			printf("ER Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->ERSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x08:
			printf("Key  4 pressed\n");
			printf("WR Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->WRSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;

		case 0x10:
			printf("Key  5 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("NorthStraightSensor Sensor is triggered\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->NorthStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x20:
			printf("Key  6 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("SouthStraightSensor is pressed\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->SouthStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x40:
			printf("Key  7 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("EastStraightSensor is pressed\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->EastStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x80:
			printf("Key  8 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("WestStraightSensor is pressed\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->WestStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;

		case 0x100:
			printf("Key  9 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("North Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->NPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x200:
			printf("Key 10 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("South Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->SPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x400:
			printf("Key 11 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("East Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->EPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x800:
			printf("Key 12 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("West Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->WPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;

		// we have 4 left over butten for something else ?
		case 0x1000:
			printf("Key 13 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x2000:
			printf("Key 14 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x4000:
			printf("Key 15 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x8000:
			printf("Key 16 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x00:  // key release event (do nothing)
			break;
		default:
			printf("Key pressed could not be determined - %lu\n", word);
	}

	// do the decision here
	pthread_mutex_lock(&sensor_Resources->mode_Status_mutex);
	int localMode = sensor_Resources->mode_Status;
	pthread_mutex_unlock(&sensor_Resources->mode_Status_mutex);



	if(localMode == MODE_ID_NIGHT_TIME){
		pthread_mutex_lock(&sensor_Resources->sensorMutex);
			int NorthStraightSensor= sensor_Resources->NorthStraightSensor;
			int SouthStraightSensor= sensor_Resources->SouthStraightSensor;
			int EastStraightSensor= sensor_Resources->EastStraightSensor;
			int WestStraightSensor= sensor_Resources->WestStraightSensor;
			int NRSensor= sensor_Resources->NRSensor;
			int SRSensor= sensor_Resources->SRSensor;
			int ERSensor= sensor_Resources->ERSensor;
			int WRSensor= sensor_Resources->WRSensor;
		pthread_mutex_unlock(&sensor_Resources->sensorMutex);

		if(NorthStraightSensor || SouthStraightSensor|| EastStraightSensor || WestStraightSensor
				|| NRSensor || SRSensor || ERSensor || WRSensor ){
			timer_settime(timer_idTrafficLight, 0, &itime3,NULL);
		}
	}
}


void DecodeKeyValueFlipped(uint32_t word, thread_data *sensor_Resources)
{
	switch(word)
	{
		case 0x01:
			printf("Key  1 pressed\n");
			printf("NR Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			//sensor_Resources->NRSensor = 1;
			sensor_Resources->SRSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x02:
			printf("Key  2 pressed\n");
			printf("SR Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			//sensor_Resources->SRSensor = 1;
			sensor_Resources->NRSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x04:
			printf("Key  3 pressed\n");
			printf("ER Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			//sensor_Resources->ERSensor = 1;
			sensor_Resources->WRSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x08:
			printf("Key  4 pressed\n");
			printf("WR Sensor is is triggered\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			//sensor_Resources->WRSensor = 1;
			sensor_Resources->ERSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;

		case 0x10:
			printf("Key  5 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("NorthStraightSensor Sensor is triggered\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->NorthStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x20:
			printf("Key  6 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("SouthStraightSensor is pressed\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->SouthStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x40:
			printf("Key  7 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("EastStraightSensor is pressed\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->EastStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x80:
			printf("Key  8 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("WestStraightSensor is pressed\n");
			pthread_mutex_lock(&sensor_Resources->sensorMutex);
			sensor_Resources->WestStraightSensor = 1;
			pthread_mutex_unlock(&sensor_Resources->sensorMutex);
			usleep(1); // do this so we only fire once
			break;

		case 0x100:
			printf("Key  9 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("North Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->NPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x200:
			printf("Key 10 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("South Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->SPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x400:
			printf("Key 11 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("East Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->EPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;
		case 0x800:
			printf("Key 12 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			printf("West Pedestrian is pressed\n");
			pthread_mutex_lock(&sensor_Resources->buttonMutex);
			sensor_Resources->WPStatus = 1;
			pthread_mutex_unlock(&sensor_Resources->buttonMutex);
			usleep(1); // do this so we only fire once
			break;

		// we have 4 left over butten for something else ?
		case 0x1000:
			printf("Key 13 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x2000:
			printf("Key 14 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x4000:
			printf("Key 15 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x8000:
			printf("Key 16 pressed\n");
			pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
			usleep(1); // do this so we only fire once
			break;
		case 0x00:  // key release event (do nothing)
			break;
		default:
			printf("Key pressed could not be determined - %lu\n", word);
	}

	// do the decision here
	pthread_mutex_lock(&sensor_Resources->mode_Status_mutex);
	int localMode = sensor_Resources->mode_Status;
	pthread_mutex_unlock(&sensor_Resources->mode_Status_mutex);



	if(localMode == MODE_ID_NIGHT_TIME){
		pthread_mutex_lock(&sensor_Resources->sensorMutex);
			int NorthStraightSensor= sensor_Resources->NorthStraightSensor;
			int SouthStraightSensor= sensor_Resources->SouthStraightSensor;
			int EastStraightSensor= sensor_Resources->EastStraightSensor;
			int WestStraightSensor= sensor_Resources->WestStraightSensor;
			int NRSensor= sensor_Resources->NRSensor;
			int SRSensor= sensor_Resources->SRSensor;
			int ERSensor= sensor_Resources->ERSensor;
			int WRSensor= sensor_Resources->WRSensor;
		pthread_mutex_unlock(&sensor_Resources->sensorMutex);

		if(NorthStraightSensor || SouthStraightSensor|| EastStraightSensor || WestStraightSensor
				|| NRSensor || SRSensor || ERSensor || WRSensor ){
			timer_settime(timer_idTrafficLight, 0, &itime3,NULL);
		}
	}
}


/*
 *
 *
 * Here is the ISR
 *
 *
 *
 */
typedef struct
{
	int count_thread;
	uintptr_t gpio1_base;
	struct sigevent pevent; // remember to fill in "event" structure in main
}ISR_data;

// create global struct to share data between threads
ISR_data ISR_area_data;

const struct sigevent* Inthandler( void* area, int id )
{
	// 	"Do not call any functions in ISR that call kernerl - including printf()
	//struct sigevent *pevent = (struct sigevent *) area;
	ISR_data *p_ISR_data = (ISR_data *) area;

	InterruptMask(GPIO1_IRQ, id);  // Disable all hardware interrupt

	// must do this in the ISR  (else stack over flow and system will crash
	out32(p_ISR_data->gpio1_base + GPIO_IRQSTATUS_1, SD0); //clear IRQ

	// do this to tell us how many times this handler gets called
	p_ISR_data->count_thread++;
	// got IRQ.
	// work out what it came from

    InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

    // return a pointer to an event structure (preinitialized
    // by main) that contains SIGEV_INTR as its notification type.
    // This causes the InterruptWait in "int_thread" to unblock.
	return (&p_ISR_data->pevent);
}

uintptr_t gpio1_base;
uintptr_t control_module;
volatile uint32_t val = 0;

int setup(){
	  control_module = mmap_device_io(AM335X_CONTROL_MODULE_SIZE,
			  AM335X_CONTROL_MODULE_BASE);
	  gpio1_base = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	  // initalise the global stuct
	  ISR_area_data.count_thread = 0;
	  ISR_area_data.gpio1_base = gpio1_base;

	  memset(&ISR_area_data.pevent, 0, sizeof(ISR_area_data.pevent));
	  SIGEV_INTR_INIT (&ISR_area_data.pevent);
	  ISR_area_data.pevent.sigev_notify = SIGEV_INTR;  // Setup for external interrupt

		// we also need to have the PROCMGR_AID_INTERRUPT and PROCMGR_AID_IO abilities enabled. For more information, see procmgr_ability().
	  ThreadCtl( _NTO_TCTL_IO_PRIV , 1);// Request I/O privileges  for QNX7;

	  procmgr_ability( 0, PROCMGR_AID_INTERRUPT | PROCMGR_AID_IO);

	  if( (control_module)&&(gpio1_base) )
	  {
		// set DDR for LEDs to output and GPIO_28 to input
		val = in32(gpio1_base + GPIO_OE); // read in current setup for GPIO1 port
		val |= 1<<28;                     // set IO_BIT_28 high (1=input, 0=output)
		out32(gpio1_base + GPIO_OE, val); // write value to input enable for data pins
		val &= ~(LED0|LED1|LED2|LED3);    // write value to output enable
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for LED pins

		val = in32(gpio1_base + GPIO_OE);
		val &= ~SCL;                      // 0 for output
		out32(gpio1_base + GPIO_OE, val); // write value to output enable for data pins

		val = in32(gpio1_base + GPIO_DATAOUT);
		val |= SCL;              // Set Clock Line High as per TTP229-BSF datasheet
		out32(gpio1_base + GPIO_DATAOUT, val); // for 16-Key active-Low timing diagram

		in32s(&val, 1, control_module + P9_12_pinConfig );
		printf("Original pinmux configuration for GPIO1_28 = %#010x\n", val);

		// set up pin mux for the pins we are going to use  (see page 1354 of TRM)
		volatile _CONF_MODULE_PIN pinConfigGPMC; // Pin configuration strut
		pinConfigGPMC.d32 = 0;
		// Pin MUX register default setup for input (GPIO input, disable pull up/down - Mode 7)
		pinConfigGPMC.b.conf_slewctrl = SLEW_SLOW;    // Select between faster or slower slew rate
		pinConfigGPMC.b.conf_rxactive = RECV_ENABLE;  // Input enable value for the PAD
		pinConfigGPMC.b.conf_putypesel= PU_PULL_UP;   // Pad pullup/pulldown type selection
		pinConfigGPMC.b.conf_puden = PU_ENABLE;       // Pad pullup/pulldown enable
		pinConfigGPMC.b.conf_mmode = PIN_MODE_7;      // Pad functional signal mux select 0 - 7

		// Write to PinMux registers for the GPIO1_28
		out32(control_module + P9_12_pinConfig, pinConfigGPMC.d32);
		in32s(&val, 1, control_module + P9_12_pinConfig);   // Read it back
		printf("New configuration register for GPIO1_28 = %#010x\n", val);

		// Setup IRQ for SD0 pin ( see TRM page 4871 for register list)
		out32(gpio1_base + GPIO_IRQSTATUS_SET_1, SD0);	// Write 1 to GPIO_IRQSTATUS_SET_1
		out32(gpio1_base + GPIO_IRQWAKEN_1, SD0);    	// Write 1 to GPIO_IRQWAKEN_1
		out32(gpio1_base + GPIO_FALLINGDETECT, SD0);    // set falling edge
		out32(gpio1_base + GPIO_CLEARDATAOUT, SD0);     // clear GPIO_CLEARDATAOUT
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);      // clear any prior IRQs
		// all good to go periperhals can be set
		return EXIT_SUCCESS;
	}
	  else{
		  // periperhals cannot be initialized
		return EXIT_FAILURE;
	}

}

void *interruptKeyPadThread(void *data){
	thread_data *FSM_Resources = (thread_data*)data;

	// Main code starts here
	//The thread that calls InterruptWait() must be the one that called InterruptAttach().
	//    id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK | _NTO_INTR_FLAGS_NO_UNMASK | _NTO_INTR_FLAGS_END);
	 int id = InterruptAttach (GPIO1_IRQ, Inthandler, &ISR_area_data, sizeof(ISR_area_data), _NTO_INTR_FLAGS_TRK_MSK );

	InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

	int i = 0;
	printf( "Main entering loop and will call InterruptWait\n");
	for(;;)
	{
		// Block main until we get a sigevent of type: 	ISR_area_data.pevent
		InterruptWait( 0, NULL );   // block this thread until an interrupt occurs  (Wait for a hardware interrupt)
		InterruptDisable();
		// printf("do interrupt work here...\n");

		volatile uint32_t word = 0;
		//  confirm that SD0 is still low (that is a valid Key press event has occurred)
		val = KeypadReadIObit(gpio1_base, SD0);  // read SD0 (means data is ready)

		if(val == 0)  // start reading key value form the keypad
		{
			 word = 0;  // clear word variable

			 delaySCL(); // wait a short period of time before reading the data Tw  (10 us)

			 for(i=0;i<16;i++)           // get data from SD0 (16 bits)
			 {
				strobe_SCL(gpio1_base);  // strobe the SCL line so we can read in data bit

				val = KeypadReadIObit(gpio1_base, SD0); // read in data bit
				val = ~val & 0x01;                      // invert bit and mask out everything but the LSB

				word = word | (val<<i);  // add data bit to word in unique position (build word up bit by bit)
			 }
			 //printf("word=%u\n",word);
			 DecodeKeyValueFlipped(word, FSM_Resources);
			 //printf("Interrupt count = %i\n", ISR_area_data.count_thread);
		}
		//sched_yield();
		out32(gpio1_base + GPIO_IRQSTATUS_1, SD0);
		InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt
		InterruptEnable();
	}
	 munmap_device_io(control_module, AM335X_CONTROL_MODULE_SIZE);
}

// lcd specific codes



typedef union
{
	struct _pulse   pulse;
    // your other message structures would go here too
} my_message_t;

#define MY_PULSE_CODE   _PULSE_CODE_MINAVAIL


// Writes to I2C
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData)
{
	i2c_send_t hdr;
    iov_t sv[2];
    int status, i;

    uint8_t LCDpacket[21] = {};  // limited to 21 characters  (1 control bit + 20 bytes)

    // set the mode for the write (control or data)
    LCDpacket[0] = mode;  // set the mode (data or control)

	// copy data to send to send buffer (after the mode bit)
	for (i=0;i<NbData+1;i++)
		LCDpacket[i+1] = *pBuffer++;

    hdr.slave.addr = Address;
    hdr.slave.fmt = I2C_ADDRFMT_7BIT;
    hdr.len = NbData + 1;  // 1 extra for control (mode) bit
    hdr.stop = 1;

    SETIOV(&sv[0], &hdr, sizeof(hdr));
    SETIOV(&sv[1], &LCDpacket[0], NbData + 1); // 1 extra for control (mode) bit
      // int devctlv(int filedes, int dcmd,     int sparts, int rparts, const iov_t *sv, const iov_t *rv, int *dev_info_ptr);
    status = devctlv(fd, DCMD_I2C_SEND, 2, 0, sv, NULL, NULL);

    if (status != EOK){
    	printf("status = %s\n", strerror ( status ));
		return EXIT_FAILURE;
	}
	else{
		return status;
	}
}

int Initialise_I2C_Resource(){
	int file;
	int error;
	volatile uint8_t LCDi2cAdd = 0x3C;
	_Uint32t speed = 10000; // nice and slow (will work with 200000)
	LCD_connect td;

	// Create the mutex
	pthread_mutex_init(&td.mutex,NULL);		// pass NULL as the attr parameter to use the default attributes for the mutex

	// Open I2C resource and set it up
	if ((file = open("/dev/i2c1",O_RDWR)) < 0)	  // OPEN I2C1
		printf("Error while opening Device File.!!\n");
	else
		printf("I2C1 Opened Successfully\n");

	error = devctl(file,DCMD_I2C_SET_BUS_SPEED,&(speed),sizeof(speed),NULL);  // Set Bus speed
	if (error)
	{
		fprintf(stderr, "Error setting the bus speed: %d\n",strerror ( error ));
		return(EXIT_FAILURE);
	}
	else{
		printf("Bus speed set = %d\n", speed);
		Initialise_LCD(file, LCDi2cAdd);
		return(file);
	}
}

void Initialise_LCD (int fd, _Uint32t LCDi2cAdd)
{
	uint8_t	LCDcontrol = 0x00;

	//   Initialise the LCD display via the I2C bus
	LCDcontrol = 0x38;  // data byte for FUNC_SET_TBL1
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x39; // data byte for FUNC_SET_TBL2
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x14; // data byte for Internal OSC frequency
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x79; // data byte for contrast setting
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x50; // data byte for Power/ICON control Contrast set
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x6C; // data byte for Follower control
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x0C; // data byte for Display ON
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x01; // data byte for Clear display
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}

void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column)
{
	uint8_t position = 0x80; // SET_DDRAM_CMD (control bit)
	uint8_t rowValue = 0;
	uint8_t	LCDcontrol = 0;
	if (row == 1)
		rowValue = 0x40;     // memory location offset for row 1
	position = (uint8_t)(position + rowValue + column);
	//printf("Position: %#01x\n", position);
	LCDcontrol = position;
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}

int synchronized = 1;

void *LCDthread_ex (void *data)
{
	pthread_detach(pthread_self());
	Hybrid_LCD_FSM_Resource *thread_data = (Hybrid_LCD_FSM_Resource*) data;
	uint8_t	LCDdata[21] = {};
	while(1){
		// wait for message/pulse
		pthread_mutex_lock(&thread_data->resource_data->trafficStateMutex);
		 // test the condition and wait until it is true
		 while(!thread_data->resource_data->TrafficState_lcd_ready)
		 {
			 pthread_cond_wait(&thread_data->resource_data->trafficState_FSM_condvar, &thread_data->resource_data->trafficStateMutex);
			 printf("traffic light lcd data_ready = %d\n",thread_data->resource_data->TrafficState_client_ready);
		 }

		if(synchronized) pthread_mutex_lock(&thread_data->lcd_resource->mutex);     //lock the function to make sure the variables are protected
		   HardWareTrafficLightPrintState(thread_data);
			// write new data to I2C
		if(synchronized) pthread_mutex_unlock(&thread_data->lcd_resource->mutex);	//unlock the functions to release the variables for use by other functions
			// else other pulses ...

			thread_data->resource_data->TrafficState_lcd_ready = 0;

		   pthread_mutex_unlock(&thread_data->resource_data->trafficStateMutex);
		   fflush(stdout);
	}
}

#define EastConst  'E'
#define WestConst  'W'
#define NortConst  'N'
#define SouthConst  'S'
#define equalConst '='

#define redConst  'R'
#define greenConst  'G'
#define yellowConst  'Y'
#define flashConst  'F'

void *LCDThread_Pedestrian(void *data){
	pthread_detach(pthread_self());  // no need for this thread to join
	Hybrid_LCD_FSM_Resource *thread_data = (Hybrid_LCD_FSM_Resource*) data;

	pthread_mutex_lock(&thread_data->resource_data->pedStateMutex);
	enum pedStates localState = thread_data->resource_data->PedsCurState;
	pthread_mutex_unlock(&thread_data->resource_data->pedStateMutex);

	HardWarePedestrianPrintState(thread_data, localState);
	fflush(stdout);
}

void *LCDThread_TrafficLight(void *data){
	pthread_detach(pthread_self());  // no need for this thread to join
	Hybrid_LCD_FSM_Resource *thread_data = (Hybrid_LCD_FSM_Resource*) data;

	pthread_mutex_lock(&thread_data->resource_data->trafficStateMutex);
	enum trafficLightStates localState = thread_data->resource_data->TrafficCurState;
	pthread_mutex_unlock(&thread_data->resource_data->trafficStateMutex);

	HardWareTrafficLightPrintStateFlipped(thread_data, localState);
	fflush(stdout);
}



// index 0 19....
#define eastLetterPos 0
#define westLetterPos 10

#define eastNorthEqualPos 1
#define westSouthEqualPos 11

#define northLetterPos 0
#define southLetterPos 10

#define NorthSouthRow 0
#define NorthStraightPos 2
#define NorthRightPos 3//(NorthStraightPos + 1)
#define SouthStraightPos  12
#define SouthRightPos 13//(SouthStraightPos + 1)

#define EastWestRow 1
#define EastStraightPos 2
#define EastRightPos 3 //(EastStraightPos + 1)
#define WestStraightPos 12
#define WestRightPos 13 //(WestStraightPos + 1)

#define NorthPedPos 4 //(NorthStraightPos + 2)
#define SouthPedPos 14 //(SouthStraightPos + 2)

#define EastPedPos 4 //(EastStraightPos + 2)
#define WestPedPos 14 //(WestStraightPos + 2)

void *LCDThread_TrainStatus(void *data){
	pthread_detach(pthread_self());  // no need for this thread to join
	Hybrid_LCD_FSM_Resource *thread_data = (Hybrid_LCD_FSM_Resource*) data;

	pthread_mutex_lock(&thread_data->resource_data->trainCrossingStateMutex);
	enum trafficLightStates localState = thread_data->resource_data->TrainCrossingCurState;
	pthread_mutex_unlock(&thread_data->resource_data->trainCrossingStateMutex);

	if(localState == Train_Present){
		SingleStatusUpdate(NorthSouthRow, 19, thread_data, 'T');
	}else{
		SingleStatusUpdate(NorthSouthRow, 19, thread_data, 'A');
	}
	fflush(stdout);
}

void *InitializeLCD_TrafficLight(void *data){
	pthread_detach(pthread_self());  // no need for this thread to join
	Hybrid_LCD_FSM_Resource *thread_data = (Hybrid_LCD_FSM_Resource*) data;

	SingleStatusUpdate(NorthSouthRow, northLetterPos, thread_data, NortConst);
	SingleStatusUpdate(NorthSouthRow, eastNorthEqualPos, thread_data, equalConst);
	SingleStatusUpdate(NorthSouthRow, southLetterPos, thread_data, SouthConst);
	SingleStatusUpdate(NorthSouthRow, westSouthEqualPos, thread_data, equalConst);
	SingleStatusUpdate(EastWestRow, eastLetterPos, thread_data, EastConst);
	SingleStatusUpdate(EastWestRow, eastNorthEqualPos, thread_data, equalConst);
	SingleStatusUpdate(EastWestRow, westLetterPos, thread_data, WestConst);
	SingleStatusUpdate(EastWestRow, westSouthEqualPos, thread_data, equalConst);

	SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, redConst);
	SingleStatusUpdate(NorthSouthRow, NorthStraightPos , thread_data, redConst);
	SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, redConst);
	SingleStatusUpdate(EastWestRow, WestStraightPos , thread_data, redConst);

	SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);
	SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

	SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);
	SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

	SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, redConst);
	SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, redConst);

	fflush(stdout);
}

void SingleStatusUpdate(int CursorFirstPos, int CursorSecondPos, Hybrid_LCD_FSM_Resource *thread_data, char input){
	uint8_t	LCDdata[1] = {};

	pthread_mutex_lock(&thread_data->lcd_resource->mutex);     //lock the function to make sure the variables are protected
	thread_data->lcd_resource->CursorFirstPos = CursorFirstPos;
	thread_data->lcd_resource->CursorSecondPos = CursorSecondPos;

	SetCursor(thread_data->lcd_resource->fd, thread_data->lcd_resource->Address,
			thread_data->lcd_resource->CursorFirstPos,
			thread_data->lcd_resource->CursorSecondPos); // set cursor on LCD to first position first line

	   sprintf(LCDdata,"%c",input);

	   I2cWrite_(thread_data->lcd_resource->fd,
			   thread_data->lcd_resource->Address, DATA_SEND, &LCDdata[0], sizeof(LCDdata));

	pthread_mutex_unlock(&thread_data->lcd_resource->mutex);	//unlock the functions to release the variables for use by other functions

}

void HardWareTrafficLightPrintState(Hybrid_LCD_FSM_Resource *thread_data, enum trafficLightStates CurState){
	switch (CurState){
		case NSEWRed_NorthSouth:
		case NSEWRed_NorthSouth_Special_NoTrain:
		case NSEWRed_NorthSouth_Special_TrainCond:
		case NSEWRed_EastWest:
		case NSEWRed_EastWest_Special_NoTrain:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos , thread_data, redConst);
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, redConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos , thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos , thread_data, redConst);
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, redConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos , thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, redConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, redConst);
			break;
		case NSGreen:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos , thread_data, greenConst);
			break;
		case NSYellow:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, yellowConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, yellowConst);
			break;
		case EWGreen:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
			break;
		case EWYellow:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, yellowConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, yellowConst);
			break;
		case NR_SRGreen:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, greenConst);
			break;
		case NR_SRYellow:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, yellowConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, yellowConst);
			break;
		case ER_WRGreen:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, greenConst);
			break;
		case ER_WRYellow:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, yellowConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, yellowConst);
			break;
		case N_NRGreen:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, greenConst);
			break;
		case NGreen_NRYellow:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, yellowConst);
			break;
		case NGreen:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);
			break;
		case S_SRGreen:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, greenConst);
			break;
		case SGreen_SRYellow:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, yellowConst);
			break;
		case SYellow:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, yellowConst);
			break;
		case SGreenNoTrain:
		case SGreenTrain:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);
			break;
		case E_ERGreen:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, greenConst);
			break;
		case EGreen_ERYellow:
			printf("\n");
			SingleStatusUpdate(EastWestRow, NorthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, NorthRightPos, thread_data, yellowConst);
			break;
		case EGreenNoTrain:
		case EGreenTrain:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, redConst);
			break;
		case W_WRGreen:
			printf("\n");
			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, greenConst);
			break;
		case WGreen_WRYellow:
			printf("\n");
			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, yellowConst);
			break;
		case WGreen:
			printf("\n");
			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, redConst);
			break;
		default:
			printf("State Not Handled\n");
			break;
	}
}

void HardWareTrafficLightPrintStateFlipped(Hybrid_LCD_FSM_Resource *thread_data, enum trafficLightStates CurState){
	switch (CurState){
		case NSEWRed_NorthSouth:
		case NSEWRed_NorthSouth_Special_NoTrain:
		case NSEWRed_NorthSouth_Special_TrainCond:
		case NSEWRed_EastWest:
		case NSEWRed_EastWest_Special_NoTrain:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos , thread_data, redConst);
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, redConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos , thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos , thread_data, redConst);
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, redConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos , thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, redConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, redConst);
			break;
		case NSGreen:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos , thread_data, greenConst);
			break;
		case NSYellow:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, yellowConst);
			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, yellowConst);
			break;
		case EWGreen:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
			break;
		case EWYellow:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, yellowConst);
			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, yellowConst);
			break;
		case NR_SRGreen:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, greenConst);
			break;
		case NR_SRYellow:
			printf("\n");
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, yellowConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, yellowConst);
			break;
		case ER_WRGreen:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, greenConst);
			break;
		case ER_WRYellow:
			printf("\n");
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, yellowConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, yellowConst);
			break;
		case N_NRGreen:
			printf("\n");
//			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, greenConst);

			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, greenConst);

			break;
		case NGreen_NRYellow:
			printf("\n");
//			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, yellowConst);

			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, yellowConst);
			break;
		case NGreen:
			printf("\n");
//			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

			break;
		case S_SRGreen:
			printf("\n");
//			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, greenConst);

			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, greenConst);

			break;
		case SGreen_SRYellow:
			printf("\n");
//			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, yellowConst);

			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, yellowConst);

			break;
		case SYellow:
			printf("\n");
//			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, yellowConst);

			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, yellowConst);

			break;
		case SGreenNoTrain:
		case SGreenTrain:
			printf("\n");
//			SingleStatusUpdate(NorthSouthRow, SouthStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(NorthSouthRow, SouthRightPos, thread_data, redConst);

			SingleStatusUpdate(NorthSouthRow, NorthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(NorthSouthRow, NorthRightPos, thread_data, redConst);

			break;
		case E_ERGreen:
			printf("\n");
//			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, greenConst);

			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, greenConst);

			break;
		case EGreen_ERYellow:
			printf("\n");
//			SingleStatusUpdate(EastWestRow, NorthStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(EastWestRow, NorthRightPos, thread_data, yellowConst);

			SingleStatusUpdate(EastWestRow, SouthStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, SouthRightPos, thread_data, yellowConst);

			break;
		case EGreenNoTrain:
		case EGreenTrain:
			printf("\n");
			//SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			//SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, redConst);

			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, redConst);

			break;
		case W_WRGreen:
			printf("\n");
//			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, greenConst);

			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, greenConst);

			break;
		case WGreen_WRYellow:
			printf("\n");
//			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, yellowConst);

			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, yellowConst);

			break;
		case WGreen:
			printf("\n");
//			SingleStatusUpdate(EastWestRow, WestStraightPos, thread_data, greenConst);
//			SingleStatusUpdate(EastWestRow, WestRightPos, thread_data, redConst);

			SingleStatusUpdate(EastWestRow, EastStraightPos, thread_data, greenConst);
			SingleStatusUpdate(EastWestRow, EastRightPos, thread_data, redConst);

			break;
		default:
			printf("State Not Handled\n");
			break;
	}
}


void HardWarePedestrianPrintState(Hybrid_LCD_FSM_Resource *thread_data, enum pedStates CurState){
	switch (CurState){
			case NSEWPRed:
				printf("\n");
				SingleStatusUpdate(NorthSouthRow, NorthPedPos, thread_data, redConst);
				SingleStatusUpdate(NorthSouthRow, SouthPedPos, thread_data, redConst);
				SingleStatusUpdate(EastWestRow, EastPedPos, thread_data, redConst);
				SingleStatusUpdate(EastWestRow, WestPedPos, thread_data, redConst);
				break;
			case NPGreen:
				printf("\n");
				SingleStatusUpdate(NorthSouthRow, NorthPedPos, thread_data, greenConst);
				break;
			case NPFlash:
				printf("\n");
				SingleStatusUpdate(NorthSouthRow, NorthPedPos, thread_data, flashConst);
				break;
			case SPGreen:
				printf("\n");
				SingleStatusUpdate(NorthSouthRow, SouthPedPos, thread_data, greenConst);
				break;
			case SPFlash:
				printf("\n");
				SingleStatusUpdate(NorthSouthRow, SouthPedPos, thread_data, flashConst);
				break;
			case EPGreen:
				printf("\n");
				SingleStatusUpdate(EastWestRow, EastPedPos, thread_data, greenConst);
				break;
			case EPFLash:
				printf("\n");
				SingleStatusUpdate(EastWestRow, EastPedPos, thread_data, flashConst);
				break;
			case WPGreen:
				printf("\n");
				SingleStatusUpdate(EastWestRow, WestPedPos, thread_data, greenConst);
				break;
			case WPFlash:
				printf("\n");
				SingleStatusUpdate(EastWestRow, WestPedPos, thread_data, flashConst);
				break;
			case NP_SPGreen:
				printf("\n");
				SingleStatusUpdate(NorthSouthRow, NorthPedPos, thread_data, greenConst);
				SingleStatusUpdate(NorthSouthRow, SouthPedPos, thread_data, greenConst);
				break;
			case NP_SPFlash:
				printf("\n");
				SingleStatusUpdate(NorthSouthRow, NorthPedPos, thread_data, flashConst);
				SingleStatusUpdate(NorthSouthRow, SouthPedPos, thread_data, flashConst);
				break;
			case EP_WPGreen:
				printf("\n");
				SingleStatusUpdate(EastWestRow, EastPedPos, thread_data, greenConst);
				SingleStatusUpdate(EastWestRow, WestPedPos, thread_data, greenConst);
				break;
			case EP_WPFlash:
				printf("\n");
				SingleStatusUpdate(EastWestRow, EastPedPos, thread_data, flashConst);
				SingleStatusUpdate(EastWestRow, WestPedPos, thread_data, flashConst);
				break;
			default:
				printf("Pedestrian State Not Handled");
				break;
	}
}






