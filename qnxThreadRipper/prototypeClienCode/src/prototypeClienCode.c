#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <share.h>
#include <time.h>
#include <errno.h>

#define LOCAL_ATTACH_POINT "TrafficNode"			  			     // change myname to the same name used for the server code.
#define QNET_ATTACH_POINT  "/net/VM_x86_Target02/dev/name/local/TrafficNode"  // hostname using full path, change myname to the name used for server

#define CENTRAL_ATTACH_POINT "CentralController"

#define MY_PULSE_CODE  _PULSE_CODE_MINAVAIL //Pulse for timer

#define BUF_SIZE 100

enum trafficLightStates {
	NSEWRed_NorthSouth,
	NSEWRed_NorthSouth_Special_NoTrain,
	NSEWRed_NorthSouth_Special_TrainCond,
	NSEWRed_EastWest,
	NSEWRed_EastWest_Special_NoTrain,
	//NSEWRed_EastWest_Special_TrainCond,
	NSGreen,
	NSYellow,
	EWGreen,
	EWYellow,
	NR_SRGreen,
	NR_SRYellow,
	ER_WRGreen,
	ER_WRYellow,
	N_NRGreen,
	NGreen_NRYellow,
	NGreen,
	S_SRGreen,
	SGreen_SRYellow,
	SYellow,
	SGreen,
	E_ERGreen,
	EGreen_ERYellow,
	EGreen,
	W_WRGreen,
	WGreen_WRYellow,
	WGreen
};

enum pedStates {
	NSEWPRed,
	NPGreen,
	NPFlash,
	SPGreen,
	SPFlash,
	EPGreen,
	EPFLash,
	WPGreen,
	WPFlash,
	NP_SPGreen,
	NP_SPFlash,
	EP_WPGreen,
	EP_WPFlash
};

enum controllerFSM { //Reference report for code flow
	menu_Init,
	main_Menu,
	status_Display,
	conn_Status,
	func_Status,
	control_Menu,
	control_Menu2,
	get_Sched_or_Mode,
	set_Sched_or_Mode,
	Force_or_Canc_State,
	Force_or_Canc_Mode
};

typedef struct {
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
	int commandId;
	int packetData;     // our data
	enum trafficLightStates trafficLightState; //traffic light states
} packet_data; //packet sent to server

typedef struct {
	struct _pulse hdr; // Our real data comes after this header
	char buf[BUF_SIZE]; // Message we send back to clients to tell them the messages was processed correctly.
	int reply;

	enum trafficLightStates trafficLightState;
	enum pedStates pedastrianStates;
} packet_reply_data; //reply packet expected when receiving from server

typedef union {
	struct _pulse pulse;
} timer_data; //used for getting pulse for timer

typedef struct {
	pthread_mutex_t button_Mutex; // add a mutex object into the structure we want to protect
	pthread_mutex_t command_Mutex; // add a mutex object into the structure we want to protect
	pthread_mutex_t update_Mutex; // add a mutex object into the structure we want to protect

	pthread_cond_t button_CondVar; // needs to be set to PTHREAD_COND_INITIALIZER;
	pthread_cond_t command_CondVar; // needs to be set to PTHREAD_COND_INITIALIZER;
	pthread_cond_t update_CondVar; // needs to be set to PTHREAD_COND_INITIALIZER;

	int channel_ID; //ID of established channel for something
	int buttonPressed; //conditional variable used for checking if button has been pressed
	int buttonData; //button data shared between button press thread and FSM
	int newCommand;// used for conditional variable sending commands to server
	int newUpdate; //used for conditional variable for gettign updates from server
	int serverUpdate;


	packet_data nodeParameter;
	packet_reply_data update_Node; //update status from railway node
} thread_data;


void PedStatePrintState(enum pedStates *CurState);

void TrafficLightPrintState(enum trafficLightStates *CurState);

void button_button_CondVar(thread_data * condition_Data, int * buttonRead);

void *button_thread(void *data);

void *menuFSM_thread(void*data);

void *clientThread_Send1(void *data);

void *serverThread(void *data);

int main(int argc, char *argv[]) {
	pthread_t serverpthread, clientSend1, stateMachine_pthread, button_pthread;
	thread_data resource_data;

	resource_data.update_Node.trafficLightState = NSGreen;
	void *pthread_end;

	resource_data.button_Mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	resource_data.update_Mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	resource_data.command_Mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	resource_data.button_CondVar = (pthread_cond_t) PTHREAD_COND_INITIALIZER;
	resource_data.update_CondVar = (pthread_cond_t) PTHREAD_COND_INITIALIZER;
	resource_data.command_CondVar = (pthread_cond_t) PTHREAD_COND_INITIALIZER;
	resource_data.buttonPressed = 0;
	resource_data.newCommand = 0;
	resource_data.newUpdate = 1;

	pthread_create(&serverpthread, NULL, serverThread, &resource_data);
//	pthread_create(&clientSend1, NULL, clientThread_Send1, &resource_data);
//	pthread_create(&stateMachine_pthread,NULL,menuFSM_thread,&resource_data);
//	pthread_create(&button_pthread,NULL,button_thread,&resource_data);

	pthread_join(serverpthread, &pthread_end);
//	pthread_join(clientSend1, &pthread_end);
//	pthread_join(stateMachine_pthread,&pthread_end);
//	pthread_join(button_pthread,&pthread_end);

	printf("\nMain Thread Terminating....");
	return EXIT_SUCCESS;
}


void PedStatePrintState(enum pedStates *CurState){
	int localState = *CurState;
	switch(*CurState){
		case NSEWPRed:
			printf("In Pedestrian current state: NSEWPedestrianRed\n");
			break;
		case NPGreen:
			printf("In Pedestrian current state: NPGreen\n");
			break;
		case NPFlash:
			printf("In Pedestrian current state: NPFlash\n");
			break;
		case SPGreen:
			printf("In Pedestrian current state: SPGreen\n");
			break;
		case SPFlash:
			printf("In Pedestrian current state: SPFlash\n");
			break;
		case EPGreen:
			printf("In Pedestrian current state: EPGreen\n");
			break;
		case EPFLash:
			printf("In Pedestrian current state: EPFlash\n");
			break;
		case WPGreen:
			printf("In Pedestrian current state: WPGreen\n");
			break;
		case WPFlash:
			printf("In Pedestrian current state: WPFlash\n");
			break;
		case NP_SPGreen:
			printf("In Pedestrian current state: NP_SPGreen\n");
			break;
		case NP_SPFlash:
			printf("In Pedestrian current state: NP_SPFlash\n");
			break;
		case EP_WPGreen:
			printf("In Pedestrian current state: EP_WPGreen\n");
			break;
		case EP_WPFlash:
			printf("In Pedestrian current state: EP_WPFlash\n");
			break;
		default:
			printf("Pedestrian State Not Handled, value was: %d", localState);
			break;
	}
}


void TrafficLightPrintState(enum trafficLightStates *CurState){
	//printf("In Do Something 1\n");
	int localState = *CurState;
	//printf("In raw state: %d\n", localState);
	switch (*CurState){
		case NSEWRed_NorthSouth:
			printf("In current state: NSEWRed_NorthSouth\n");
			break;
		case NSEWRed_NorthSouth_Special_NoTrain:
			printf("In current state: NSEWRed_NorthSouth_Special_NoTrain\n");
			break;
		case NSEWRed_NorthSouth_Special_TrainCond:
			printf("In current state: NSEWRed_NorthSouth_Special_TrainCond\n");
			break;
		case NSEWRed_EastWest:
			printf("In current state: NSEWRed_EastWest\n");
			break;
		case NSEWRed_EastWest_Special_NoTrain:
			printf("In current state: NSEWRed_EastWest_Special_NoTrain\n");
			break;
//		case NSEWRed_EastWest_Special_TrainCond:
//			printf("In current state: NSEWRed_EastWest_Special_TrainCond\n");
//			break;
		case NSGreen:
			printf("In current state: NSGreen\n");
			break;
		case NSYellow:
			printf("In current state: NSYellow\n");
			break;
		case EWGreen:
			printf("In current state: EWGreen\n");
			break;
		case EWYellow:
			printf("In current state: EWYellow\n");
			break;
		case NR_SRGreen:
			printf("In current state: NR_SRGreen\n");
			break;
		case NR_SRYellow:
			printf("In current state: NR_SRYellow\n");
			break;
		case ER_WRGreen:
			printf("In current state: ER_WRGreen\n");
			break;
		case N_NRGreen:
			printf("In current state: N_NRGreen\n");
			break;
		case NGreen_NRYellow:
			printf("In current state: NGreen_NRYellow\n");
			break;
		case NGreen:
			printf("In current state: NGreen\n");
			break;
		case S_SRGreen:
			printf("In current state: S_SRGreen\n");
			break;
		case SGreen_SRYellow:
			printf("In current state: SGreen_SRYellow\n");
			break;
		case SYellow:
			printf("In current state: SYellow\n");
			break;
		case SGreen:
			printf("In current state: SGreen\n");
			break;
		case E_ERGreen:
			printf("In current state: E_ERGreen\n");
			break;
		case EGreen_ERYellow:
			printf("In current state: EGreen_ERYellow\n");
			break;
		case EGreen:
			printf("In current state: EGreen\n");
			break;
		case W_WRGreen:
			printf("In current state: W_WRGreen\n");
			break;
		case WGreen_WRYellow:
			printf("In current state: WGreen_WRYellow\n");
			break;
		case WGreen:
			printf("In current state: WGreen\n");
			break;
		case ER_WRYellow:
			printf("In current state: ER_WRYellow\n");
			break;
		default:
			printf("State Not Handled, Value is: %d\n", localState);
			break;
	}
}

void button_button_CondVar(thread_data * condition_Data, int * buttonRead)
{	//Used for going into the condition variable setion

	pthread_mutex_lock(&condition_Data->button_Mutex);
	//printf("-FSM Thread been Mutexed\n");
	while (!condition_Data->buttonPressed) {
		//printf("-FSM Thread been waiting\n");
		pthread_cond_wait(&condition_Data->button_CondVar,
				&condition_Data->button_Mutex);
	}

	*buttonRead = condition_Data->buttonData;
	condition_Data->buttonPressed = 0;

	pthread_cond_signal(&condition_Data->button_CondVar);
	pthread_mutex_unlock(&condition_Data->button_Mutex);
}

void *button_thread(void *data) { //used to read the button
	printf("-----------Button Thread Started\n");
	thread_data * button_Resources = (thread_data*) data;
	int button_Press;

	while (1) {
		scanf("%d", &button_Press); //wait for user input
		printf("Button pressed\n");

		//Button Lock
		pthread_mutex_lock(&button_Resources->button_Mutex);

		//printf("Button Thread been mutexed\n");
		/*
		while (button_Resources->buttonPressed) {
			//printf("Button Wait\n");
			pthread_cond_wait(&button_Resources->button_CondVar,
					&button_Resources->button_Mutex);
		}
		 */
		button_Resources->buttonData = button_Press;
		button_Resources->buttonPressed = 1;

		pthread_cond_signal(&button_Resources->button_CondVar);
		pthread_mutex_unlock(&button_Resources->button_Mutex);
	}
}


void *menuFSM_thread(void*data)	{
	printf("---------Menu Thread Started\n");

	thread_data * FSM_Resources = (thread_data*) data; //contains mutexs and etc to for resource sharing

	enum controllerFSM FSM = main_Menu; //State machine to be used
	int buttonInput; //hold buttoninput
	char node_String[20] = "yes";



	while (1) {
		switch (FSM) {
		case main_Menu:
			printf("Main Menu State:\n-Status Display:1\n-Control Status:2\n\n");
			/*
			 * Main Menu State
			 * -Status Display:1
			 * -Control Status:2
			 */

			button_button_CondVar(FSM_Resources, &buttonInput);

			if (buttonInput == 1) {
				FSM = status_Display;
			} else if (buttonInput == 2) {
				FSM = control_Menu;
			}
			break;

		case status_Display:
			printf("Status Display State:\n-Connection Status:1\n-Functional Status:2\n-Back:9\n\n");
			/*
			 * Status Display State
			 * -Connection Status:1
			 * -Functional Status:2
			 * -Back:9
			 */
			button_button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 1)
			{
				FSM = conn_Status;
			}
			else if(buttonInput == 2)
			{
				FSM = func_Status;
			}
			else if (buttonInput == 9)
			{
				FSM = main_Menu;
			}

			break;
		case conn_Status:
			printf("Connection Status\n");
			printf("-Node 1: Status| Node 2: Status\n");
			printf("-Node 3: Status| Back:9\n\n");
			/*
			 *Connection Status
			 *-Node 1: Status| Node 2: Status
			 *-Node 3: Status| Back:9
			 */
			button_button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 9) {
				FSM = status_Display;
			}

			break;

		case func_Status:
			//printf("Functional Status\n-Node 1: EW| Node 2: E\n-Node 3: N| Back: 9\n\n");
			/*
			 *Functional Status
			 *-Node 1: EW| Node 2: E
			 *-Node 3: N| Back:9
			 */

			while(1)
			{
				button_button_CondVar(FSM_Resources, &buttonInput);

				if(	(buttonInput == 9)&&(FSM_Resources->serverUpdate==0))
				{
					FSM = status_Display;
					break;
				}



				FSM_Resources->serverUpdate = 0;

			}
			break;

		case control_Menu:
			printf("Control Menu State:\n");
			printf("-Get Schedule/Mode:1\n");
			printf("-Set Schedule/Mode:2\n");
			printf("Back:9\n-Scroll Down:0\n\n");
			/*
			 * Control Menu State
			 * -Get Schedule/Mode:1
			 * -Set Schedule/Mode:2
			 * -Back:9
			 * -Scroll Down:0
			 *
			 */
			button_button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 9) {
				FSM = main_Menu;
			} else if (buttonInput == 0) {
				FSM = control_Menu2;
			}
			break;

		case control_Menu2:
			printf(	"Control Menu State:\n-Force/Cancel State:1\n-Force/Cancel Mode:2\n-Back:9\n-Scroll Up:0\n\n");
			/*
			 * Control Menu State
			 * -Force/Cancel State:1
			 * -Force/Cancel Mode:2
			 * -Back:9
			 * -Scroll Up:0
			 */
			button_button_CondVar(FSM_Resources, &buttonInput);
			if (buttonInput == 9) {
				FSM = main_Menu;
			} else if (buttonInput == 0) {
				FSM = control_Menu;
			}

			break;

		default:
			printf("------------Error in state\n, buttonInput is: %d\n",
					buttonInput);
			break;
		}

	}
}


void *clientThread_Send1(void *data)  {
	thread_data * Command_Resources = (thread_data*)data;
	packet_data commandPacket1;
	packet_reply_data replyData;

	int packet_Input;

	commandPacket1.ClientID = 1; // unique number for this client (optional)

	int serverConn;

	if ((serverConn = name_open(LOCAL_ATTACH_POINT, 0)) == -1) {
		printf("\n    ERROR, could not connect to server!\n\n");
		return EXIT_FAILURE;
	}

	commandPacket1.hdr.type = 0x00;
	commandPacket1.hdr.subtype = 0x00;

	while (1) {
		//scanf("%d", &packet_Input);
		///do condvarrrrrrrrrrrrrrrrrrrrrrr


		commandPacket1.packetData = packet_Input;

		fflush(stdout);
		if (MsgSend(serverConn, &commandPacket1, sizeof(commandPacket1), &replyData, sizeof(replyData)) == -1)
		{
		}
		else
		{
			printf("   -->Reply is: '%s'\n", replyData.buf);
			if(replyData.reply==0x81)
			{
				printf("Successfully Completed\n");
			}
			else
			{
				printf("Command could not be completed\n");
			}

		}
	}
	// Close the connection

	printf("\n Sending message to server to tell it to close the connection\n");

	name_close(serverConn);

	return EXIT_SUCCESS;
}


void *serverThread(void *data)  {
	thread_data * Update_Resources = (thread_data*)data;

	packet_data dataReceive_Server; //packet to send to server
	packet_reply_data replyData_Server; //packet to send to server

	int server_Recv = 0; // receive code when channel signal comes back

	name_attach_t * serverAttach;

   if ((serverAttach = name_attach(NULL, CENTRAL_ATTACH_POINT, 0)) == NULL)
   {
	   printf("\n Possibly another server with the same name is already running !\n");
	   return EXIT_FAILURE;
   }


   replyData_Server.hdr.type = 0x01;
   replyData_Server.hdr.subtype = 0x00;

   while (1)
   {
	   printf("Now waiting\n");
	   server_Recv = MsgReceive(serverAttach->chid, &dataReceive_Server, sizeof(dataReceive_Server), NULL);

	   if (server_Recv == -1)
	   {
		   printf("\nFailed to dataReceive_ServerReceive\n");
		   break;
	   }

	   if (server_Recv == 0)
	   {
		   switch (dataReceive_Server.hdr.code)
		   {
				   default:
				   // Some other pulse sent by one of your processes or the kernel
				   printf("\nServer got some other pulse %d\n", dataReceive_Server.hdr.code);
				   break;

		   }
		   continue;// go back to top of while loop
	   }

	   // for messages:
	   if(server_Recv > 0) // if true then A message was received
	   {

		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (dataReceive_Server.hdr.type == _IO_CONNECT )
		   {
			   MsgReply( server_Recv, EOK, NULL, 0 );
			   printf("\n gns service is running....");
			   continue;
		   }

		   // Some other I/O message was received; reject it
		   if (dataReceive_Server.hdr.type > _IO_BASE && dataReceive_Server.hdr.type <= _IO_MAX )
		   {
			   MsgError( server_Recv, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   continue;	// go back to top of while loop
		   }

		   TrafficLightPrintState(&dataReceive_Server.trafficLightState);
		   printf("\n");
		   MsgReply(server_Recv, EOK, &replyData_Server, sizeof(replyData_Server));
	   }
	   else
	   {
		   printf("\nERROR: Server received something, but could not handle it correctly\n");
	   }

   }

   // Remove the attach point name from the file system (i.e. /dev/name/local/<myname>)
   name_detach(serverAttach, 0);


	printf("\n Sending message to server to tell it to close the connection\n");
	return EXIT_SUCCESS;
}
