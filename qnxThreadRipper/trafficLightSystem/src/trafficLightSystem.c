#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <errno.h>
#include <stdbool.h>

enum trafficLightStates {
	NSEWRed,
	NSGreen,NSYellow,
	EWGreen,EWYellow,
	NR_SRGreen,NR_SRYellow,
	ER_WRGreen,N_NRGreen,
	NGreen_NRYellow,
	NGreen,
	S_SRGreen,SGreen_SRYellow,
	SYellow,
	SGreen,
	E_ERGreen,
	EGreen_ERYellow,
	EGreen,
	W_WRGreen,
	WGreen_WRYellow,
	WGreen
};

char * progname = "TrafficLight1.c";
#define MY_PULSE_CODE _PULSE_CODE_MINAVAIL

int count = 0;

typedef union
{
	struct _pulse   pulse;
    // your other message structures would go here too
} my_message_t;

typedef struct {
 pthread_mutex_t mutex; // add a mutex object into the structure we want to protect
 int channel_ID;
} thread_data;

void PrintState(enum trafficLightStates *CurState);
void SingleStep_TrafficLight_SM(enum trafficLightStates * CurState );
void *trafficlightFSM_thread(void *data);

// clone variables make it global for easy access
timer_t                 timer_id;
struct itimerspec       itime;
struct itimerspec       itime2;
struct itimerspec       itime3;
bool check_control_command = false;

int main(void) {
	printf("Traffic Light 1 With Periodic Timer\n");
	pthread_t trafficlight_stateMachine_pthread, car_sensor_pthread;
	thread_data resource_data;
	void *pthread_end;
	pthread_create(&trafficlight_stateMachine_pthread, NULL, trafficlightFSM_thread, resource_data);

	pthread_join(trafficlight_stateMachine_pthread, &pthread_end);

	printf("All good. Main Terminated...\n\n");
	return EXIT_SUCCESS;
}

void *trafficlightFSM_thread(void *data){
	printf("---------Traffic Light Thread Started\n");
	int rcvid;
	my_message_t msg;
	thread_data FSM_Resources = *((thread_data*)data);

	// initializer
	struct sigevent event;
	int chid;
	chid = ChannelCreate(0);
	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf(stderr, "%s:  couldn't ConnectAttach to self! \n", progname);
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);

	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_id) == -1)
	{
	   printf (stderr, "%s:  couldn't create a timer, errno %d \n", progname, errno);
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// modification added here
	// 1 second delay interval timer
	itime.it_value.tv_sec = 1;	   // 1 second
	itime.it_value.tv_nsec = 0;    // 0 second
	itime.it_interval.tv_sec = 1;  // 1 second
	itime.it_interval.tv_nsec = 0; // 0 second

	// 2 second delay interval timer
	itime2.it_value.tv_sec = 2; // 2 second
	itime2.it_value.tv_nsec = 0; // 0 secomd
	itime2.it_interval.tv_sec = 2; // 2 second
	itime2.it_interval.tv_nsec = 0; // 0 second

	// immediate delay interval timer
	itime3.it_value.tv_sec = 0;		// 1 second
	itime3.it_value.tv_nsec = 1;    // 100 million nsecs = .1 nsecs
	itime3.it_interval.tv_sec = 0;  // 1 second
	itime3.it_interval.tv_nsec = 1; // 100 million nsecs = .1 nsecs

	enum trafficLightStates CurState = NSEWRed;
	timer_settime(timer_id, 0, &itime3, NULL);
	while (1){
		rcvid = MsgReceive(chid, &msg, sizeof(msg), NULL);
		if (rcvid == 0){
			if(msg.pulse.code == MY_PULSE_CODE){
				SingleStep_TrafficLight_SM( &CurState );
			}
		}
		fflush(stdout);
	}
}

void SingleStep_TrafficLight_SM(enum trafficLightStates *CurState ){
	switch (*CurState){
			case NSEWRed:
				count += 1;
				if(count == 1){
					PrintState(CurState);
					*CurState = NSGreen;
					timer_settime(timer_id, 0, &itime, NULL);
				}
				else if(count == 2){
					count = 1;
					PrintState(CurState);
					*CurState = EWGreen;
					timer_settime(timer_id, 0, &itime, NULL);
				}
				break;
			case S_SRGreen:
				PrintState(CurState);
				*CurState = SGreen_SRYellow;
				// 1 second delay timer interval
				timer_settime(timer_id, 0, &itime, NULL);
				break;
			case SGreen_SRYellow:
				PrintState(CurState);
				*CurState = SGreen;
				timer_settime(timer_id, 0, &itime, NULL);
				break;
			case SGreen:
				PrintState(CurState);
				*CurState = NSGreen;
				timer_settime(timer_id, 0, &itime, NULL);
				break;
			case NSGreen:
				PrintState(CurState);
				*CurState = NSYellow;
				// 1 second delay timer interval
				timer_settime(timer_id, 0, &itime, NULL);
				break;
			case NSYellow:
				PrintState(CurState);
				*CurState = NSEWRed;
				// 1 second delay timer interval
				timer_settime(timer_id, 0, &itime, NULL);
				break;
			case EWGreen:
				PrintState(CurState);
				*CurState = EWYellow;
				// 1 second delay timer interval
				timer_settime(timer_id, 0, &itime, NULL);
				break;
			case EWYellow:
				PrintState(CurState);
				*CurState = NSEWRed;
				// 1 second delay timer interval
				timer_settime(timer_id, 0, &itime, NULL);
				break;
	}
}

void PrintState(enum trafficLightStates *CurState){
	//printf("In Do Something 1\n");
	int localState = *CurState;
	//printf("In raw state: %d\n", localState);
	switch (*CurState){
		case NSEWRed:
			printf("In current state: NSEWRed\n");
			break;
		case NSGreen:
			printf("In current state: NSGreen\n");
			break;
		case NSYellow:
			printf("In current state: NSYellow\n");
			break;
		case EWGreen:
			printf("In current state: EWGreen\n");
			break;
		case EWYellow:
			printf("In current state: EWYellow\n");
			break;
		case NR_SRGreen:
			printf("In current state: NR_SRGreen\n");
			break;
		case NR_SRYellow:
			printf("In current state: NR_SRYellow\n");
			break;
		case NGreen:
			printf("In current state: NR_SRYellow\n");
			break;
		case S_SRGreen:
			printf("In current state: S_SRGreen");
			break;
		case SGreen_SRYellow:
			printf("In current state: SGreen_SRYellow");
			break;
	}
}
