/*
 * BeagleBoneIO_simpleLEDS.c
 *
 *  Created on: 24/04/2017
 *      Author: Samuel Ippolito
 */

#include <stdlib.h>
#include <stdio.h>
#include <hw/inout.h>      // for in32() and out32();
#include <sys/mman.h>      // for mmap_device_io();
#include <sys/neutrino.h>  // for ThreadCtl( _NTO_TCTL_IO_PRIV , NULL);
#include <stdint.h>		   // for unit32 types

#define AM335X_CONTROL_MODULE_BASE   (uint64_t) 0x44E10000
#define AM335X_CONTROL_MODULE_SIZE   (size_t)   0x00001448
#define AM335X_GPIO_SIZE             (uint64_t) 0x00001000
#define AM335X_GPIO1_BASE            (size_t)   0x4804C000

#define BUZZER  (1<<19)	  // GPIO1_19 // EHRPWM1B
#define LED0	(1<<21)   // GPIO1_21
#define LED1	(1<<22)   // GPIO1_22
#define LED2	(1<<23)   // GPIO1_23
#define LED3	(1<<24)   // GPIO1_24

// GPMC_A1_Configuration
#define PIN_MODE_0   0x00
#define PIN_MODE_1   0x01
#define PIN_MODE_2   0x02
#define PIN_MODE_3   0x03
#define PIN_MODE_4   0x04
#define PIN_MODE_5   0x05
#define PIN_MODE_6   0x06
#define PIN_MODE_7   0x07

// PIN MUX Configuration strut values  (page 1420 from TRM)
#define PU_ENABLE    0x00
#define PU_DISABLE   0x01
#define PU_PULL_UP   0x01
#define PU_PULL_DOWN 0x00
#define RECV_ENABLE  0x01
#define RECV_DISABLE 0x00
#define SLEW_FAST    0x00
#define SLEW_SLOW    0x01

#define GPIO_OE        0x134
#define GPIO_DATAIN    0x138
#define GPIO_DATAOUT   0x13C

// the two is in the GPIO3
#define K1_SWITCH (1 << 21)
#define K2_SWITCH (1 << 19)

// the two is in the GPIO1
#define K3_SWITCH (1 << 28)
#define K4_SWITCH (1 << 29)

#define GPIO1_19_pinConfig 0x84C // conf_gpmc_a3

typedef union _CONF_MODULE_PIN_STRUCT // See TRM Page 1420
{
	unsigned int d32;
	struct {    // name: field size unsigned
		int conf_mmode : 3;       // LSB unsigned
		int conf_puden : 1;
		unsigned int conf_putypesel : 1;
		unsigned int conf_rxactive : 1;
		unsigned int conf_slewctrl : 1;
		unsigned int conf_res_1 : 13; // reserved
		unsigned int conf_res_2 : 12; // reserved MSB
		} b;
} _CONF_MODULE_PIN;

// making the ISR
typedef struct
{
	int count_thread;
	uintptr_t gpio1_base;
	struct sigevent pevent; // remember to fill in "event" structure in main
}ISR_data;

ISR_data ISR_area_data;

const struct sigevent* Inthandler( void* area, int id )
{
	// 	"Do not call any functions in ISR that call kernerl - including printf()
	//struct sigevent *pevent = (struct sigevent *) area;
	ISR_data *p_ISR_data = (ISR_data *) area;

	InterruptMask(GPIO1_IRQ, id);  // Disable all hardware interrupt

	// must do this in the ISR  (else stack over flow and system will crash
	out32(p_ISR_data->gpio1_base + GPIO_IRQSTATUS_1, SD0); //clear IRQ

	// do this to tell us how many times this handler gets called
	p_ISR_data->count_thread++;
	// got IRQ.
	// work out what it came from

    InterruptUnmask(GPIO1_IRQ, id);  // Enable a hardware interrupt

    // return a pointer to an event structure (preinitialized
    // by main) that contains SIGEV_INTR as its notification type.
    // This causes the InterruptWait in "int_thread" to unblock.
	return (&p_ISR_data->pevent);
}

int setup();

int main(int argc, char *argv[])
{
	int result = setup();

	if(setup != EXIT_SUCCESS){
		uintptr_t gpio1_base = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);
		volatile uint32_t val = 0;
		val  = in32(gpio1_base + GPIO_DATAOUT);
		val |= (LED0|BUZZER);
		out32(gpio1_base + GPIO_DATAOUT, val); // write new value
		// write to the LEDS
		int i=0;
		while(i<4)
		{
			printf("LEDS value = %i\n", i);
			// Write GPIO1 data output register
			val  = in32(gpio1_base + GPIO_DATAOUT); // read in current value
			if(i % 2){
				val &= ~(LED0|BUZZER);
			}
			else{
				val |= (LED0|BUZZER);
			}
			i++;
			//val &= ~(LED0|LED1|LED2|LED3); // clear the bits that we might change
			//val |= ((i++)&0x0F)<<21;       // set the pattern to display
			out32(gpio1_base + GPIO_DATAOUT, val); // write new value
			sleep(1);
		}
		val  = in32(gpio1_base + GPIO_DATAOUT);
		val |= (LED0|BUZZER);
		out32(gpio1_base + GPIO_DATAOUT, val); // write new value

		for(;;){
			InterruptWait(0, NULL);
		}
	}
	else{
		prinf("Cannot Setup Periperhals\n");
	}
	munmap_device_io(gpio1_base, AM335X_GPIO_SIZE);
	printf("Main Terminated...!\n");
	return EXIT_SUCCESS;
}

int setup(){
	printf("BeagleBone LED tester\n");
	ThreadCtl( _NTO_TCTL_IO_PRIV, 1);

	uintptr_t control_module = mmap_device_io(AM335X_CONTROL_MODULE_SIZE,
											 AM335X_CONTROL_MODULE_BASE);
	uintptr_t gpio1_base = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);
	volatile uint32_t val = 0;

	if( !gpio1_base )
	{
		// An error has occurred
		perror("Can't map Control Base Module / GPIO Base");
		printf("Mapping IO ERROR! Main Terminated...!\n");
		return EXIT_SUCCESS;
	}
	printf("I/O Port Mapping is Successful!\n");

	// Read GPIO output enable register (data direction in/out)
	//  0 The corresponding GPIO port is configured as an output.
	//  1 The corresponding GPIO port is configured as an input.

	// READ the value of the output enable register for the
	// I/O pins that are connected to the LEDS
	val  = in32(gpio1_base + GPIO_OE);
	printf("original value of GPIO_1 output enable register= %#010x\n", val);

	// write value to output enable
	val &= ~(LED0|LED1|LED2|LED3|BUZZER);
	out32(gpio1_base + GPIO_OE, val);

	// confirm that GPIO_OE is set to output
	val  = in32(gpio1_base + GPIO_OE);
	printf("new value of GPIO_1 output enable register= %#010x\n", val);

	in32s(&val, 1, control_module + GPIO1_19_pinConfig );
	printf("Original pinmux configuration for GPIO1_19 = %#010x\n", val);

	// set up pin mux for the pins we are going to use  (see page 1354 of TRM)
	volatile _CONF_MODULE_PIN pinConfigGPMC; // Pin configuration strut
	pinConfigGPMC.d32 = 0;
	// Pin MUX register default setup for input (GPIO input, disable pull up/down - Mode 7)
	pinConfigGPMC.b.conf_slewctrl = SLEW_SLOW;    // Select between faster or slower slew rate
	pinConfigGPMC.b.conf_rxactive = RECV_ENABLE;  // Input enable value for the PAD
	pinConfigGPMC.b.conf_putypesel= PU_PULL_UP;   // Pad pullup/pulldown type selection
	pinConfigGPMC.b.conf_puden = PU_ENABLE;       // Pad pullup/pulldown enable
	pinConfigGPMC.b.conf_mmode = PIN_MODE_7;      // Pad functional signal mux select 0 - 7

	// Write to PinMux registers for the GPIO1_28
	out32(control_module + GPIO1_19_pinConfig, pinConfigGPMC.d32);
	in32s(&val, 1, control_module + GPIO1_19_pinConfig);   // Read it back
	printf("New configuration register for GPIO1_19 = %#010x\n", val);
}

void DecodeKeyValue(uint32_t word){
	switch(word){
	case 0x01:
		printf("Key 1 Pressed\n");
		pthread_create(NULL, NULL, Flash_LED0_ex, NULL); // flash LED
		break;
	}
}

// Thread used to Flash the 4 LEDs on the BeagleBone for 100ms
void *Flash_LED0_ex(void *notused)
{
	pthread_detach(pthread_self());  // no need for this thread to join
	uintptr_t gpio1_port = mmap_device_io(AM335X_GPIO_SIZE, AM335X_GPIO1_BASE);

	uintptr_t val;
	// Write GPIO data output registerzz
	val  = in32(gpio1_port + GPIO_DATAOUT);
	val |= (LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	usleep(100000);  // 100 ms wait
	//sched_yield();  // if used without the usleep, this line will flash the LEDS for ~4ms

	val  = in32(gpio1_port + GPIO_DATAOUT);
	val &= ~(LED0|LED1|LED2|LED3);
	out32(gpio1_port + GPIO_DATAOUT, val);

	munmap_device_io(gpio1_port, AM335X_GPIO_SIZE);
}
