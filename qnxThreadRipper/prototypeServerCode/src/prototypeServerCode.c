#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <errno.h>
#include <stdbool.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <share.h>
#include <time.h>
#include <errno.h>
#include <math.h>

// Define what the channel is called. It will be located at <hostname>/dev/name/local/myname"
// change myname to something unique for you (The client should be set to same name)
#define ATTACH_POINT "TrafficNode1"

// hostname RMIT_BBB_v5_07 // 192.168.201.138 (next to chris)
// hostname RMIT_BBB_v5_06 // 192.168.201.71
// hostname RMIT_BBB_v5_03 // 192.168.201.24
// hostname RMIT_BBB_v5_09 // 192.168.201.149

//#define ATTACH_POINT "/net/RMIT_BBB_v5_07/dev/name/local/trafficServer"

#define BUF_SIZE 100

enum pedStates{
	NSEWPRed,
	NPGreen,
	NPFlash,
	SPGreen,
	SPFlash,
	EPGreen,
	EPFLash,
	WPGreen,
	WPFlash,
	NP_SPGreen,
	NP_SPFlash,
	EP_WPGreen,
	EP_WPFlash
};

enum trafficLightStates {
	NSEWRed_NorthSouth,
	NSEWRed_NorthSouth_Special_NoTrain,
	NSEWRed_NorthSouth_Special_TrainCond,
	NSEWRed_EastWest,
	NSEWRed_EastWest_Special_NoTrain,
	NSGreen,
	NSYellow,
	EWGreen,
	EWYellow,
	NR_SRGreen,
	NR_SRYellow,
	ER_WRGreen,
	ER_WRYellow,
	N_NRGreen,
	NGreen_NRYellow,
	NGreen,
	S_SRGreen,
	SGreen_SRYellow,
	SYellow,
	SGreen,
	E_ERGreen,
	EGreen_ERYellow,
	EGreen,
	W_WRGreen,
	WGreen_WRYellow,
	WGreen
};

enum Rail_Crossing_States {
	Train_Present,
	Train_NotPresent
};

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
	int commandId;
    int packetData;     // our data
} my_data;

typedef struct {
	enum trafficLightStates TrafficCurState;
	int greenTimingStart;
	int greenTimingDuration;
	pthread_mutex_t trafficStateMutex;

	int sensorNewData;
	// were using for sensor based
	int NorthStraightSensor;
	int SouthStraightSensor;
	int EastStraightSensor;
	int WestStraightSensor;
	int NRSensor; // 1 0
	int SRSensor; // 1 0
	int ERSensor; // 1 0
	int WRSensor; // 1 0
	char sensorInput;
	pthread_mutex_t sensorMutex;

	enum pedStates PedsCurState;
	pthread_mutex_t pedStateMutex;

	int pedStateNewData;
	int NPStatus; // 1 0
	int SPStatus; // 1 0
	int EPStatus; // 1 0
	int WPStatus; // 1 0
	char buttonInput;
	pthread_mutex_t buttonMutex; // add a mutex object into the structure we want to protect
	int channel_ID;

	enum Rail_Crossing_States TrainCrossingCurState;
	pthread_mutex_t trainCrossingStateMutex;
} thread_data;

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	char buf[BUF_SIZE]; // Message we send back to clients to tell them the messages was processed correctly.
	int reply;

	enum trafficLightStates trafficLightState;
	enum pedStates pedastrianStates;
} packet_reply_data;

// prototypes
void *server_thread(void *data);

int main(void){
	printf("Server running\n");
	void *pthread_end;

	thread_data resource_data;

    pthread_t serverThread_pthread;
    // initalize all the data
    resource_data.TrafficCurState = NSEWRed_NorthSouth;
	resource_data.greenTimingStart = 0;
	resource_data.greenTimingDuration = 0;
	resource_data.trafficStateMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

	resource_data.NorthStraightSensor = 0;
	resource_data.SouthStraightSensor = 0;
	resource_data.EastStraightSensor = 0;
	resource_data.WestStraightSensor = 0;

	resource_data.NRSensor = 0;
	resource_data.SRSensor = 0;
	resource_data.ERSensor = 0;
	resource_data.WRSensor = 0;
	resource_data.sensorInput = 'o';
	resource_data.sensorMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

	resource_data.PedsCurState = NSEWPRed;
	resource_data.pedStateMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	resource_data.pedStateNewData = 0;
	resource_data.NPStatus = 0;
	resource_data.SPStatus = 0;
	resource_data.EPStatus = 0;
	resource_data.WPStatus = 0;

	resource_data.buttonInput = 'o';
	resource_data.buttonMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
	resource_data.channel_ID = ChannelCreate(0);

	resource_data.TrainCrossingCurState = Train_NotPresent;
	resource_data.trainCrossingStateMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

	char hostnm[100];
	memset(hostnm, '\0', 100);
	hostnm[99] = '\n';
	gethostname(hostnm, sizeof(hostnm));
	printf("--> Machine hostname is: '%s'\n", hostnm);
	printf ("--> The PID of this process is %d\n", getpid());

    pthread_create(&serverThread_pthread, NULL, server_thread,&resource_data);
    pthread_join(serverThread_pthread, &pthread_end);
}


/*** Server code ***/
void *server_thread(void *data){
   name_attach_t *attach;

   thread_data *FSM_Resources = (thread_data*)data;

   if ((attach = name_attach(NULL, ATTACH_POINT, 0)) == NULL)
   {
       printf("\nFailed to name_attach on ATTACH_POINT: %s \n", ATTACH_POINT);
       printf("\n Possibly another server with the same name is already running !\n");
	   return EXIT_FAILURE;
   }

   printf("Server Listening for Clients on ATTACH_POINT: %s \n\n", ATTACH_POINT);

   	/*
	 *  Server Loop
	 */
   my_data msg;
   int rcvid=0, msgnum=0;  		// no message received yet
   int Stay_alive=0, living=0;	// server stays running (ignores _PULSE_CODE_DISCONNECT request)

   packet_reply_data replymsg; // replymsg structure for sending back to client
   replymsg.hdr.type = 0x01;
   replymsg.hdr.subtype = 0x00;

   while (1)
   {
	   // Do your MsgReceive's here now with the chid
       rcvid = MsgReceive(attach->chid, &msg, sizeof(msg), NULL);
       /*
        * Wait for message or pulse on channel
        * (channe id, where to store data, size of buffer, info)
        * blocks until message received
        */

       if (rcvid == -1)  // Error condition, exit
       {
           printf("\nFailed to MsgReceive\n");
           break;
       }

       if (rcvid == 0)  //  Pulse received, work out what type
       {
           switch (msg.hdr.code)
           {
			   case _PULSE_CODE_UNBLOCK:
				   printf("\nServer got _PULSE_CODE_UNBLOCK after %d, msgnum\n", msgnum);
				   break;

			   case _PULSE_CODE_COIDDEATH:  // from the kernel
				   printf("\nServer got _PULSE_CODE_COIDDEATH after %d, msgnum\n", msgnum);
				   break;

			   case _PULSE_CODE_THREADDEATH: // from the kernel
				   printf("\nServer got _PULSE_CODE_THREADDEATH after %d, msgnum\n", msgnum);
				   break;

			   default:
				   // Some other pulse sent by one of your processes or the kernel
				   printf("\nServer got some other pulse after %d\n", msg.hdr.code);
				   break;

           }
           continue;// go back to top of while loop
       }

       // for messages:
       if(rcvid > 0) // if true then A message was received
       {
		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (msg.hdr.type == _IO_CONNECT )
		   {
			   MsgReply( rcvid, EOK, NULL, 0 );
			   printf("\n gns service is running....");
			   continue;
		   }

		   // Some other I/O message was received; reject it
		   if (msg.hdr.type > _IO_BASE && msg.hdr.type <= _IO_MAX )
		   {
			   MsgError( rcvid, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   continue;	// go back to top of while loop
		   }

		   printf("Command ID: %d\n", msg.ClientID);
		   printf("Packet Data: %d\n\n", msg.packetData);

		   sprintf(replymsg.buf, "Message %d received", msgnum);
		   printf("Server received data packet with value of '%d' from client (ID:%d), ", msg.packetData, msg.ClientID);
		   fflush(stdout);

		   sprintf(replymsg.buf, "Acknowledgement 1");
		   replymsg.trafficLightState = NSEWRed_NorthSouth;
		   MsgReply(rcvid, EOK, &replymsg, sizeof(replymsg));
       }
       else
       {
		   printf("\nERROR: Server received something, but could not handle it correctly\n");
       }

   }

   // Remove the attach point name from the file system (i.e. /dev/name/local/<myname>)
   name_detach(attach, 0);

   return EXIT_SUCCESS;
}

void TrafficLightPrintState(enum trafficLightStates *CurState){
	int localState = *CurState;
	switch (*CurState){
		case NSEWRed_NorthSouth:
			printf("In current state: NSEWRed_NorthSouth\n");
			break;
		case NSEWRed_NorthSouth_Special_NoTrain:
			printf("In current state: NSEWRed_NorthSouth_Special_NoTrain\n");
			break;
		case NSEWRed_NorthSouth_Special_TrainCond:
			printf("In current state: NSEWRed_NorthSouth_Special_TrainCond\n");
			break;
		case NSEWRed_EastWest:
			printf("In current state: NSEWRed_EastWest\n");
			break;
		case NSEWRed_EastWest_Special_NoTrain:
			printf("In current state: NSEWRed_EastWest_Special_NoTrain\n");
			break;
		case NSGreen:
			printf("In current state: NSGreen\n");
			break;
		case NSYellow:
			printf("In current state: NSYellow\n");
			break;
		case EWGreen:
			printf("In current state: EWGreen\n");
			break;
		case EWYellow:
			printf("In current state: EWYellow\n");
			break;
		case NR_SRGreen:
			printf("In current state: NR_SRGreen\n");
			break;
		case NR_SRYellow:
			printf("In current state: NR_SRYellow\n");
			break;
		case ER_WRGreen:
			printf("In current state: ER_WRGreen\n");
			break;
		case N_NRGreen:
			printf("In current state: N_NRGreen\n");
			break;
		case NGreen_NRYellow:
			printf("In current state: NGreen_NRYellow\n");
			break;
		case NGreen:
			printf("In current state: NGreen\n");
			break;
		case S_SRGreen:
			printf("In current state: S_SRGreen\n");
			break;
		case SGreen_SRYellow:
			printf("In current state: SGreen_SRYellow\n");
			break;
		case SYellow:
			printf("In current state: SYellow\n");
			break;
		case SGreen:
			printf("In current state: SGreen\n");
			break;
		case E_ERGreen:
			printf("In current state: E_ERGreen\n");
			break;
		case EGreen_ERYellow:
			printf("In current state: EGreen_ERYellow\n");
			break;
		case EGreen:
			printf("In current state: EGreen\n");
			break;
		case W_WRGreen:
			printf("In current state: W_WRGreen\n");
			break;
		case WGreen_WRYellow:
			printf("In current state: WGreen_WRYellow\n");
			break;
		case WGreen:
			printf("In current state: WGreen\n");
			break;
		case ER_WRYellow:
			printf("In current state: ER_WRYellow\n");
			break;
		default:
			printf("State Not Handled\n");
			break;
	}
}
