/*
 * I2C LCD test program using BBB I2C1 using timers on 4 threads.
 *
 * LCD data:  MIDAS  MCCOG22005A6W-FPTLWI  Alphanumeric LCD, 20 x 2, Black on White, 3V to 5V, I2C, English, Japanese, Transflective
 * LCD data sheet, see:
 *                      http://au.element14.com/midas/mccog22005a6w-fptlwi/lcd-alpha-num-20-x-2-white/dp/2425758?ost=2425758&selectedCategoryId=&categoryNameResp=All&searchView=table&iscrfnonsku=false
 *
 *  BBB P9 connections:
 *    - P9_Pin17 - SCL - I2C1	 GPIO3_2
 *    - P9_Pin18 - SDA - I2C1    GPIO3_1
 *
 *  LCD connections:
 *    - pin 1 � VOUT to the 5V     (external supply should be used)
 *    - pin 4 � VDD  to the 5V
 *    - pin 8 � RST  to the 5V
 *
 *    - pin 2 - Not connected  (If 3.3V is used then add two caps)
 *    - pin 3 - Not connected  (If 3.3V is used then add two caps)
 *
 *    - pin 5 - VSS  to Ground
 *    - pin 6 � SDA  to the I2C SDA Pin
 *    - pin 7 - SCL  to the I2C SCL Pin
 *
 * Author:  Samuel Ippolito
 * Date:	10/08/2018
 */

#include <stdlib.h>
#include <stdio.h>
#include "stdint.h"
#include <fcntl.h>
#include <devctl.h>
#include <hw/i2c.h>
#include <errno.h>
#include <unistd.h>
#include <sys/neutrino.h>
#include <sched.h>

#include <time.h>
#include <sys/netmgr.h>

#define DATA_SEND 0x40  // sets the Rs value high
#define Co_Ctrl   0x00  // mode to tell LCD we are sending a single command

#define MY_PULSE_CODE   _PULSE_CODE_MINAVAIL


// Function prototypes
int main(int argc, char *argv[]);
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData);
void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column);
void Initialise_LCD (int fd, _Uint32t LCDi2cAdd);

int synchronized = 1;

typedef struct
{
	int fd;
	uint8_t Address;
	uint8_t mode;
	pthread_mutex_t mutex;
} LCD_connect;

typedef union
{
	struct _pulse   pulse;
    // your other message structures would go here too
} my_message_t;

void *LCDthread_A_ex (void *data)
{
	LCD_connect *td = (LCD_connect*) data;
	uint8_t	LCDdata[10] = {};

	struct sigevent         event;
	struct itimerspec       itime;
	timer_t                 timer_id;
	int                     chid;
	int                     rcvid;
	my_message_t            msg;

	chid = ChannelCreate(0); // Create a communications channel

	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf("LCDthread_A_ex:  couldn't ConnectAttach to self!\n");
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);
	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_id) == -1)
	{
	   printf("LCDthread_A_ex:  couldn't create timer!\n");
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// setup the timer (0s initial delay value, 1s reload interval)
	itime.it_value.tv_sec = 0;
	itime.it_value.tv_nsec = 1;
	itime.it_interval.tv_sec = 1; 	  		   // 1 second
	itime.it_interval.tv_nsec= 0;

	// and start the timer!
	timer_settime(timer_id, 0, &itime, NULL);

	int i;
	for(i=0;i<100;i++)
	{
		// wait for message/pulse
		rcvid = MsgReceive(chid, &msg, sizeof(msg), NULL);

		// determine who the message came from
		if (rcvid == 0) // this process
		{
		   // received a pulse, now check "code" field...
		   if (msg.pulse.code == MY_PULSE_CODE) // we got a pulse
		   {
			   if(synchronized) pthread_mutex_lock(&td->mutex);     //lock the function to make sure the variables are protected
			   	   // write some Text to the LCD screen
			   	   SetCursor(td->fd, td->Address,0,0); // set cursor on LCD to first position first line
			   	   sprintf(LCDdata,"A(0,0)=%d",i);
			   	   I2cWrite_(td->fd, td->Address, DATA_SEND, &LCDdata[0], sizeof(LCDdata));		// write new data to I2C
			   if(synchronized) pthread_mutex_unlock(&td->mutex);	//unlock the functions to release the variables for use by other functions
			}
			// else other pulses ...
	   }
	   // else other messages ...
	}
	return 0;
}

void *LCDthread_B_ex (void *data)
{
	LCD_connect *td = (LCD_connect*) data;
	uint8_t	LCDdata[10] = {};

	struct sigevent         event;
	struct itimerspec       itime;
	timer_t                 timer_id;
	int                     chid;
	int                     rcvid;
	my_message_t            msg;

	chid = ChannelCreate(0); // Create a communications channel

	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf("LCDthread_A_ex:  couldn't ConnectAttach to self!\n");
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}
	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);
	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_id) == -1)
	{
	   printf("LCDthread_A_ex:  couldn't create timer!\n");
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// setup the timer (0s initial delay value, 0.5s reload interval)
	itime.it_value.tv_sec = 0;
	itime.it_value.tv_nsec =1;
	itime.it_interval.tv_sec = 0;
	itime.it_interval.tv_nsec= 500000000;    // 500 million nsecs = .5 secs

	// and start the timer!
	timer_settime(timer_id, 0, &itime, NULL);


	int i;
	for(i=0;i<100;i++)
	{
		// wait for message/pulse
		rcvid = MsgReceive(chid, &msg, sizeof(msg), NULL);

		// determine who the message came from
		if (rcvid == 0) // this process
		{
		   // received a pulse, now check "code" field...
		   if (msg.pulse.code == MY_PULSE_CODE) // we got a pulse
		   {
			   if(synchronized) pthread_mutex_lock(&td->mutex);     //lock the function to make sure the variables are protected
			   	   // write some Text to the LCD screen
			   	   SetCursor(td->fd, td->Address,0,10); // set cursor on LCD to first position first line
			   	   sprintf(LCDdata,"B(0,10)=%d",i);
			   	   I2cWrite_(td->fd, td->Address, DATA_SEND, &LCDdata[0], sizeof(LCDdata));		// write new data to I2C
			   if(synchronized) pthread_mutex_unlock(&td->mutex);	//unlock the functions to release the variables for use by other functions
			}
			// else other pulses ...
	   }
	   // else other messages ...
	}
	return 0;
}

void *LCDthread_C_ex (void *data)
{
	LCD_connect *td = (LCD_connect*) data;
	uint8_t	LCDdata[10] = {};

	struct sigevent         event;
	struct itimerspec       itime;
	timer_t                 timer_id;
	int                     chid;
	int                     rcvid;
	my_message_t            msg;

	chid = ChannelCreate(0); // Create a communications channel

	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf("LCDthread_A_ex:  couldn't ConnectAttach to self!\n");
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}
	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);
	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_id) == -1)
	{
	   printf("LCDthread_A_ex:  couldn't create timer!\n");
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// setup the timer (0s initial delay value, 0.2s reload interval)
	itime.it_value.tv_sec = 0;
	itime.it_value.tv_nsec =1;
	itime.it_interval.tv_sec = 0;
	itime.it_interval.tv_nsec= 200000000;    // 200 million nsecs = .2 secs

	// and start the timer!
	timer_settime(timer_id, 0, &itime, NULL);


	int i;
	for(i=0;i<100;i++)
	{
		// wait for message/pulse
		rcvid = MsgReceive(chid, &msg, sizeof(msg), NULL);

		// determine who the message came from
		if (rcvid == 0) // this process
		{
		   // received a pulse, now check "code" field...
		   if (msg.pulse.code == MY_PULSE_CODE) // we got a pulse
		   {
			   if(synchronized) pthread_mutex_lock(&td->mutex);     //lock the function to make sure the variables are protected
			   	   // write some Text to the LCD screen
			   	   SetCursor(td->fd, td->Address,1,0); // set cursor on LCD to first position first line
			   	   sprintf(LCDdata,"C(1,0)=%d",i);
			   	   I2cWrite_(td->fd, td->Address, DATA_SEND, &LCDdata[0], sizeof(LCDdata));		// write new data to I2C
			   if(synchronized) pthread_mutex_unlock(&td->mutex);	//unlock the functions to release the variables for use by other functions
			}
			// else other pulses ...
	   }
	   // else other messages ...
	}
	return 0;
}

void *LCDthread_D_ex (void *data)
{
	LCD_connect *td = (LCD_connect*) data;
	uint8_t	LCDdata[10] = {};

	struct sigevent         event;
	struct itimerspec       itime;
	timer_t                 timer_id;
	int                     chid;
	int                     rcvid;
	my_message_t            msg;

	chid = ChannelCreate(0); // Create a communications channel

	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf("LCDthread_A_ex:  couldn't ConnectAttach to self!\n");
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}
	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);
	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_id) == -1)
	{
	   printf("LCDthread_A_ex:  couldn't create timer!\n");
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// setup the timer (0s initial delay value, 0.1s reload interval)
	itime.it_value.tv_sec = 0;
	itime.it_value.tv_nsec =1;
	itime.it_interval.tv_sec = 0;
	itime.it_interval.tv_nsec= 100000000;    // 100 million nsecs = .1 secs

	// and start the timer!
	timer_settime(timer_id, 0, &itime, NULL);


	int i;
	for(i=0;i<100;i++)
	{
		// wait for message/pulse
		rcvid = MsgReceive(chid, &msg, sizeof(msg), NULL);

		// determine who the message came from
		if (rcvid == 0) // this process
		{
		   // received a pulse, now check "code" field...
		   if (msg.pulse.code == MY_PULSE_CODE) // we got a pulse
		   {
			   if(synchronized) pthread_mutex_lock(&td->mutex);     //lock the function to make sure the variables are protected
			   	   // write some Text to the LCD screen
			   	   SetCursor(td->fd, td->Address,1,10); // set cursor on LCD to first position first line
			   	   sprintf(LCDdata,"D(1,10)=%d",i);
			   	   I2cWrite_(td->fd, td->Address, DATA_SEND, &LCDdata[0], sizeof(LCDdata));		// write new data to I2C
			   if(synchronized) pthread_mutex_unlock(&td->mutex);	//unlock the functions to release the variables for use by other functions
			}
			// else other pulses ...
	   }
	   // else other messages ...
	}
	return 0;
}


int main(int argc, char *argv[])
{
	int file;
	int error;
	volatile uint8_t LCDi2cAdd = 0x3C;
	_Uint32t speed = 10000; // nice and slow (will work with 200000)
	LCD_connect td;

	uint8_t	LCDdata[21] = {};

	// Create the mutex
	pthread_mutex_init(&td.mutex,NULL);		// pass NULL as the attr parameter to use the default attributes for the mutex

	// Open I2C resource and set it up
	if ((file = open("/dev/i2c1",O_RDWR)) < 0)	  // OPEN I2C1
		printf("Error while opening Device File.!!\n");
	else
		printf("I2C1 Opened Successfully\n");

	error = devctl(file,DCMD_I2C_SET_BUS_SPEED,&(speed),sizeof(speed),NULL);  // Set Bus speed
	if (error)
	{
		fprintf(stderr, "Error setting the bus speed: %d\n",strerror ( error ));
		exit(EXIT_FAILURE);
	}
	else
		printf("Bus speed set = %d\n", speed);

	Initialise_LCD(file, LCDi2cAdd);

	usleep(1);


	// launch threads
	pthread_t  th1, th2, th3, th4;
	pthread_mutex_lock(&td.mutex);		//lock the function to make sure the variables are protected
		td.fd     = file;
		td.Address= LCDi2cAdd;
	td.mode   = DATA_SEND;
    pthread_mutex_unlock(&td.mutex);	//unlock the functions to release the variables for use by other functions

	pthread_create (&th1, NULL, LCDthread_A_ex, &td);
	pthread_create (&th2, NULL, LCDthread_B_ex, &td);
	pthread_create (&th3, NULL, LCDthread_C_ex, &td);
	pthread_create (&th4, NULL, LCDthread_D_ex, &td);

	pthread_join (th4, NULL); // wait for fastest thread to finish

	// Destroy the mutex
	pthread_mutex_destroy(&td.mutex);

	printf("\n complete");
	return EXIT_SUCCESS;
}


// Writes to I2C
int  I2cWrite_(int fd, uint8_t Address, uint8_t mode, uint8_t *pBuffer, uint32_t NbData)
{
	i2c_send_t hdr;
    iov_t sv[2];
    int status, i;

    uint8_t LCDpacket[21] = {};  // limited to 21 characters  (1 control bit + 20 bytes)

    // set the mode for the write (control or data)
    LCDpacket[0] = mode;  // set the mode (data or control)

	// copy data to send to send buffer (after the mode bit)
	for (i=0;i<NbData+1;i++)
		LCDpacket[i+1] = *pBuffer++;

    hdr.slave.addr = Address;
    hdr.slave.fmt = I2C_ADDRFMT_7BIT;
    hdr.len = NbData + 1;  // 1 extra for control (mode) bit
    hdr.stop = 1;

    SETIOV(&sv[0], &hdr, sizeof(hdr));
    SETIOV(&sv[1], &LCDpacket[0], NbData + 1); // 1 extra for control (mode) bit
      // int devctlv(int filedes, int dcmd,     int sparts, int rparts, const iov_t *sv, const iov_t *rv, int *dev_info_ptr);
    status = devctlv(fd, 		  DCMD_I2C_SEND, 2,          0,          sv,              NULL,           NULL);

    if (status != EOK)
    	printf("status = %s\n", strerror ( status ));

    return status;
}


void SetCursor(int fd, uint8_t LCDi2cAdd, uint8_t row, uint8_t column)
{
	uint8_t position = 0x80; // SET_DDRAM_CMD (control bit)
	uint8_t rowValue = 0;
	uint8_t	LCDcontrol = 0;
	if (row == 1)
		rowValue = 0x40;     // memory location offset for row 1
	position = (uint8_t)(position + rowValue + column);
	LCDcontrol = position;
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}


void Initialise_LCD (int fd, _Uint32t LCDi2cAdd)
{
	uint8_t	LCDcontrol = 0x00;

	//   Initialise the LCD display via the I2C bus
	LCDcontrol = 0x38;  // data byte for FUNC_SET_TBL1
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x39; // data byte for FUNC_SET_TBL2
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x14; // data byte for Internal OSC frequency
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x79; // data byte for contrast setting
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x50; // data byte for Power/ICON control Contrast set
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x6C; // data byte for Follower control
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x0C; // data byte for Display ON
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C

	LCDcontrol = 0x01; // data byte for Clear display
	I2cWrite_(fd, LCDi2cAdd, Co_Ctrl, &LCDcontrol, 1);		// write data to I2C
}
