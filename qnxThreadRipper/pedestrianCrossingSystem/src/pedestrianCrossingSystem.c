#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <errno.h>
#include <stdbool.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/dispatch.h>
#include <sys/netmgr.h>
#include <sys/neutrino.h>
#include <share.h>
#include <time.h>
#include <errno.h>

enum pedStates{
	NSEWPRed,
	NPGreen,
	NPFlash,
	SPGreen,
	SPFlash,
	EPGreen,
	EPFLash,
	WPGreen,
	WPFlash,
	NP_SPGreen,
	NP_SPFlash,
	EP_WPGreen,
	EP_WPFlash
};

enum trafficLightStates {
	NSEWRed,
	NSGreen,NSYellow,
	EWGreen,EWYellow,
	NR_SRGreen,NR_SRYellow,
	ER_WRGreen,N_NRGreen,
	NGreen_NRYellow,
	NGreen,
	S_SRGreen,SGreen_SRYellow,
	SYellow,
	SGreen,
	E_ERGreen,
	EGreen_ERYellow,
	EGreen,
	W_WRGreen,
	WGreen_WRYellow,
	WGreen
};

char * progname = "PedsLight1.c";
#define MY_PULSE_CODE _PULSE_CODE_MINAVAIL

// clone variables make it global for easy access
timer_t                 timer_id;
struct itimerspec       itime;
struct itimerspec       itime2;
struct itimerspec       itime3;
bool check_control_command = false;

typedef union {
	struct _pulse   pulse;
	// your other message structures would go here too
} my_message_t;

typedef struct {
	enum trafficLightStates TrafficCurState;
	enum pedStates PedsCurState;
	bool NPStatus;
	bool SPStatus;
	bool EPStatus;
	bool WPStatus;
	char buttonInput;
	pthread_mutex_t mutex; // add a mutex object into the structure we want to protect
	int channel_ID;
} thread_data;

typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
	int ClientID; // our data (unique id from client)
    int data;     // our data
} packet_data; //

#define BUF_SIZE 100
typedef struct
{
	struct _pulse hdr; // Our real data comes after this header
    char buf[BUF_SIZE];// Message we send back to clients to tell them the messages was processed correctly.
} packet_reply_data;

typedef struct {
	enum trafficLightStates TrafficCurState;
	enum pedStates PedsCurState;
	bool NPStatus;
	bool SPStatus;
	bool EPStatus;
	bool WPStatus;
	char buttonInput;
	pthread_mutex_t mutex;
} trafficLight_data;

int packetListen(int chid, packet_data * msg_recv, packet_reply_data * msg_reply_send);
void PedStatePrintState(enum pedStates *CurState);
void *pedestrianFSM_thread(void *data);
void SingleStep_TrafficLight_SM(enum trafficLightStates *TrafficLightCurState, enum pedStates *PedestrianCurState);
void *peds_button_thread(void *data);
void SingleStep_Pedestrian_SM(enum pedStates *CurState);

int main(void) {
	printf("Pedestrian Crossing Prototype");
	pthread_t peds_stateMachine_pthread, peds_button_pthread;
	thread_data resource_data;
	void *pthread_end;

	resource_data.channel_ID = ChannelCreate(0);

	pthread_create(&peds_stateMachine_pthread,NULL,pedestrianFSM_thread,&resource_data);
	pthread_create(&peds_button_pthread,NULL,peds_button_thread,&resource_data);

	pthread_join(peds_stateMachine_pthread,&pthread_end);
	pthread_join(peds_button_pthread,&pthread_end);

	printf("\nMain Terminating....");
	return EXIT_SUCCESS;
}

bool checkService(){
	return false;
}

void *peds_button_thread(void *data){
	printf("-----------Button Thread Started\n");
	thread_data button_Resources = *((thread_data*)data);

	packet_data msg_send;
	packet_reply_data msg_reply_recv;

	int serverPID  = 100000;	// CHANGE THIS Value to PID of the server process
	int	serverCHID = 1;			// CHANGE THIS Value to Channel ID of the server process (typically 1)
	int server_coid = 0;	//Value of channel

	//Following code reads file from relative location
	FILE *fp;
	fp = fopen( "PedestrianInput.info", "r" );
	if( fp != NULL ){
		int readValue[1];
		fread( &readValue, sizeof(int), sizeof(readValue), fp );
		printf("From Client: CHID is: %d\n", readValue[0]);
		printf("From Client: PID Value is: %d\n", readValue[1]);
		fclose( fp );
		serverPID = readValue[0]; //wrote pid from file
		serverCHID = readValue[1]; //write chid from file
	}
	else{
		printf("From Client: Could not open file\n");
	}

	server_coid = ConnectAttach(ND_LOCAL_NODE, serverPID, serverCHID, _NTO_SIDE_CHANNEL, 0); //conenct to channel, returns value of connection ID

	if (server_coid == -1){
		printf("ERROR, could not connect to server!\n\n");
		//  pthread_exit(EXIT_FAILURE);
	}

	msg_send.ClientID = 200; //can be changed to whatever
	msg_send.hdr.type = 0x00; //set information as data send
	msg_send.hdr.subtype = 0x00; //set information as data send
	while(1)
	{
		scanf("%c",&msg_send.data); //wait for user input
		if (MsgSend(server_coid, &msg_send, sizeof(msg_send), &msg_reply_recv, sizeof(msg_reply_recv)) == -1)
		{
			printf(" Error data '%d' NOT sent to server\n", msg_send.data);
			break;
		}
		else
		{  // now process the reply
		   // printf("   -->Reply is: '%s'\n", msg_reply_recv.buf);
		}
	}
}

void *pedestrianFSM_thread(void *data){
	printf("---------Traffic Light Thread Started\n");
	int rcvid;
	my_message_t msg;
	thread_data FSM_Resources = *((thread_data*)data);

	// initializer
	struct sigevent event;
	int chid;
	chid = ChannelCreate(0);
	event.sigev_notify = SIGEV_PULSE;

	// create a connection back to ourselves for the timer to send the pulse on
	event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, chid, _NTO_SIDE_CHANNEL, 0);
	if (event.sigev_coid == -1)
	{
	   printf(stderr, "%s:  couldn't ConnectAttach to self! \n", progname);
	   perror(NULL);
	   exit(EXIT_FAILURE);
	}

	struct sched_param th_param;
	pthread_getschedparam(pthread_self(), NULL, &th_param);
	event.sigev_priority = th_param.sched_curpriority;    // old QNX660 version getprio(0);

	event.sigev_code = MY_PULSE_CODE;

	// create the timer, binding it to the event
	if (timer_create(CLOCK_REALTIME, &event, &timer_id) == -1)
	{
	   printf (stderr, "%s:  couldn't create a timer, errno %d \n", progname, errno);
	   perror (NULL);
	   exit (EXIT_FAILURE);
	}

	// modification added here
	// 1 second delay interval timer
	itime.it_value.tv_sec = 1;	   // 1 second
	itime.it_value.tv_nsec = 0;    // 0 second
	itime.it_interval.tv_sec = 1;  // 1 second
	itime.it_interval.tv_nsec = 0; // 0 second

	// 2 second delay interval timer
	itime2.it_value.tv_sec = 2; // 2 second
	itime2.it_value.tv_nsec = 0; // 0 secomd
	itime2.it_interval.tv_sec = 2; // 2 second
	itime2.it_interval.tv_nsec = 0; // 0 second

	// immediate delay interval timer
	itime3.it_value.tv_sec = 0;		// 1 second
	itime3.it_value.tv_nsec = 1;    // 100 million nsecs = .1 nsecs
	itime3.it_interval.tv_sec = 0;  // 1 second
	itime3.it_interval.tv_nsec = 1; // 100 million nsecs = .1 nsecs

	enum trafficLightStates CurState = NSEWRed;

	int buttonInput; //hold buttoninput

	packet_reply_data msg_reply_send;
	int serverPID = getpid(); 		// get server process ID
	FILE *fp;

	packet_data msg_recv;
	msg_recv.hdr.type = 0x01;
	msg_recv.hdr.subtype = 0x00;

	// Create Channel
	chid = ChannelCreate(_NTO_CHF_DISCONNECT);
	/*
	 * Create channel that can be used to receive messages and pulses
	 * Once created, channel owned by process and not creating thread
	 * _NTO_CHF_DISCONNECT delivers pulse when all connections from pulse detach
	 * 		if process dies without detachign connection, kernal does it
	 */

	if (chid == -1)  // _NTO_CHF_DISCONNECT flag used to allow detach
	{
		printf("\nFailed to create communication channel on server\n");
		pthread_exit(EXIT_FAILURE);
	}

	fp = fopen( "PedestrianInput.info", "w" );
	if( fp != NULL )
	{
		int writeError;
		writeError = fwrite( &serverPID, sizeof(int), sizeof(serverPID), fp );
		writeError = fwrite( &chid, sizeof(int), sizeof(chid), fp );
		printf( "From Server: Successfully wrote ServerID and ChannelID: %d and %d \n", serverPID, chid);
		fclose( fp );
	}
	else
	{
		printf("Failed to write\n");
		pthread_exit(EXIT_FAILURE);
	}

	printf("From Server: Process ID   : %d \n", serverPID);
	printf("From Server: Channel ID   : %d \n\n", chid);

	timer_settime(timer_id, 0, &itime3, NULL);
	while (1){
		rcvid = MsgReceive(chid, &msg, sizeof(msg), NULL);
		if (rcvid == 0){
			if(msg.pulse.code == MY_PULSE_CODE){
				//SingleStep_Pedestrian_SM( &CurState );
			}
		}
		fflush(stdout);
	}
}

// pass it down the trafic
void SingleStep_TrafficLight_SM(enum trafficLightStates *TrafficLightCurState,
		enum pedStates *PedestrianCurState){

	bool check = false;

	if(check){
		switch(*TrafficLightCurState){
				case EWGreen:

					break;
				case NSGreen:

					break;
				case W_WRGreen:

					break;
				case E_ERGreen:

					break;
				case N_NRGreen:

					break;
				case S_SRGreen:

					break;
				default:
					break;
		}

		// call rusu on you m8
	}
}

void SingleStep_Pedestrian_SM(enum pedStates *CurState){
	int localState = *CurState;
	switch(*CurState){
		case NSEWPRed:
			// do the button check and also the scheduling
			break;
		case NPGreen:
			*CurState = NPFlash;
			break;
		case NPFlash:
			*CurState = NSEWPRed;
			break;
		case SPGreen:
			*CurState = SPFlash;
			break;
		case SPFlash:
			*CurState = NSEWPRed;
			break;
		case EPGreen:
			*CurState = EPFLash;
			break;
		case EPFLash:
			*CurState = NSEWPRed;
			break;
		case WPGreen:
			*CurState = WPFlash;
			break;
		case WPFlash:
			*CurState = NSEWPRed;
			break;
		case NP_SPGreen:
			*CurState = NP_SPFlash;
			break;
		case NP_SPFlash:
			*CurState = NSEWPRed;
			break;
		case EP_WPGreen:
			*CurState = EP_WPFlash;
			break;
		case EP_WPFlash:
			*CurState = NSEWPRed;
			break;
		default:
			break;
	}
}

void PedStatePrintState(enum pedStates *CurState){
	int localState = *CurState;
	switch(*CurState){
		case NSEWPRed:
			printf("In current state: NSEWPRed");
			break;
		case NPGreen:
			printf("In current state: NPGreen");
			break;
		case NPFlash:
			printf("In current state: NPFlash");
			break;
		case SPGreen:
			printf("In current state: SPGreen");
			break;
		case SPFlash:
			printf("In current state: SPFlash");
			break;
		case EPGreen:
			printf("In current state: EPGreen");
			break;
		case EPFLash:
			printf("In current state: EPFlash");
			break;
		case WPGreen:
			printf("In current state: WPGreen");
			break;
		case WPFlash:
			printf("In current state: WPFlash");
			break;
		case NP_SPGreen:
			printf("In current state: NP_SPGreen");
			break;
		case NP_SPFlash:
			printf("In current state: NP_SPFlash");
			break;
		case EP_WPGreen:
			printf("In current state: EP_WPGreen");
			break;
		case EP_WPFlash:
			printf("In current state: EP_WPFlash");
			break;
	}
}

int packetListen(int chid, packet_data * msg_recv, packet_reply_data * msg_reply_send)
{
	int rcvid=0, msgnum=0, buttonInput = 10;
	rcvid = MsgReceive(chid, msg_recv, sizeof(*msg_recv), NULL);
	if (rcvid == -1)  // Error condition, exit
	{
		printf("\nFailed to MsgReceive\n");
		return 10;
	}
	else
	{
		printf("From Server - Message is: %d\n\n",msg_recv->data);
		buttonInput = msg_recv->data;
	}

	if (rcvid == 0)  //  Pulse received, work out what type
		   {
			   switch (msg_recv->hdr.code)
			   {
				   case _PULSE_CODE_DISCONNECT:
						// A client disconnected all its connections by running
						// name_close() for each name_open()  or terminated
					    ConnectDetach(msg_recv->hdr.scoid);
						printf("\nServer was told to Detach from ClientID:%d ...\n", msg_recv->ClientID);
						return 10;

				   case _PULSE_CODE_UNBLOCK:
						// REPLY blocked client wants to unblock (was hit by a signal
						// or timed out).  It's up to you if you reply now or later.
					   printf("\nServer got _PULSE_CODE_UNBLOCK after %d, msgnum\n", msgnum);
					   break;

				   case _PULSE_CODE_COIDDEATH:  // from the kernel
					   printf("\nServer got _PULSE_CODE_COIDDEATH after %d, msgnum\n", msgnum);
					   break;

				   case _PULSE_CODE_THREADDEATH: // from the kernel
					   printf("\nServer got _PULSE_CODE_THREADDEATH after %d, msgnum\n", msgnum);
					   break;

				   default:
					   // Some other pulse sent by one of your processes or the kernel
					   printf("\nServer got some other pulse after %d, msgnum\n", msgnum);
					   break;

			   }
		   }

	   if(rcvid > 0) // if true then A message was received
	   {
		   // If the Global Name Service (gns) is running, name_open() sends a connect message. The server must EOK it.
		   if (msg_recv->hdr.type == _IO_CONNECT )
		   {
			   MsgReply( rcvid, EOK, NULL, 0 );
			   printf("\n gns service is running....");
		   }

		   // Some other I/O message was received; reject it
		   if (msg_recv->hdr.type > _IO_BASE && msg_recv->hdr.type <= _IO_MAX )
		   {
			   MsgError( rcvid, ENOSYS );
			   printf("\n Server received and IO message and rejected it....");
			   return 10;
		   }

		   // put your message handling code here and assemble a reply message
		   sprintf(msg_reply_send->buf, "Value: %d Received", msg_recv->data);


		   fflush(stdout);
		   sleep(1); // Delay the reply by a second (just for demonstration purposes)
	//	   printf("\n    -----> replying with: '%s'\n",replymsg.buf);
		   MsgReply(rcvid, EOK, &msg_reply_send, sizeof(msg_reply_send));
	   }
	   else
	   {
		   printf("\nERROR: Server received something, but could not handle it correctly\n");
	   }

	   return buttonInput;
}
